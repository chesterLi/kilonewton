@extends('site.layouts.default')
{{-- Content --}}
@section('content')
<div  style="position: relative;height: 50em;">
    <div class="panel panel-default" style="width:500px; position:absolute; top:20%; left:50%;margin-right: -50%;transform: translate(-50%, -50%);margin: 0;">
        <div class="panel-body">
        <div class="page-header">
            <h1>Восстановление пароля</h1>
        </div>
        <form action="{{URL::to('user/forgot')}}" method="post">

            <div class="form-group">
                <label for="email" class="col-sm-2 control-label">Введите email:</label>
                <div class="col-sm-10">
                    <input type="text" name="email" class="form-control">
                </div>
              </div>
              <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                    <button type="submit" class="btn btn-danger">Восстановить пароль</button>
                </div>
              </div>
        </form>
        </div>
    </div>
</div>
@if(Session::has('message'))
    <div class="modal fade" id="notif" tabindex="-1" role="dialog" aria-labelledby="notifLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Закрыть</span></button>
                <div class="modal-body">
                    {{Session::get('message')}}
                </div>
            </div>
        </div>
    </div>
@endif
@stop
