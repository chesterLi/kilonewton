<!DOCTYPE html>
<html>
<head>
    <title>Ваучер</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>

    <style>

    body {
        font-family: 'arial';
              font-style: normal;
              font-weight: 400;
              src: url({{public_path()}}'/assets/site/static/font/ARIAL.TTF') format('truetype');
    }
    @font-face {

    }
    </style>
</head>
<body>
{{--<img src="/assets/site/static/i/logopdf.jpg">--}}
    <h1>{{$code}}</h1>
    <p>Введите данный промо-код в специальное окошко во время оформления покупки курса.</p>
    <p>Пошаговая инструкция: Главная страница – все курсы – выберите нужный курс – в конце страницы, кликнуть «купить курс целиком» -
    ввести промо-код в специальное окошко – готово.</p>
    <p>Проект KiloNewton.ru — это центр онлайн-образования для архитекторов, проектировщиков и инженеров-строителей.
    Нашей командой, совместно с привлеченными профессионалами-практиками, разрабатываются и совершенствуются дистанционные
    курсы по AutoCAD, Revit, Robot, Лира и другим популярным CAD- и CAE-комплексам.</p>


</body>
</html>
