@extends('site.layouts.default')
{{-- Content --}}
@section('content')
<div class="breadcrumb-wrap">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-sm-12">
                <ol class="breadcrumb">
                    <li><a href="{{URL::to('/')}}">Главная</a></li>
                    <li class="active">Контакты</li>
                </ol>
            </div>
            <div class="col-md-6 hidden-sm hidden-xs">
                <div class="row">
 {{--                    <div class="col-sm-18 phone text-center">
                        8(800) 555-20-72
                    </div>
                    <div class="col-sm-6">
                        <form class="search" role="search" action="">
                            <div class="input-group">
                                <input type="text" name="query" class="form-control">
                                <span class="input-group-btn">
                                    <button class="btn btn-default" type="submit">Поиск</button>
                                </span>
                            </div>
                        </form>
                    </div>--}}
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container">
<h2>Контакты</h2>

    <div class="row">
        <div class="col-md-6">
            <ul class="list-unstyled">
                   <li><b>Офис:</b> Казахстан, г. Алматы, ул. Солодовникова 21, офис Е-5</li>
                   <li><b>Телефон:</b> +7 700 341 02 41</li>
                   <li><b>E-mail:</b> info@kilonewton.ru</li>
                   <li><b>Skype:</b> kilonewton.ru</li>
                   <li><a href="{{URL::to('about')}}">Контакты сотрудников</a></li>
            </ul>
            <h3>Реквизиты</h3>
            {{$text->text}}
        </div>
        <div class="col-md-6"><a class="dg-widget-link" href="http://2gis.kz/almaty/firm/9429940001525439/center/76.893319,43.23387500000001/zoom/16?utm_medium=widget-source&utm_campaign=firmsonmap&utm_source=bigMap">Посмотреть на карте Алматы</a><div class="dg-widget-link"><a href="http://2gis.kz/almaty/center/76.893319,43.233875/zoom/16/routeTab/rsType/bus/to/76.893319,43.233875╎КИЛОНЬЮТОН, компания по разработке онлайн-курсов для инженеров-строителей?utm_medium=widget-source&utm_campaign=firmsonmap&utm_source=route">Найти проезд до КИЛОНЬЮТОН, компания по разработке онлайн-курсов для инженеров-строителей</a></div><script charset="utf-8" src="http://widgets.2gis.com/js/DGWidgetLoader.js"></script><script charset="utf-8">new DGWidgetLoader({"width":440,"height":400,"borderColor":"#a3a3a3","pos":{"lat":43.23387500000001,"lon":76.893319,"zoom":16},"opt":{"city":"almaty"},"org":[{"id":"9429940001525439"}]});</script><noscript style="color:#c00;font-size:16px;font-weight:bold;">Виджет карты использует JavaScript. Включите его в настройках вашего браузера.</noscript></div>
    </div>

    <div class="row">
        <div class="col-md-12">
        <h3>Форма обратной связи</h3>
            <form action="{{URL::to('contacts/sendForm')}}" method="post">
            <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
              <div class="form-group">
                <label for="name">Ваше имя</label>
                <input type="name" class="form-control" id="name" placeholder="Имя" name="name">
                {{ $errors->first('name', '<span class="help-inline">:message</span>') }}
              </div>
              <div class="form-group">
                <label for="email">Ваш email</label>
                <input type="email" class="form-control" id="email" placeholder="Email" name="email">
                {{ $errors->first('email', '<span class="help-inline">:message</span>') }}
              </div>
              <div class="form-group">
                <label for="text">Текст</label>
                <textarea rows="10" cols="20" class="form-control" name="text" id="text"></textarea>
                {{ $errors->first('text', '<span class="help-inline">:message</span>') }}
              </div>
              <div class="g-recaptcha" data-sitekey="6LcjKCETAAAAAN6sFk-Nk1xMOBxZhB1dqAiP3Cku"></div>
               {{ $errors->first('g-recaptcha-response', '<span class="help-inline">:message</span>') }}
              <button type="submit" class="btn btn-default">Отправить</button>
            </form>
            </br>
        </div>
    </div>
</div><!-- .user-courses-statt -->
    @if(Session::has('success_form'))
        <div class="modal fade" id="notif" tabindex="-1" role="dialog" aria-labelledby="notifLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Закрыть</span></button>
                    <div class="modal-body">
                        Спасибо! Мы скоро свяжемся с вами!
                    </div>
                </div>
            </DIV>
        </div>
    @endif
@stop