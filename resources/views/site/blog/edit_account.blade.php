@extends('site.layouts.default')
{{-- Content --}}
@section('content')
<div class="breadcrumb-wrap">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-sm-12">
                <ol class="breadcrumb">
                    <li><a href="{{URL::to('/')}}">Главная</a></li>
                    <li class="active">Кабинет пользователя</li>
                </ol>
            </div>
            <div class="col-md-6 hidden-sm hidden-xs">
                <div class="row">
 {{--                    <div class="col-sm-18 phone text-center">
                        8(800) 555-20-72
                    </div>
                   <div class="col-sm-6">
                        <form class="search" role="search" action="">
                            <div class="input-group">
                                <input type="text" name="query" class="form-control">
                                <span class="input-group-btn">
                                    <button class="btn btn-default" type="submit">Поиск</button>
                                </span>
                            </div>
                        </form>
                    </div>--}}
                </div>
            </div>
        </div>
    </div>
</div>
<div class="user-courses">
    <div class="container">
        <div class="row">
            <form class="form-horizontal" method="post" action="{{URL::to('user/edit')}}" enctype="multipart/form-data">
                <div class="form-group">
                    <h4 class="text-center">Редактирование</h4>
		<!-- notifications -->
		@include('notifications')
		<!-- ./ notifications -->
                </div>
                <div id="edit-errors"></div>
                <h4>Личные данные</h4>
                <div class="form-group" id="email" {{{ $errors->has('email') ? 'error' : '' }}}>
                    <label for="email" class="col-xs-3">Email:</label>
                    <div class="col-sm-9">
                        <input type="text" name="email" class="form-control" value="{{{ Input::old('email', isset($user) ? $user->email : null) }}}">
                        {{ $errors->first('email', '<span class="help-inline">:message</span>') }}
                    </div>
                </div>
                <div class="form-group" id="username" {{{ $errors->has('username') ? 'error' : '' }}}>
                    <label for="username" class="col-xs-3">ФИО:</label>
                    <div class="col-sm-9">
                        <input type="text" name="username" class="form-control" value="{{{ Input::old('username', isset($user) ? $user->username : null) }}}">
                        {{ $errors->first('username', '<span class="help-inline">:message</span>') }}
                    </div>
                </div>
                <div class="form-group" id="gender">
                    <label for="gender" class="col-xs-3">Пол:</label>
                    <div class="col-sm-9 form-inline">
                        Муж {{ Form::radio('gender', '1', ($user->gender == '1'), array('class'=>'radio')) }}
                        Жен {{ Form::radio('gender', '2', ($user->gender == '2'), array('class'=>'radio')) }}
                    </div>
                </div>
                <div class="form-group" id="phone">
                    <label for="name" class="col-xs-3">Телефон:</label>
                    <div class="col-sm-9">
                        <input type="text" name="phone" class="form-control" value="{{{ Input::old('phone', isset($user) ? $user->phone : null) }}}">
                    </div>
                </div>
                <div class="form-group" id="photo">
                    <label for="photo" class="col-xs-3">Аватар:</label>
                    <div class="col-sm-9">
                    <div>Загрузите фотографию, выделите область, которая должна отображаться и нажмите "сохранить".</div>
                       <input type="file" name="photo" id="user-photo"><br>
                       <input type="hidden" name="file-name">
                            @if(isset($user->file))
                                <img src="{{URL::to('uploads/users', $user->file)}}" id="uploaded-image">
                            @else
                                <img src="/assets/site/static/i/ava-muzh.png" id="uploaded-image">
                            @endif
                    </div>
                </div>
                <input id="file-name" name="file_name" type="hidden">
                <input type="hidden" name="file_width" value="" id="file-width">
                <input type="hidden" name="file_height" value="" id="file-height">
                <input type="hidden" name="file_x" value="" id="file-x">
                <input type="hidden" name="file_y" value="" id="file-y">
                <h4>Сменить пароль</h4>
                <div class="form-group" id="old_password">
                    <label for="password" class="col-xs-3">Старый пароль:</label>
                    <div class="col-sm-9">
                        <input type="password" name="old_password" class="form-control">
                    </div>
                </div>
                <div class="form-group" id="password">
                    <label for="password" class="col-xs-3">Новый пароль:</label>
                    <div class="col-sm-9">
                        <input type="password" name="password" class="form-control">
                    </div>
                </div>
                <div class="form-group" id="password_confirmation">
                    <label for="password" class="col-xs-3">Повторить пароль:</label>
                    <div class="col-sm-9">
                        <input type="password" name="password_confirmation" class="form-control">
                    </div>
                </div>
                <input type="hidden" value="{{$user->id}}" name="user_id">
                <div class="form-group">
                    <div class="col-sm-12">
                        <button type="submit" class="btn btn-danger center-block">Сохранить</button>
                        @if(Sentry::getUser()->role == 'hr')
                        <a href="{{URL::to('cabinet_hr', $user->id)}}">Вернуться в кабинет</a>
                        @elseif(Sentry::getUser()->role == 'author')
                        <a href="{{URL::to('cabinet_author', $user->id)}}">Вернуться в кабинет</a>
                        @elseif(Sentry::getUser()->role == 'instructor')
                        <a href="{{URL::to('cabinet_instructor', $user->id)}}">Вернуться в кабинет</a>
                        @elseif(Sentry::getUser()->role == 'partner')
                            <a href="{{URL::to('cabinet_partner', $user->id)}}">Вернуться в кабинет</a>
                        @else
                        <a href="{{URL::to('account', $user->id)}}">Вернуться в кабинет</a>
                        @endif
                    </div>
                </div>
            </form>
        </div>
    </div>
</div><!-- .user-courses -->
@stop