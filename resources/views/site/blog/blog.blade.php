@extends('site.layouts.default')
{{-- Content --}}
@section('content')
<div class="breadcrumb-wrap">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-sm-12">
                <ol class="breadcrumb">
                    <li><a href="{{URL::to('/')}}">Главная</a></li>
                    <li class="active">Статьи</li>
                </ol>
            </div>
            <div class="col-md-6 hidden-sm hidden-xs">
                <div class="row">
{{--                    <div class="col-sm-18 phone text-center">
                        8(800) 555-20-72
                    </div>
                    <div class="col-sm-6">
                        <form class="search" role="search" action="">
                            <div class="input-group">
                                <input type="text" name="query" class="form-control">
                                <span class="input-group-btn">
                                    <button class="btn btn-default" type="submit">Поиск</button>
                                </span>
                            </div>
                        </form>
                    </div>--}}
                </div>
            </div>
        </div>
    </div>
</div>
<div class="blog-wrap bg-gray">
    <div class="container">
        <ul class="filter-wrap">
        @foreach($tags as $tag)
            <li lass="filter" data-filter="{{$tag->id}}"><a style="color:#ffffff" href="{{URL::to('tag_filter', $tag->id)}}">{{$tag->name}}</a> <span>({{$tag->blogs->count()}})</span></li>
        @endforeach
        </ul>
        <div class="courses-ad">
            <h3>Наши курсы</h3>
            <div class="courses-links">
            @foreach($courses as $course)
                <a href="{{URL::to('course', $course->id)}}">{{$course->name}}</a> |
            @endforeach
            </div>
        </div>
        <div class="row blog-list">
        <?php $counter = 0; ?>
        @foreach($articles as $article)
        @if($counter % 2 == 0)
            </div>
            <div class="row blog-list">
        @endif
        <?php $counter++; ?>
            <div class="col-sm-6 col-xs-12">
                <div class="blog-item">
                    <div class="blog-photo">
                        <a href="{{URL::to('blog', $article->id)}}"><img src="{{URL::to('uploads/blog/crop', $article->file)}}" width="476"/></a>
                    </div>
                    <div class="blog-content">
                        <a href="{{URL::to('blog', $article->id)}}" style="color: #3e4649"><h2>{{$article->title}}</h2></a>
                        <p>{{Str::words($article->text, $words = 10, $end = '...')}}</p>
                        <div class="blog-tags">
                        @foreach($article->tags as $tag)
                            <a href="{{URL::to('tag_filter', $tag->id)}}">{{$tag->name}}</a>,
                        @endforeach
                        </div>
                    </div>
                    <a href="{{URL::to('blog', $article->id)}}" class="btn btn-block"><span class="glyphicon glyphicon-chevron-down" aria-hidden="true"></span></a>
                </div>
            </div>
        @endforeach
        </div>
    </div><!-- .container -->
</div><!-- .blog-wrap -->

<div class="blog-subscribe">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-xs-12 blog-subscribe-text">Понравился блог? Подписывайтесь на рассылку!</div>
            <div class="col-md-6 col-xs-12 blog-subscribe-form">
                <form class="form-inline">
                    <div class="form-group">
                        <label for="subscribe-email">Ваш e-mail:</label>
                        <input type="email" class="form-control" id="subscribe-email">
                    </div>
                    <button type="submit" class="btn btn-info">подписаться</button>
                </form>                
            </div>
        </div><!-- .row -->
    </div><!-- .container -->
</div>
@stop