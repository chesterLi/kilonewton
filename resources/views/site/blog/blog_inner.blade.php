@extends('site.layouts.default')
{{-- Content --}}
@section('content')
<div class="breadcrumb-wrap">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-sm-12">
                <ol class="breadcrumb">
                    <li><a href="{{URL::to('/')}}">Главная</a></li>
                    <li><a href="{{URL::to('blog')}}">Статьи</a></li>
                    <li class="active">{{$blog->title}}</li>
                </ol>
            </div>
            <div class="col-md-6 hidden-sm hidden-xs">
                <div class="row">
{{--                     <div class="col-sm-18 phone text-center">
                        8(800) 555-20-72
                    </div>
                   <div class="col-sm-6">
                        <form class="search" role="search" action="">
                            <div class="input-group">
                                <input type="text" name="query" class="form-control">
                                <span class="input-group-btn">
                                    <button class="btn btn-default" type="submit">Поиск</button>
                                </span>
                            </div>
                        </form>
                    </div>--}}
                </div>
            </div>
        </div>
    </div>
</div>
<div class="blog-wrap bg-gray">
    <div class="container">
        <div class="row blog-single">
            <div class="col-md-9 col-xs-12">
                <div class="blog-content">
                    <h1>{{$blog->title}}</h1>
                    <div class="blog-photo">
                        <img src="{{URL::to('uploads/blog/crop', $blog->file)}}" class="img-responsive" />
                    </div>
                    <p>{{$blog->text}}</p>
                    <div class="row">
                        <div class="col-md-8 col-sm-6 col-xs-12">
                            <div class="blog-tags">
                            @foreach($blog->tags as $tag)
                                <a href="{{URL::to('tag_filter', $tag->id)}}">{{$tag->name}}</a>,
                            @endforeach
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-6 col-xs-12">
                            <div class="social-links">
                                <div class="pluso" data-background="transparent" data-options="medium,square,line,horizontal,nocounter,theme=01" data-services="vkontakte,facebook,google,linkedin"></div>
                            </div>                    
                        </div>
                    </div>
                </div>
                
                <div class="recent-posts">
                    <div class="block-header">Это интересно:</div>
                    <div class="row">
                        @foreach($similar_blogs[0]->slice(0, 4) as $key=>$similar)
                        <div class="col-md-6 col-xs-12">
                            <a href="{{URL::to('blog', $similar->id)}}" class="recent-item">
                                <div class="recent-photo pull-left" style="background-image: url( {{URL::to('uploads/blog', $similar->file)}} );"></div>
                                <div class="recent-content">
                                    <h3>{{$similar->title}}</h3>
                                    <div>{{Str::words($similar->text, $words = 5, $end = '...')}}</div>
                                </div>
                            </a>
                        </div>
                        @endforeach
                    </div>
                </div><!-- .recent-posts -->
            </div>
            <div class="col-md-3 col-xs-12">
                <div class="row courses-list">
                @foreach($courses as $key=>$course)
                @if($course->category_id == 5)
                   <div class="col-md-12 col-xs-6">
                       <div class="courses-item bg-green">
                @elseif($course->category_id == 6)
                    <div class="col-md-12 col-xs-6">
                        <div class="courses-item bg-blue">
                @elseif($course->category_id == 7)
                    <div class="col-md-12 col-xs-6">
                        <div class="courses-item bg-yellow">
                @else
                    <div class="col-md-12 col-xs-6">
                        <div class="courses-item bg-red">
                @endif
                           <div class="courses-title">
                               <h2>{{$course->name}}</h2>
                           </div>
                           <div class="courses-content">
                               <p class="author">Автор: {{$course->author->username}}.</p>
                               <p class="finished">Количество человек прошедших курс: {{$course->users_passed}}</p>
                                <a href="{{URL::to('course', $course->id)}}" class="courses-start">начать бесплатно<span class="glyphicon glyphicon-chevron-right pull-right" aria-hidden="true"></span></a>
                           </div>
                       </div>
                   </div>
                @endforeach
               </div><!-- .courses-list -->
            </div>
        </div><!-- .row -->

    </div><!-- .container -->
</div><!-- .blog-wrap -->
@if(Session::has('success'))
    <div class="modal fade" id="notif" tabindex="-1" role="dialog" aria-labelledby="notifLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Закрыть</span></button>
                <div class="modal-body">
                    <p>Cпасибо за подписку!</p>
                </div>
            </div>
        </div>
    </div>
@endif
<div class="blog-subscribe">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-xs-12 blog-subscribe-text">Понравился блог? Подписывайтесь на рассылку!</div>
            <div class="col-md-6 col-xs-12 blog-subscribe-form">
                <form class="form-inline" method="post" action="{{URL::to('subscribe')}}">
                    <div class="form-group">
                        <label for="subscribe-email">Ваш e-mail:</label>
                        <input type="email" class="form-control" id="subscribe-email" name="email">
                    </div>
                    <button type="submit" class="btn btn-info">подписаться</button>
                </form>                
            </div>
        </div><!-- .row -->
    </div><!-- .container -->
</div><!-- .blog-subscribe -->

<div class="comments">
    <div class="container">
        <h4>Оставить комментарий</h4>
        <form class="comment-form" enctype="multipart/form-data" method="post" id="send-comment">
            <label>новое сообщение:</label>
            <textarea  name="text" id="message-textarea"></textarea>
            <input type="hidden" value="{{$blog->id}}" name="blog_id">
            @if(!Sentry::check())
                Что бы оставить комментарий <a href="#" data-toggle="modal" data-target="#sign-in">авторизуйтесь</a> или
                <a href="#" data-toggle="modal" data-target="#reg">зарегистрируйтесь</a>.
            @else
            <button type="submit" class="btn btn-success">опубликовать</button>
            @endif
        </form>
        @if($blog->comments)
        <div id="all-comments">
        @foreach($blog->comments as $comment)
            <div class="comment">
                <div class="name">{{$comment->user->username}} <time>[{{$comment->created_at}}]</time></div>
                <p>
                    {{$comment->text}}
                </p>
            </div>
        @endforeach
        </div>
        @endif
    </div>
</div>
@stop