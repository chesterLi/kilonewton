@extends('site.layouts.default')
{{-- Content --}}
@section('content')
<div class="about-top">
    <img src="assets/site/static/i/about-img.jpg" class="img-responsive center-block">
</div>

<div class="bg-gray about-main">
    <div class="container">

        <h1 class="block-header text-center">О компании</h1>
            <div>
                {{$about->text}}
            </div>
        <h2 class="block-header text-center">Почему учатся у нас?</h2>
        <div class="about-benefit-list">
            <div class="about-benefit">
                <img src="assets/site/static/i/about-benefit-1.png">
                Доступность и планомерность изложения материала
            </div>
            <div class="about-benefit">
                <img src="assets/site/static/i/about-benefit-2.png">
                Глубокое погружение в тему, подробная информация о каждом инструменте
            </div>
            <div class="about-benefit">
                <img src="assets/site/static/i/about-benefit-3.png">
                Занятия ведут сертифицированные преподаватели
            </div>
            <div class="about-benefit">
                <img src="assets/site/static/i/about-benefit-4.png">
                Отработанная модель преподавания
            </div>
            <div class="about-benefit">
                <img src="assets/site/static/i/about-benefit-5.png">
                Индивидуальный подход к обучению и индивидуальные консультации
            </div>
            <div class="about-benefit">
                <img src="assets/site/static/i/about-benefit-6.png">
                Интерактивные задания
            </div>
            <div class="about-benefit">
                <img src="assets/site/static/i/about-benefit-7.png">
                Удобное для занятий время и место
            </div>
            <div class="about-benefit">
                <img src="assets/site/static/i/about-benefit-8.png">
                Возможность выбрать только нужные уроки, а не слушать весь курс, отыскивая необходимую информацию
            </div>
            <div class="about-benefit">
                <img src="assets/site/static/i/about-benefit-9.png">
                Курс «заточен» на практическое применение знаний
            </div>
            <div class="about-benefit">
                <img src="assets/site/static/i/about-benefit-10.png">
                Получение сертификата Autodesk после прохождения курса
            </div>
            <div class="about-benefit">
                <img src="assets/site/static/i/about-benefit-11.png">
                Карьерный и профессиональный рост
            </div>
        </div>

    </div>
</div>

<div class="container team">

    <h2 class="block-header text-center">Команда</h2>
    <div class="row text-center">
            <div class="col-xs-10 col-xs-offset-1 col-sm-4 col-sm-offset-1">
                <div class="team-member">
                    <img src="assets/site/static/i/avasilyev.jpeg" class="img-responsive img-circle center-block" />
                    <h4>Александр Васильев</h4>
                    <h5>Разработчик электронных курсов</h5>
                    <p>Все курсы на kiloNewton, Александр, скрупулезно изучает, структурирует, выстраивает и снабжает интерактивными заданиями. Кроме того, он создает обучающие видеоролики по продуктами компании.<br><b>alex@kilonewton.ru</b></p>
                </div>
            </div>

        <div class="col-xs-10 col-xs-offset-1 col-sm-4 col-sm-offset-2">
            <div class="team-member">
                <img src="assets/site/static/i/ragim.jpeg" class="img-responsive img-circle center-block" />
                <h4>Рагим Рамазанов</h4>
                <h5>Менеджер по маркетингу</h5>
                <p>Совершенствует процессы взаимодействия компании с клиентами и партнерами. Занимается подбором материалов для сайта компании.<br><b>ragim@kilonewton.ru</b></p>
            </div>
        </div>
    </div>
    <div class="row text-center">
        <div class="col-xs-10 col-xs-offset-1 col-sm-4 col-sm-offset-4">
            <div class="team-member">
                <img src="assets/site/static/i/me.jpeg" class="img-responsive img-circle center-block" />
                <h4>Мария Кулькина</h4>
                <h5>Программист</h5>
                <p>С любовью пишет код, который служит на благо повышения квалификации специалистов по строительному проектированию.<br><b>mk@kilonewton.ru</b></p>
            </div>
        </div>
    </div>

</div>

<div class="bg-ocean about-ocean-block">
    <div class="container">
        На курсах kiloNewton преподают <a href="{{URL::to('experts')}}">эксперты</a> отрасли
    </div>
</div>

{{--<div class="container">
    <h2 class="block-header text-center">С нами сотрудничают:</h2>
    <div class="partners-slider-wrap">
        <div class="partners-slider">
            <div class="slide-box">
                <div class="slide"><img src="assets/site/static/i/_temp/partner-1.png" /></div>
            </div>
            <div class="slide-box">
                <div class="slide"><img src="assets/site/static/i/_temp/partner-2.png" /></div>
            </div>
            <div class="slide-box">
                <div class="slide"><img src="assets/site/static/i/_temp/partner-3.png" /></div>
            </div>
            <div class="slide-box">
                <div class="slide"><img src="assets/site/static/i/_temp/partner-4.png" /></div>
            </div>
            <div class="slide-box">
                <div class="slide"><img src="assets/site/static/i/_temp/partner-5.png" class="img-responsive" /></div>
            </div>
        </div>
    </div><!-- .partners-slider-wrap -->
</div>--}}
@stop