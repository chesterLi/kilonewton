@extends('site.layouts.default')
{{-- Content --}}
@section('content')
<div class="comments bg-gray">
    <div class="container">
        <h4 class="text-center">Обсуждение задания</h4>
        <form class="comment-form" enctype="multipart/form-data" method="post" id="send-message">
            <label>новое сообщение:</label>
            <textarea  name="text" id="message-textarea"></textarea>
            <input type="hidden" value="{{$chapter_id}}" name="chapter_id">
            <input type="hidden" value="{{$course->id}}" name="course_id">
            <input type="hidden" value="{{$course->instructor_id}}" name="instructor_id">
            <input type="hidden" value="{{$course->id}}" name="course_id">
            <input type="file" name="discussion_file"/>
            @if(!Sentry::check())
                Что бы отправить вопрос инструктору <a href="#" data-toggle="modal" data-target="#sign-in">авторизуйтесь</a> или
                <a href="#" data-toggle="modal" data-target="#reg">зарегистрируйтесь</a>.
            @else
            <button type="submit" class="btn btn-success">опубликовать</button>
            @endif
        </form>
        @if(isset($discussion))
        <div id="all-discussions">
        @foreach($discussion as $item)
        <div class="comment">
            <div class="name">{{$item->user->username}} <time>[{{$item->created_at}}]</time></div>
            <p>
                {{$item->text}}
            </p>
            @if($item->file)
            <ul class="comment-attach">
                <li><a href="{{URL::to('download',array('discussion',$item->file))}}">скачать</a></li>
            </ul>
            @endif
        </div>
        @endforeach
        </div>
        @endif
    </div>
</div>
@stop