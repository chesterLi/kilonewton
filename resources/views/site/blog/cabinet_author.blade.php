@extends('site.layouts.default')
{{-- Content --}}
@section('content')
<div class="breadcrumb-wrap">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-sm-12">
                <ol class="breadcrumb">
                    <li><a href="{{URL::to('/')}}">Главная</a></li>
                    <li class="active">Кабинет автора</li>
                </ol>
            </div>
            <div class="col-md-6 hidden-sm hidden-xs">
                <div class="row">
 {{--                   <div class="col-sm-18 phone text-center">
                        8(800) 555-20-72
                    </div>
                    <div class="col-sm-6">
                        <form class="search" role="search" action="">
                            <div class="input-group">
                                <input type="text" name="query" class="form-control">
                                <span class="input-group-btn">
                                    <button class="btn btn-default" type="submit">Поиск</button>
                                </span>
                            </div>
                        </form>
                    </div>--}}
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container p-b-35">
    <div class="row">
        <div class="col-xs-3 hidden-xs">
        @if(!$user->file)
            @if(!$user->gender || $user->gender == 1)
                <img src="/assets/site/static/i/ava-muzh.png" class="img-responsive">
            @else
                <img src="/assets/site/static/i/ava-zhen.png" class="img-responsive">
            @endif
        @else
            <img src="{{URL::to('uploads/users',$user->file)}}" class="img-responsive img-circle" width="180px">
        @endif
        </div>
        <div class="col-sm-9 col-xs-12">
            <h1 class="block-header" style="padding: 0 0 0.4em 0;">Кабинет автора</h1>
            <div class="user-info">
                <div class="user-name">ФИО: {{$user->username}}</div>
                <div>Количество курсов/глав: <b>{{$courses->count()}}/{{$chapters->count()}}</b></div>
                {{--<div>Обучается/закончили: <b>10/{{count($students)}}</b></div>--}}
                <div><a href="{{URL::to('user/get_edit', $user->id)}}" class="user-setting pull-left"><img src="/assets/site/static/i/icon-setting.png"></a></div>
            </div>
        </div>
    </div>
</div>
        
<div class="bg-gray p-tb-35">
    <div class="container">
        <div class="h2">Курсы</div>
        <div class="row">
        @foreach($courses as $course)
            <div class="col-xs-12 col-sm-6">
                <p><b>{{$course->name}}</b></p>
                <p>Главы</p>
                @foreach($course->chapters as $chapter)
                    <p>{{$chapter->name}}</p>
                @endforeach
            </div>
            @endforeach
        </div>
    </div>
</div>

<div class="p-tb-35">
    <div class="container">
        
        <div class="h2">История платежей слушателей</div>
        
        <table class="table table-responsive table-condensed table-cleared">
            <tr>
                <th>ФИО</th>
                <th>Дата платежа</th>
                <th>Сумма</th>
                <th>Курс</th>
                <th>Главы</th>
            </tr>
            @foreach($orders as $key=>$order)
            <tr>
                <td>{{$order->username->username}}</td>
                <td>{{$order->created_at}}</td>
                <td>{{$order->sum}}</td>
                <td>{{$key}}</td>
                <td>
                    @foreach($order->items as $item)
                       {{$item->product}},
                    @endforeach
                </td>
            </tr>
            @endforeach
            <tr class="total-row">
                <td><b>Итого:</b></td>
                <td></td>
                <td><b>{{$total}}</b></td>
                <td></td>
                <td></td>
            </tr>
        </table>

    </div>
</div>
@stop