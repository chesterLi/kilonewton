@extends('site.layouts.default')
{{-- Content --}}
@section('content')

    <div class="breadcrumb-wrap">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-sm-12">
                    <ol class="breadcrumb">
                        <li><a href="{{URL::to('/')}}">Главная</a></li>
                        <li class="active">Тест</li>
                    </ol>
                </div>
                <div class="col-md-6 hidden-sm hidden-xs">
                    <div class="row">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="exam-task bg-gray">
        <div class="container">
            <div class="row text-center">
                Каждая глава курса имеет определенную тематику и раскрывает вопросы по определенным сегментам
                программного продукта.
                Предварительное тестирование поможет подобрать нужные для вас главы по данному курсу. Это сократит ваше
                время на ознакомление
                с полным содержанием курса и сэкономит ваши деньги, за счет приобретения только тех глав по которым у
                вас недостаточно знаний.
                Тестирование совершенно бесплатно.
                <hr/>
            </div>
            <?php
            $course = Course::find($test->course_id);
            $chapter = \App\Models\Chapter::find($test->chapter_id);
            ?>

            <form class="form-horizontal" method="post" id="course-test">
                @foreach($test->questionsLimit as $key => $question)
                    <?php
                    $paginator++;
                ?>
                    {{$paginator .'. '. $question->question}}
                    <ul style="list-style:lower-alpha" class="test-qst">
                        <li><label>{{$question->var}} <input type="radio" name="test[{{$question->id}}]"
                                                             value="{{$question->var}}"></label></li>
                        <li><label>{{$question->var1}} <input type="radio" name="test[{{$question->id}}]"
                                                              value="{{$question->var1}}"></label></li>
                        <li><label>{{$question->var2}} <input type="radio" name="test[{{$question->id}}]"
                                                              value="{{$question->var2}}"></label></li>
                        <li><label>{{$question->var3}} <input type="radio" name="test[{{$question->id}}]"
                                                              value="{{$question->var3}}"></label></li>
                        <li><label>{{$question->var4}} <input type="radio" name="test[{{$question->id}}]"
                                                              value="{{$question->var4}}"></label></li>
                    </ul>
                @endforeach
                <input type="hidden" name="chapter_id" value="{{$chapter->id}}">
                <input type="hidden" name="course_id" value="{{$course->id}}">
                <input type="hidden" name="csrf-token" value="{{ csrf_token() }}">
                <input type="hidden" name="paginator" value="{{$paginator}}">
            </form>

            @if($nextTest)
                <a class="btn btn-primary center-block" id="count-test" data-url="{{$nextTest->id}}"
                   href="{{URL::to('getNextTest', $nextTest->id)}}">Продолжить</a>
            @else
                <a class="btn btn-primary center-block" href="{{URL::to('getResult', $course->id)}}">Посмотреть
                    результат</a>
            @endif
        </div>
    </div>
@stop