@extends('site.layouts.default')
{{-- Content --}}
@section('content')
<div class="breadcrumb-wrap">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-sm-12">
                <ol class="breadcrumb">
                    <li><a href="{{URL::to('/')}}">Главная</a></li>
                    <li><a href="{{URL::to('courses')}}">Все курсы</a></li>
                    <li><a href="{{URL::to('course', $course->id)}}">{{$course->name}}</a></li>
                    <li class="active">{{$chapter->name}}</li>
                </ol>
            </div>
            <div class="col-md-6 hidden-sm hidden-xs">
                <div class="row">
 {{--                   <div class="col-sm-6 phone text-center">
                        8(800) 555-20-72
                    </div>
                    <div class="col-sm-6">
                        <form class="search" role="search" action="">
                            <div class="input-group">
                                <input type="text" name="query" class="form-control">
                                <span class="input-group-btn">
                                    <button class="btn btn-default" type="submit">Поиск</button>
                                </span>
                            </div>
                        </form>
                    </div>--}}
                </div>
            </div>
        </div>
    </div>
</div>
<div class="course-header bg-gray">
    <div class="container text-center">
        <h1>{{$course->name}}</h1>
        <div class="row course-include">
            <div class="col-md-12 col-md-offset-0 col-sm-10 col-sm-offset-1">
                <div class="row">
                    <div class="col-sm-3 col-xs-12">16 разделов</div>
                    <div class="col-sm-3 col-xs-12">119 видеоуроков</div>
                    <div class="col-sm-3 col-xs-12">15 интерактивов</div>
                    <div class="col-sm-3 col-xs-12">Сертификат по окончании</div>
                </div>
            </div>
        </div>
        <div class="progress-title">Выполнено {{$user_chapters}} из {{$course_chapters}} заданий ({{$percentage}}%)</div>
        <div class="progress">
            <div class="progress-bar" role="progressbar" aria-valuenow="{{$percentage}}" aria-valuemin="0" aria-valuemax="100" style="width: {{$percentage}}%;"></div>
        </div>        
    </div><!-- .container -->
</div><!-- .course-header -->

<div class="instructor double-row">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-xs-12">
                <div class="photo pull-left">
                    <img src="/assets/site/static/i/_temp/instructor.png" class="img-responsive" />
                </div>
                <div class="text">
                    <h4>Автор главы: <span>Александр Шахнович</span></h4>
                    <p>Благодарим вас за покупку курса! Данный учебный материал позволит вам овладеть навыками работы в одной из популярнейших на сегодня программ в сфере технологий информационного моделирования</p>
                </div>
            </div>
            <div class="col-md-6 col-xs-12 instructor-box">
                <div class="photo pull-right">
                    <img src="/assets/site/static/i/_temp/instructor.png" class="img-responsive" />
                </div>
                <div class="text">
                    <h4>Ваш инструктор: <span>Александр Васильев</span></h4>
                    <p>Добрый день! Ниже вы увидите список разделов курса. Каждый раздел состоит из уроков, интерактива и проверочного задания.</p>
                </div>
                <div>
                    <a href="#" class="btn btn-primary">заявка на консультацию</a>
                    <span class="info-text">доступно еще 12 дней</span>
                </div>
            </div>
        </div>
    </div><!-- .container -->
</div><!-- .instructor-->

<div class="course-parts bg-gray">
    <div class="container">
        <div class="part-header">
            <div class="part-status text-right">Доступно для изучения</div>
        </div>        
        <div class="row lessons-list">
        @foreach($chapter->materials as $material)
            <div class="col-xs-12 col-sm-6 col-md-4">
                <div class="lesson-item">
                    {{--<div class="lesson-overlay"><div>Просмотрено</div></div>--}}
                    <div class="lesson-title">
                        <h3>{{$material->name}}</h3>
                    </div>
                    @if($material->url)

                    <div style="display:none">
                        <div id="data{{$material->id}}"><iframe src="//player.vimeo.com/video/{{$material->url}}?portrait=0&color=333" width="960" height="540" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe><div style="margin:10px 10px;width:900px;">{{$material->text}}</div></div>
                    </div>

                    <a id="inline" href="#data{{$material->id}}"><img src="{{Helpers::getThumbnail($material->url)}}" width="360" height="200"></a>
                    @elseif($material->file)
                    <div style="display:none">
                        <div id="data{{$material->id}}"><iframe src="/uploads/zip/{{$material->file}}/index.html" width="960" height="540" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe><div style="margin:10px 10px;width:900px;">{{$material->text}}</div></div>
                    </div>
                    <a id="inline" href="#data{{$material->id}}"><img src="/assets/site/static/i/play.jpg" width="360" height="200"></a>
{{--                    <a id="inline" href="#data{{$material->id}}"><div class="lesson-photo" style="background-image: url(/assets/site/static/i/play.jpg);"></div></a>--}}
                    @endif
                    <a href="#" class="check-viewed">отметить как просмотренное</a>
                </div>
            </div>
            @endforeach
        </div><!-- .lessons-list -->
    </div><!-- .container -->
</div><!-- .course-parts -->

<div class="exam-header text-center">
    <div class="container">Проверочное задание</div>
</div>

<div class="exam-task bg-gray">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-4">
                <div class="task-preview" style="background-image: url(/assets/site/static/i/_temp/task.jpg);"></div>
                <a href="#" class="btn btn-block btn-success">скачать файл задания</a>
            </div>
            @if(Sentry::check() and $status != 1 and $status !=2)
            <form class="form-horizontal" method="post" action="{{ URL::to('examfile') }}" accept-charset="UTF-8">
            <div class="col-xs-12 col-sm-4 col-sm-offset-4">
                <div class="file-loader">
                    <span>не более 25 мб</span>
                    <div class="input-group">
{{--                        <input type="text" class="form-control">
                        <span class="input-group-btn">
                            <button class="btn btn-default" type="button">обзор</button>
                        </span>--}}
                        <input id="fileupload" type="file" name="files[]" data-url="{{URL::to('fileupload')}}" multiple>
                        <div id="loading" style="display:none"><i class="fa fa-cog fa-spin fa-fw margin-bottom" style="font-size:35px;"></i></div>
                    </div>
                    <div id="files"></div>
                    <input type="hidden" value="{{Sentry::getUser()->id}}" name="user_id">
                    <input type="hidden" value="{{$chapter->id}}" name="chapter_id">
                    <input type="hidden" value="{{$course->id}}" name="course_id">
                </div>
               {{-- <a href="#" class="btn btn-block btn-primary">отправить на проверку</a>--}}
                <button type="submit" class="btn btn-block btn-primary">отправить на проверку</button>
            </div>
            </form>
            @endif
        </div>
    </div>
</div>

<div class="exam-status text-center">
@if(isset($status))
    @if($status == 2)
    <h4>Статус: <span>Выполнено</span></h4>
    @elseif($status == 3)
    <h4>Статус: <span>Не принято</span></h4>
    @elseif($status == 1)
    <h4>Статус: <span>На проверке</span></h4>
    @endif
 @else
 <h4>Статус: <span>Не выполнено</span></h4>
@endif
    <p>За правильное выполенение задание + 10 баллов к вашему рейтингу</p>
</div>

<div class="comments bg-gray">
    <div class="container">
        <h4 class="text-center">Обсуждение задания</h4>
        <form class="comment-form" enctype="multipart/form-data" method="post" action="{{URL::to('discussion')}}">
            <label>новое сообщение:</label>
            <textarea  name="text"></textarea>
            <input type="hidden" value="{{$chapter->id}}" name="chapter_id">
            <input type="file" name="discussion_file"/>
            @if(!Sentry::check())
                Что бы отправить вопрос инструктору <a href="#" data-toggle="modal" data-target="#sign-in">авторизуйтесь</a> или
                <a href="#" data-toggle="modal" data-target="#reg">зарегистрируйтесь</a>.
            @else
            <button type="submit" class="btn btn-success">опубликовать</button>
            @endif
        </form>
        @if(isset($discussion))
        @foreach($discussion as $item)
        <div class="comment">
            <div class="name">{{$item->user->username}} <time>[{{$item->created_at}}]</time></div>
            <p>
                {{$item->text}}
            </p>
            @if($item->file)
            <ul class="comment-attach">
                <li><a href="{{URL::to('download',array('discussion',$item->file))}}">скачать</a></li>
            </ul>
            @endif
        </div>
        @endforeach
        @endif
    </div>
</div>
<ul class="part-pager clearfix">
    @if(!$previousChapter)
    <li class="prev"><span class="glyphicon glyphicon-cat" aria-hidden="true"></span></li>
    @else
    <li class="prev"><span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span><a href="{{URL::to('chapter',array($course->id, $previousChapter->id))}}">предыдущая глава</a></li>
    @endif
    @if(!$nextChapter)
    <li class="next"><span class="glyphicon glyphicon-chevron-cat" aria-hidden="true"></span></li>
    @else
    <li class="next"><a href="{{URL::to('chapter',array($course->id, $nextChapter->id))}}">следующая глава</a><span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span></li>
    @endif
</ul>
@stop