@extends('site.layouts.default')
{{-- Content --}}
@section('content')
<div class="breadcrumb-wrap">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-sm-12">
                <ol class="breadcrumb">
                    <li><a href="{{URL::to('/')}}">Главная</a></li>
                    <li><a href="{{URL::to('cabinet_hr', Sentry::getUser()->id)}}">Кабинет HR</a></li>
                    <li class="active">Лог действий пользователя</li>
                </ol>
            </div>
            <div class="col-md-6 hidden-sm hidden-xs">
                <div class="row">
 {{--                    <div class="col-sm-18 phone text-center">
                        8(800) 555-20-72
                    </div>
                   <div class="col-sm-6">
                        <form class="search" role="search" action="">
                            <div class="input-group">
                                <input type="text" name="query" class="form-control">
                                <span class="input-group-btn">
                                    <button class="btn btn-default" type="submit">Поиск</button>
                                </span>
                            </div>
                        </form>
                    </div>--}}
                </div>
            </div>
        </div>
    </div>
</div>

    <div class="container">
        <div class="row">

        <table class="table table-responsive table-condensed table-cleared">
                <tr>
                    <th>#</th>
                    <th>Событие</th>
                    <th>Дата</th>
                    <th>Время</th>
                    <th>Баллы рейтинга</th>
                    <th class="hidden-xs">Дополнительная информация</th>
                </tr>

                @if($data->isEmpty())
                <tr>
                <td>
                    <h3>Данных нет</h3>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    </td>
                </tr>
                @endif

                @foreach($data as $key=>$log)
                <tr>
                    <td>{{$key+1}}</td>
                    @if($log->action == 'video')
                    <td>Отметка просмотра видео</td>
                    <td>{{$log->created_at->format('d.m.Y')}}</td>
                    <td class="text-center">{{$log->created_at->format('H:i')}}</td>
                    <td>{{json_decode($log->data, true)['rating']}}</td>
                    <td class="hidden-xs"><a href="{{URL::to(json_decode($log->data, true)['link'])}}">Открыть главу</a></td>
                    @elseif($log->action == 'authorized')
                    <td>Вход в аккаунт</td>
                    <td>{{$log->created_at->format('d.m.Y')}}</td>
                    <td class="text-center">{{$log->created_at->format('H:i')}}</td>
                    <td>{{json_decode($log->data, true)['rating']}}</td>
                    <td class="hidden-xs"><a href="{{URL::to(json_decode($log->data, true)['link'])}}">{{json_decode($log->data, true)['link']}}</a></td>
                    @elseif($log->action == 'test')
                    <td>Сдача теста главы</td>
                    <td>{{$log->created_at->format('d.m.Y')}}</td>
                    <td class="text-center">{{$log->created_at->format('H:i')}}</td>
                    <td>{{json_decode($log->data, true)['rating']}}</td>
                    <td class="hidden-xs"><a href="{{URL::to(json_decode($log->data, true)['link'])}}">Открыть главу</a></td>
                    @elseif($log->action == 'comment')
                    <td>Написал комментарий к главе</td>
                    <td>{{$log->created_at->format('d.m.Y')}}</td>
                    <td class="text-center">{{$log->created_at->format('H:i')}}</td>
                    <td>{{json_decode($log->data, true)['rating']}}</td>
                    <td class="hidden-xs"><a href="{{URL::to(json_decode($log->data, true)['link'])}}">Открыть главу с комментарием</a></td>
                    @elseif($log->action == 'exam')
                    <td>Отправка финального задания инструктору</td>
                    <td>{{$log->created_at->format('d.m.Y')}}</td>
                    <td class="text-center">{{$log->created_at->format('H:i')}}</td>
                    <td>{{json_decode($log->data, true)['rating']}}</td>
                    <td class="hidden-xs"><a href="{{URL::to(json_decode($log->data, true)['link'])}}">Скачать файл</a></td>
                    @else
                    <td>Употребил 10 печенек в пищу</td>
                    <td>{{$log->created_at->format('d.m.Y')}}</td>
                    <td class="text-center">{{$log->created_at->format('H:i')}}</td>
                    <td>{{json_decode($log->data, true)['rating']}}</td>
                    <td class="hidden-xs"><a href="{{URL::to(json_decode($log->data, true)['link'])}}">{{json_decode($log->data, true)['link']}}</a></td>
                    @endif
                </tr>
                @endforeach
            </table>
    </div>
</div>
@stop