@extends('site.layouts.default')
{{-- Content --}}
@section('content')
<div class="breadcrumb-wrap">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-sm-12">
                <ol class="breadcrumb">
                    <li><a href="{{URL::to('/')}}">Главная</a></li>
                    <li class="active">Кабинет партнера</li>
                </ol>
            </div>
            <div class="col-md-6 hidden-sm hidden-xs">
                <div class="row">
 {{--                    <div class="col-sm-18 phone text-center">
                        8(800) 555-20-72
                    </div>
                   <div class="col-sm-6">
                        <form class="search" role="search" action="">
                            <div class="input-group">
                                <input type="text" name="query" class="form-control">
                                <span class="input-group-btn">
                                    <button class="btn btn-default" type="submit">Поиск</button>
                                </span>
                            </div>
                        </form>
                    </div>--}}
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container p-b-35">
    <div class="row">
        <div class="col-xs-3 hidden-xs">
            @if(!$user->file)
                @if(!$user->gender || $user->gender == 1)
                    <img src="/assets/site/static/i/ava-muzh.png" class="img-responsive">
                @else
                    <img src="/assets/site/static/i/ava-zhen.png" class="img-responsive">
                @endif
            @else
                <img src="{{URL::to('uploads/users',$user->file)}}" class="img-responsive img-circle" width="180px">
            @endif
        </div>
        <div class="col-sm-9 col-xs-12">
            <h1 class="block-header" style="padding: 0 0 0.4em 0;">Кабинет партнера</h1>
            <div class="user-info">
                <div class="user-name">Компания: {{$user->username}}</div>
                <div><b>Скидка для ваучера: от <span class="text-orange">{{ isset($partner_data->min) ? $partner_data->min : '' }}%</span> -  <span class="text-orange">{{ isset($partner_data->max) ? $partner_data->max : '' }}%</span></b></div>
                <div><b>Скидка для реферальной ссылки: от <span class="text-orange">{{ isset($partner_data->link_min) ? $partner_data->link_min : '' }}%</span> - <span class="text-orange">{{ isset($partner_data->link_max) ? $partner_data->link_max : '' }}%</span></b></div>
                </br>
                @if(!empty($partner_data->referral))
                <div><b>Реферальная ссылка: <span class="text-orange">{{ URL::to('referral', $partner_data->referral) }}</span></b></div>
                @endif
            </div>
        </div>
    </div>
</div>
        
<div class="bg-gray p-tb-35">
    <div class="container">
        @foreach($promo_limit as $item)
        @if(isset($item->user))
            <p><b>{{$item->user->username}}- <span class="text-orange">{{$item->discount}}%</span></b></p>
        @endif
        @endforeach
        <p>
            <span class="h2" style="margin-right: 2em;">Выдано ваучеров: {{$promo_codes->count()}}</span>
            <a href="#" class="btn btn-info" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">подробнее</a>
        </p>
    </div>
</div>

<div class="bg-gray p-tb-35 collapse" id="collapseExample">
    <div class="container" id="partner_table">

        <table class="table table-responsive table-condensed table-cleared">
            <tr>
                <th>Компания</th>
                <th>Код</th>
                <th>Курс</th>
                <th>Действителен до</th>
                <th>Скидка/комиссия</th>
                <th>E-mail</th>
                <th class="hidden-xs">PDF</th>
{{--                <th class="hidden-xs">Телефон</th>--}}
{{--                <th class="hidden-xs">Комментарий</th>--}}
            </tr>
            <tr class="filter-row hidden-xs">
                <td>
                    <div class="form-group form-group-sm">
                        <input type="text" class="form-control" placeholder="поиск" onchange="searchFunction.call(this);" id="company_search" data-type="company" data-partner="{{$user->id}}">
                    </div>
                </td>
                <td>
                    <div class="form-group form-group-sm">
                        <input type="text" class="form-control" placeholder="поиск" onchange="searchFunction.call(this);" id="company_search" data-type="code" data-partner="{{$user->id}}">
                    </div>
                </td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
{{--                <td>
                    <div class="form-group form-group-sm">
                        <input type="text" class="form-control" placeholder="поиск" onchange="searchFunction.call(this);" id="company_search" data-type="phone" data-partner="{{$user->id}}">
                    </div>
                </td>--}}
{{--                <td>
                    <div class="form-group form-group-sm">
                        <input type="text" class="form-control" placeholder="поиск">
                    </div>
                </td>--}}
            </tr>
            @foreach($promo_codes as $promo_code)
            @if($promo_code->lifetime < $now && !$promo_code->user)
            <tr>
                <td>-</td>
                <td>{{$promo_code->code}}</td>
                <td>{{$promo_code->course->name}}</td>
                <td>{{$promo_code->lifetime}}</td>
                <td>{{$promo_code->discount}}/{{$partner_data->max - $promo_code->discount}}</td>
                <td>-</td>
                <td class="hidden-xs">Срок промо-кода истек</td>
            </tr>
            @else
            <tr>
                @if($promo_code->user)
                <td>{{$promo_code->user->username}}</td>
                @else
                <td>-</td>
                @endif
                <td>{{$promo_code->code}}</td>
                <td>{{$promo_code->course->name}}</td>
                <td>{{$promo_code->lifetime}}</td>
                <td>{{$promo_code->discount}}/{{$partner_data->max - $promo_code->discount}}</td>
                @if(isset($promo_code->user))
                <td>{{$promo_code->user->email}}</td>
                @else
                <td>-</td>
                @endif
                <td class="hidden-xs"><a href="{{URL::to('partner_download', array($promo_code->code, $promo_code->course_id))}}">скачать</a></td>
{{--                @if(isset($promo_code->user))
                <td class="hidden-xs">{{$promo_code->user->phone}}</td>
                @else
                <td>-</td>
                @endif--}}
            </tr>
            @endif
            @endforeach
        </table>

    </div>
</div>
    
<div class="p-tb-35">
    <div class="container">
        <form class="form-inline voucher-generate" method="post" action="{{URL::to('make_promo')}}">
            <div class="form-group">
                {{ Form::select('courses', $courses, null, array('class'=>'form-control')) }}
            </div>
            <div class="form-group">
                <input type="text" class="form-control" data-dmin="{{$partner_data->min}}" data-dmax="{{$partner_data->max}}" placeholder="Укажите размер скидки" name="discount" id="partner-discount">
            </div>
            <div class="form-group">
                <input type="text" class="form-control" id="datepicker" placeholder="Срок действия ваучера" name="lifetime">
            </div>
            <div class="form-group">
                <input type="text" class="form-control" style="width: 45px" placeholder="1" name="amount">
            </div>
                <button type="submit" class="btn btn-primary btn-sm">сгенерировать ваучер</button>
{{--            <div class="form-group voucher-amount">
                комиисия партнера: <b class="text-blue">25%</b>
            </div>--}}
            <input type="hidden" value="{{$user->id}}" name="partner_id">
            <input type="hidden" value="{{$partner_data->max}}" name="partner_max">
            <input type="hidden" value="{{$partner_data->min}}" name="partner_min">
        </form>
    </div>
</div>

<div class="p-tb-35">
    <div class="container">
        <form class="form-inline voucher-generate" method="post" action="{{URL::to('make_referral')}}">
            <div class="form-group">
                <input type="text" class="form-control" data-dmin="{{$partner_data->link_min}}" data-dmax="{{$partner_data->link_max}}" placeholder="Укажите размер скидки" name="discount" id="partner-discount">
            </div>
                <button type="submit" class="btn btn-primary btn-sm">сгенерировать ссылку</button>
            <input type="hidden" value="{{$user->id}}" name="partner_id">
            <input type="hidden" value="{{$partner_data->link_max}}" name="partner_max">
            <input type="hidden" value="{{$partner_data->link_min}}" name="partner_min">
        </form>
    </div>
</div>

<div class="bg-gray p-tb-35">
    <div class="container">
        
        <div class="h2">Платежи по ваучерам</div>
        
        <table class="table table-responsive table-condensed table-cleared" id="partner_orders">
            <tr>
                <th>Компания</th>
                <th>Код</th>
                <th>Оплачен</th>
                <th>Сумма оплаты</th>
{{--                <th>Комиссия</th>--}}
                <th class="hidden-xs">E-mail</th>
{{--                <th class="hidden-xs">Телефон</th>--}}
            </tr>
            <tr class="filter-row hidden-xs">
                <td>
                    <div class="form-group form-group-sm">
                        <input type="text" class="form-control" placeholder="поиск" onchange="searchFunctionOrders.call(this);" id="company_search" data-type="company_order" data-partner="{{$user->id}}">
                    </div>
                </td>
                <td>
                    <div class="form-group form-group-sm">
                        <input type="text" class="form-control" placeholder="поиск" onchange="searchFunctionOrders.call(this);" id="company_search" data-type="code_order" data-partner="{{$user->id}}">
                    </div>
                </td>
                <td>
{{--                    <div class="form-group form-group-sm">
                        <input type="text" class="form-control date-control datepicker" id="date_2" onchange="searchFunctionOrders.call(this);" id="company_search" data-type="date_order" data-partner="{{$user->id}}">
                    </div>--}}
                </td>
                <td></td>
                <td></td>
{{--                <td></td>--}}
{{--                <td></td>--}}
            </tr>
            @foreach($promo_codes as $promo_code)
            @if($promo_code->user)
            <tr>
                <td>{{$promo_code->user->username}}</td>
                <td>{{$promo_code->code}}</td>
                @if($promo_code->order)
                    @if($promo_code->order->payed != 0)
                    <td>{{$promo_code->order->created_at}}</td>
                    <td>{{$promo_code->order->sum}}</td>
                    @else
                    <td>-</td>
                    <td>не оплачен</td>
                    @endif
                @else
                <td>-</td>
                <td>активирован промо код</td>
                @endif
{{--                <td>300</td>--}}
                <td class="hidden-xs"><a href="#">{{$promo_code->user->email}}</a></td>
{{--                <td class="hidden-xs">{{$promo_code->user->phone}}</td>--}}
            </tr>
            @endif
            @endforeach
        </table>

    </div>
</div>

<div class="p-tb-35">
    <div class="container">

        <div class="h2">Платежи по ссылкам</div>

        <table class="table table-responsive table-condensed table-cleared" id="partner_orders">
            <tr>
                <th>Компания</th>
                <th>Сумма оплаты</th>
                <th class="hidden-xs">E-mail</th>
            </tr>
            <tr class="filter-row hidden-xs">
{{--                <td>
                    <div class="form-group form-group-sm">
                        <input type="text" class="form-control" placeholder="поиск" onchange="searchFunctionOrders.call(this);" id="company_search" data-type="company_order" data-partner="{{$user->id}}">
                    </div>
                </td>--}}
{{--                <td>
                    <div class="form-group form-group-sm">
                        <input type="text" class="form-control" placeholder="поиск" onchange="searchFunctionOrders.call(this);" id="company_search" data-type="code_order" data-partner="{{$user->id}}">
                    </div>
                </td>--}}
                <td></td>
            </tr>
            @foreach($referral as $referrer)
            <tr>
                <td>{{$referrer->user->username}}</td>
                <td>{{Helpers::countPercentReferral($referrer->course->price, $referrer->user->referral_discount)}}</td>
                <td class="hidden-xs"><a href="#">{{$referrer->user->email}}</a></td>
            </tr>
            @endforeach
        </table>

    </div>
</div>
{{--<div class="send-invate">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-xs-12 send-invate-text">Отправьте приглашение сотруднику</div>
            <div class="col-md-6 col-xs-12 send-invate-form">
                <form class="form-inline">
                    <div class="form-group">
                        <label for="subscribe-email">E-mail:</label>
                        <input type="email" class="form-control" id="subscribe-email">
                    </div>
                    <button type="submit" class="btn btn-info">отправить</button>
                </form>                
            </div>
        </div><!-- .row -->
    </div><!-- .container -->
</div>--}}


<div class="bg-gray p-tb-35">
    <div class="container">
        
        <div class="h2">Информация по курсам:</div>
        <div class="row">
            <div class="col-xs-6 col-sm-3">
                <p>Архитектура</p>
                <a href="#">BIM. Основы платформы Revit</a><br>
                <a href="#">Revit Architecture</a><br>
                <a href="#">Revit Architecture</a>
            </div>
            <div class="col-xs-6 col-sm-3">
                <p>Конструкции</p>
                <a href="#">Revit Structure</a><br>
                <a href="#">ЛИРА-САПР</a>
            </div>
            <div class="col-xs-6 col-sm-3">
                <p>Инженерные сети</p>
                <a href="#">Revit MEP</a>
            </div>
            <div class="col-xs-6 col-sm-3">
                <p>Управление проектами</p>
                <a href="#">BIM. Основы платформы Revit</a><br>
                <a href="#">Naviswork</a>
            </div>
        </div>
    </div>
</div>
@if(Session::has('error'))
    <div class="modal fade" id="notif" tabindex="-1" role="dialog" aria-labelledby="notifLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Закрыть</span></button>
                <div class="modal-body">
                {{Session::get('error')}}
                </div>
            </div>
        </div>
    </div>
@endif
@stop