@extends('site.layouts.default')
{{-- Content --}}
@section('content')
<div class="breadcrumb-wrap">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-sm-12">
                <ol class="breadcrumb">
                    <li><a href="{{URL::to('/')}}">Главная</a></li>
                    <li class="active">Кабинет инструктора</li>
                </ol>
            </div>
            <div class="col-md-6 hidden-sm hidden-xs">
                <div class="row">
 {{--                    <div class="col-sm-18 phone text-center">
                        8(800) 555-20-72
                    </div>
                   <div class="col-sm-6">
                        <form class="search" role="search" action="">
                            <div class="input-group">
                                <input type="text" name="query" class="form-control">
                                <span class="input-group-btn">
                                    <button class="btn btn-default" type="submit">Поиск</button>
                                </span>
                            </div>
                        </form>
                    </div>--}}
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container p-b-35">
    <div class="row">
        <div class="col-xs-3 hidden-xs">
            @if(!$user->file)
                @if(!$user->gender || $user->gender == 1)
                    <img src="/assets/site/static/i/ava-muzh.png" class="img-responsive">
                @else
                    <img src="/assets/site/static/i/ava-zhen.png" class="img-responsive">
                @endif
            @else
                <img src="{{URL::to('uploads/users',$user->file)}}" class="img-responsive img-circle" width="180px">
            @endif
        </div>
        <div class="col-sm-9 col-xs-12">
            <h1 class="block-header" style="padding: 0 0 0.4em 0;">Кабинет инструктора</h1>
            <div class="user-info">
                <div class="user-name">ФИО: {{$user->username}}</div>
                <div>Всего обучающихся: <b>{{$count}}</b></div>
{{--                <div>Проходят обучение: <b><a href="#">29</a></b></div>--}}
                <div><a href="{{URL::to('user/get_edit', $user->id)}}" class="user-setting pull-left"><img src="/assets/site/static/i/icon-setting.png"></a></div>
            </div>
        </div>
    </div>
</div>
        
<div class="bg-gray p-tb-35">
    <div class="container">
        <div class="h2">Чат</div>
        <table class="table table-responsive table-condensed table-cleared table-striped table-hover">
            <tr>
                <th class="text-center">№</th>
                <th>ФИО студента</th>
                <th>Курс</th>
                <th class="text-center">Глава</th>
                <th class="text-center">Дата запроса</th>
                <th class="text-center">Ссылка на страницу</th>
            </tr>
            @foreach ($discussion as $key=>$item)
            <tr>
                <td class="text-center">{{$key+1}}</td>
                <td>{{$item->user->username}}</td>
                <td>{{$item->course->name}}</td>
                <td class="text-center">{{$item->chapter->name}}</td>
                <td class="text-center">{{$item->created_at}}</td>
                <td class="text-center"><a href="{{URL::to('discussion',array($item->course->id, $item->chapter->id, $item->user->id))}}">ссылка</a></td>
            </tr>
            @endforeach
        </table>
    </div>
</div>

{{--<div class="p-tb-35">
    <div class="container">
        <div class="h2">Консультации по главам, получасовые</div>
        <table class="table table-responsive table-condensed table-cleared table-striped table-hover">
            <tr>
                <th class="text-center">№</th>
                <th>ФИО студента</th>
                <th>Курс</th>
                <th class="text-center">Глава</th>
                <th class="text-center">Дата запроса</th>
                <th class="text-center">Дата консультации/время</th>
                <th class="text-center">Статус</th>
            </tr>
            <tr>
                <td class="text-center">1</td>
                <td><a href="#">Ким А.В.</a></td>
                <td>Revit MEP</td>
                <td class="text-center">4/7</td>
                <td class="text-center">20.10.15</td>
                <td class="text-center">21.10.15/17:15</td>
                <td class="text-center"><a href="#">ссылка</a></td>
            </tr>
        </table>
    </div>
</div>--}}
@stop