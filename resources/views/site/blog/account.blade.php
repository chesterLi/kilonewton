@extends('site.layouts.default')
{{-- Content --}}
@section('content')
<div class="breadcrumb-wrap">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-sm-12">
                <ol class="breadcrumb">
                    <li><a href="{{URL::to('/')}}">Главная</a></li>
                    <li class="active">Кабинет пользователя</li>
                </ol>
            </div>
            <div class="col-md-6 hidden-sm hidden-xs">
                <div class="row">
{{--                     <div class="col-sm-18 phone text-center">
                        8(800) 555-20-72
                    </div>
                   <div class="col-sm-6">
                        <form class="search" role="search" action="">
                            <div class="input-group">
                                <input type="text" name="query" class="form-control">
                                <span class="input-group-btn">
                                    <button class="btn btn-default" type="submit">Поиск</button>
                                </span>
                            </div>
                        </form>
                    </div>--}}
                </div>
            </div>
        </div>
    </div>
</div>
<div class="user-courses">
    <div class="container">
        <div class="row">
            <div class="col-md-3 col-sm-12 col-xs-12">
                <div class="user-photo">
                    <div class="row visible-md visible-lg">
                        <div class="col-md-12 col-xs-9">
                            <a href="{{URL::to('user/get_edit', $user->id)}}" {{-- data-toggle="modal" data-target="#edit-user" --}} class="user-setting pull-left"><img src="/assets/site/static/i/icon-setting.png"></a>
                            <div class="user-info">
                                <div class="user-name">{{$user->username}}</div>
                                <div class="user-rating">Рейтинг: {{$rating}}</div>
                            </div>
                        </div>
                        <div class="col-md-12 col-xs-3">
                        @if(!$user->file)
                            @if(!$user->gender || $user->gender == 1)
                                <img src="/assets/site/static/i/ava-muzh.png" class="img-responsive">
                            @else
                                <img src="/assets/site/static/i/ava-zhen.png" class="img-responsive">
                            @endif
                        @else
                            <img src="{{URL::to('uploads/users',$user->file)}}" class="img-responsive img-circle" width="180px">
                        @endif
                        </div>
                    </div>
                    <div class="row hidden-lg hidden-md">
                        <div class="col-xs-3">
                        @if(!$user->file)
                            @if(!$user->gender || $user->gender == 1)
                                <img src="/assets/site/static/i/ava-muzh.png" class="img-responsive">
                            @else
                                <img src="/assets/site/static/i/ava-zhen.png" class="img-responsive">
                            @endif
                        @else
                            <img src="{{URL::to('uploads/users',$user->file)}}" class="img-responsive img-circle">
                        @endif
                        </div>
                        <div class="col-xs-9">
                            <a href="{{URL::to('user/get_edit', $user->id)}}" {{-- data-toggle="modal" data-target="#edit-user" --}} class="user-setting pull-left"><img src="/assets/site/static/i/icon-setting.png"></a>
                            <div class="user-info">
                                <div class="user-name">{{$user->username}}</div>
                                <div class="user-rating">Рейтинг: {{$rating}}</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-9 col-xs-12 courses-started">
                <div class="h1">Доступные курсы:</div>
                <div class="row">
                @foreach($userCourses as $userCourse)
                    <div class="col-md-4 col-xs-6"><a href="{{URL::to('course', $userCourse->course_id)}}">{{Helpers::getCourseName($userCourse->course_id)}}</a></div>
                @endforeach
                </div>
            </div>

            <div class="col-md-9 col-xs-12 courses-started">
                <div class="h1">Начатые курсы:</div>
                <div class="row">
                    <div class="col-md-4 col-xs-6 col-header">Название курса</div>
                    <div class="col-md-4 col-xs-3 col-header">Пройдено глав</div>
                    <div class="col-md-4 col-xs-3 col-header">Заработано баллов</div>
                </div>
                @foreach($stat as $item)
                <div class="row">
                    <div class="col-md-4 col-xs-6"><a href="{{URL::to('course', $item->course->id)}}">{{$item->course->name}}</a></div>
                    <div class="col-md-4 col-xs-3">
                        <span class="score">{{$item->chapters}}/{{$item->course->chapters->count()}}</span>
                        <div class="progress progress-rounded">
                            <div class="progress-bar progress-bar-success chapters-bar" role="progressbar" data-chapters="{{$item->course->chapters->count()}}" data-user="{{$item->chapters}}"></div>
                        </div>
                    </div>
                    <div class="col-md-4 col-xs-3">
                        <span class="score"></span>
                        <div class="progress progress-rounded">
                            <div class="progress-bar progress-bar-success points-bar" role="progressbar" data-points="{{$item->points}}" data-course="{{$item->course->id}}"></div>
                        </div>
                    </div>
                </div>
                @endforeach
{{--                @foreach($started_courses as $started_course)
                <div class="row">
                    <div class="col-md-4 col-xs-6"><a href="{{URL::to('course', $started_course->course->id)}}">{{$started_course->course->name}}</a></div>
                    <div class="col-md-4 col-xs-3">
                        <span class="score">{{$started_course->chapters}}/{{$started_course->course->chapters->count()}}</span>
                        <div class="progress progress-rounded">
                            <div class="progress-bar progress-bar-success chapters-bar" role="progressbar" data-chapters="{{$started_course->course->chapters->count()}}" data-user="{{$started_course->chapters}}"></div>
                        </div>
                    </div>
                    <div class="col-md-4 col-xs-3">
                        <span class="score"></span>
                        <div class="progress progress-rounded">
                            <div class="progress-bar progress-bar-success points-bar" role="progressbar" data-points="{{$started_course->points}}" data-course="{{$started_course->course->id}}"></div>
                        </div>
                    </div>
                </div>
                @endforeach--}}
            </div>
        </div>
    </div>
</div><!-- .user-courses -->

<div class="user-courses-stat bg-gray">
    <div class="container">
        
        <div class="h2">Результаты проверок</div>
        
        <div class="courses-ended">
            <div class="row">
                <div class="col-sm-3 col-xs-4 col-header">Название курса</div>
                <div class="col-sm-3 col-xs-4 col-header">Вид контроля</div>
                <div class="col-xs-2 hidden-xs col-header">Результат в баллах</div>
                <div class="col-xs-2 hidden-xs col-header">Дата и время</div>
                <div class="col-sm-2 col-xs-4 col-header">Статус</div>
            </div>
            @foreach($tests as $test)
            <div class="row">
                <div class="col-sm-3 col-xs-4"><a href="{{URL::to('course', $test->course_id)}}">{{$test->course->name}}</a></div>
                <div class="col-sm-3 col-xs-4">Тест к главе {{$test->chapter->name}}</div>
                <div class="col-xs-2 hidden-xs">
                    <span class="score">{{$test->right_answers}}/5</span>
                    <div class="progress progress-rounded">
                        <div class="progress-bar progress-bar-success test-bar" role="progressbar" data-correct="{{$test->right_answers}}"></div>
                    </div>
                </div>
                <div class="col-xs-2 hidden-xs">{{$test->created_at}}</div>
                @if($test->passed != 1)
                <div class="col-sm-2 col-xs-4"><img src="/assets/site/static/i/close-inverse.png"> Пересдать</div>
                @else
                <div class="col-sm-2 col-xs-4"><img src="/assets/site/static/i/icon-success-inverse.png"> Сдано</div>
                @endif
            </div>
            @endforeach
        </div>        
        
        
        
        <div class="h2">Рекомендуемые курсы</div>
        @if(!isset($recommend))
        Выберите <a href="{{URL::to('courses')}}">курс</a>.
        @else
        <div class="row courses-list">
        @foreach($recommend as $course)
            @if($course->category_id == 5)
                <div class="col-md-3 col-sm-4 col-xs-6 mix category-1">
                <div class="courses-item bg-green">
            @elseif($course->category_id == 6)
                 <div class="col-md-3 col-sm-4 col-xs-6 mix category-2">
                 <div class="courses-item bg-blue">
            @elseif($course->category_id == 7)
                 <div class="col-md-3 col-sm-4 col-xs-6 mix category-3">
                 <div class="courses-item bg-yellow">
            @else
                 <div class="col-md-3 col-sm-4 col-xs-6 mix category-4">
                 <div class="courses-item bg-red">
            @endif
                   <div class="courses-title">
                       <h2>{{$course->name}}</h2>
                   </div>
                   <div class="courses-content">
                       <p class="author">{{$course->author->username}}</p>
                       <p class="finished">Курс прошли 23 человека</p>
                        <a href="{{ URL::to('course', $course->id) }}" class="courses-start">начать бесплатно<span class="glyphicon glyphicon-chevron-right pull-right" aria-hidden="true"></span></a>
                   </div>
               </div>
           </div>
        @endforeach
       </div><!-- .courses-list -->
       @endif
    </div>
</div><!-- .user-courses-statt -->

<div class="modal fade" id="edit-user">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <form class="form-horizontal" method="post" id="edit-user" enctype="multipart/form-data">
                    <div class="form-group">
                        <h4 class="text-center">Редактирование</h4>
                    </div>
                    <div id="edit-errors"></div>
                    <h4>Личные данные</h4>
                    <div class="form-group" id="email">
                        <label for="email" class="col-xs-3">Email:</label>
                        <div class="col-sm-9">
                            <input type="text" name="email" class="form-control" value="{{{ Input::old('email', isset($user) ? $user->email : null) }}}">
                        </div>
                    </div>
                    <div class="form-group" id="username">
                        <label for="username" class="col-xs-3">ФИО:</label>
                        <div class="col-sm-9">
                            <input type="text" name="username" class="form-control" value="{{{ Input::old('username', isset($user) ? $user->username : null) }}}">
                        </div>
                    </div>
                    <div class="form-group" id="gender">
                        <label for="gender" class="col-xs-3">Пол:</label>
                        <div class="col-sm-9 form-inline">
                            Муж {{ Form::radio('gender', '1', (Input::old('gender') == '1'), array('class'=>'radio')) }}
                            Жен {{ Form::radio('gender', '2', (Input::old('gender') == '2'), array('class'=>'radio')) }}
                        </div>
                    </div>
                    <div class="form-group" id="phone">
                        <label for="name" class="col-xs-3">Телефон:</label>
                        <div class="col-sm-9">
                            <input type="text" name="phone" class="form-control" value="{{{ Input::old('phone', isset($user) ? $user->phone : null) }}}">
                        </div>
                    </div>
                    <div class="form-group" id="photo">
                        <label for="photo" class="col-xs-3">Аватар:</label>
                        <div class="col-sm-9">
                           <input type="file" name="photo" id="photo">
                        </div>
                    </div>
                    <h4>Сменить пароль</h4>
                    <div class="form-group" id="old_password">
                        <label for="password" class="col-xs-3">Старый пароль:</label>
                        <div class="col-sm-9">
                            <input type="password" name="old_password" class="form-control">
                        </div>
                    </div>
                    <div class="form-group" id="password">
                        <label for="password" class="col-xs-3">Новый пароль:</label>
                        <div class="col-sm-9">
                            <input type="password" name="password" class="form-control">
                        </div>
                    </div>
                    <div class="form-group" id="password_confirmation">
                        <label for="password" class="col-xs-3">Повторить пароль:</label>
                        <div class="col-sm-9">
                            <input type="password" name="password_confirmation" class="form-control">
                        </div>
                    </div>
                    <input type="hidden" value="{{Sentry::getUser()->id}}" name="user_id">
                    <div class="form-group">
                        <div class="col-sm-12">
                            <button type="submit" class="btn btn-danger center-block">Сохранить</button>
                        </div>
                    </div>
                </form>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
@stop