@extends('site.layouts.default')
{{-- Content --}}
@section('content')
    <div class="breadcrumb-wrap">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-sm-12">
                    <ol class="breadcrumb">
                        <li><a href="{{URL::to('/')}}">Главная</a></li>
                        <li><a href="{{URL::to('courses')}}">Все курсы</a></li>
                        <li class="active">{{$course->name}}</li>
                    </ol>
                </div>
                <div class="col-md-6 hidden-sm hidden-xs">
                    <div class="row">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="course-header bg-gray">
        <div class="container text-center">
            <h1>{{$course->name}}
                @if($course->price == 0)
                    <span class="label label-default"
                          style="background-color: #d9534f; font-family: arial; font-size: 12px; position:relative; top:-10px;">Бесплатно</span>
                @endif
            </h1>
            @if($course->price != 0)
                <div class="progress-title">Выполнено {{$user_chapters}} из {{$course_chapters}} тестов ({{$percentage}}
                    %)
                </div>
                <div class="progress">
                    <div class="progress-bar" role="progressbar" aria-valuenow="{{$percentage}}" aria-valuemin="0"
                         aria-valuemax="100" style="width: {{$percentage}}%;"></div>
                </div>
            @endif
            <div>{{$course->description}}</div>
            @if($course->block_test != 1)
                <br/>
                <a href="{{URL::to('getTests', $course->id)}}"
                   style="background-color: #45adcd; color:#fff; border-color:#4570CD" class="btn btn-lg btn-default">Предварительное
                    тестирование</a>
            @endif
            <hr>
            <div class="row course-include">
                <div class="col-md-12 col-md-offset-0 col-sm-10 col-sm-offset-1">
                    <div class="row">
                        <div class="col-sm-4 col-xs-12">разделов: {{$sections}}</div>
                        <div class="col-sm-4 col-xs-12">уроков: {{$videos_count}}</div>
                        {{--
                                            <div class="col-sm-3 col-xs-12">тестов: {{$interactive_count}} </div>
                        --}}
                        @if($course->block_cert != 1)
                            <div class="col-sm-4 col-xs-12">Сертификат по окончанию</div>
                        @endif
                    </div>
                </div>
            </div>
        </div><!-- .container -->
    </div><!-- .course-header -->

    @if($course->block_consult != 1)
        <div class="instructor">
            <div class="container">
                <div class="photo pull-left">
                    @if(!$instructor->file)
                        @if(!$instructor->gender || $instructor->gender == 1)
                            <img src="/assets/site/static/i/ava-muzh.png" class="img-responsive">
                        @else
                            <img src="/assets/site/static/i/ava-zhen.png" class="img-responsive">
                        @endif
                    @else
                        <img src="{{URL::to('uploads/users',$instructor->file)}}" class="img-responsive img-circle"
                             width="180px">
                    @endif
                </div>
                <div class="text">
                    <h4>Ваш инструктор: <span>{{$instructor->username}}</span></h4>
                    <p>Добрый день! Ниже вы увидите список разделов курса. Каждый раздел состоит из уроков, интерактива
                        и проверочного задания. Для того, чтобы раздел был засчитан, Вам необходимо выполнить тест,
                        расположенный внутри каждой главы. Вопросы вы можете высылать на почту: <b>alex@kilonewton
                            .ru</b></p>
                    {{--<a href="#" class="btn btn-primary add-answer">задать вопрос</a>--}}
                </div>
            </div><!-- .container -->
        </div><!-- .instructor-->
    @endif

    {{--<div class="course-benefit bg-gray text-center">
        <div class="container">
            <div class="row">
                <div class="col-sm-3 col-xs-6">
                    <img src="/assets/site/static/i/benefit-1.png" class="img-responsive center-block" />
                    Онлайн-лекции с преподавателями-практиками помогают приобрести обширные теоретические знания.
                </div>
                <div class="col-sm-3 col-xs-6">
                    <img src="/assets/site/static/i/benefit-2.png" class="img-responsive center-block" />
                    Интерактивные задания позволяют улучшить профессиональные навыки работы в программе. Доступны с любого компьютера без установки программного обеспечения.
                </div>
                <div class="col-sm-3 col-xs-6">
                    <img src="/assets/site/static/i/benefit-3.png" class="img-responsive center-block" />
                    Выполнение проверочных работ позволяет увидеть результаты занятий, выявить пробелы и закрепить полученные знания.
                </div>
                <div class="col-sm-3 col-xs-6">
                    <img src="/assets/site/static/i/benefit-4.png" class="img-responsive center-block" />
                    Онлайн-консультации с преподавателями снимают вопросы, возникающие в ходе обучения.
                </div>
            </div>
        </div><!-- .container -->
    </div><!-- .course-benefit -->--}}
    @if($course->price != 0)
        <div class="course-buy-line">
            <div class="container">
                @if(Auth::check())
                    @if(!empty(Auth::user()->referral_discount) && Auth::user()->referral_lifetime > \Carbon\Carbon::now())
                        <div class="col-md-5" style="color: #3e4649"><a href="#" data-toggle="modal"
                                                                        data-target="#course-content">Полное содержание
                                курса</a></div>
                        <div class="col-md-7"><b class="price"
                                                 style="text-decoration: line-through; color: rgba(0, 0, 0, 0.98)">{{$course->price}}
                                <span>a</span></b><b class="price"
                                                     style="margin-right: 75px;">{{  Helpers::countPercentReferral($course->price, Auth::user()->referral_discount)}}
                                <span>a</span></b>
                            @if(!(Auth::user()->hasCourse($course->id)))
                                <a href="{{URL::to('cart', $course->id)}}" class="btn btn-lg" id="buy_course1"
                                   style="background: #ff714f; color: #ffffff"
                                   data-price="{{\App\Helpers\Helpers::countPercentReferral($course->price, Auth::user()->referral_discount)}}"
                                   data-course="{{$course->id}}" data-name="{{$course->name}}">Купить курс</a></div>
                    @endif
                @else
                    <div class="col-md-5" style="color: #3e4649"><a href="#" data-toggle="modal"
                                                                    data-target="#course-content">Полное содержание
                            курса</a></div>
                    <div class="col-md-7"><b class="price" style="margin-right: 90px;">{{$course->price}}<span>a</span></b>
                        <a href="{{URL::to('cart', $course->id)}}" class="btn btn-lg" id="buy_course1"
                           style="background: #ff714f; color: #ffffff" data-price="{{$course->price}}"
                           data-course="{{$course->id}}" data-name="{{$course->name}}">Купить курс</a></div>
                @endif
                @else
                    {{--
                                <div class="col-md-7" style="color:#3e4649"><a href="#" data-toggle="modal" data-target="#sign-in">Авторизуйтесь</a> или <a href="#" data-toggle="modal" data-target="#reg">зарегистрируйтесь</a>, чтобы приобрести курс</div>
                    --}}
                    <a href="#" data-toggle="modal" data-target="#course-content">Полное содержание курса</a>
                    <div class="col-md-5"><span style="color:#3e4649 ">Стоимость курса:</span> <b
                                class="price">{{$course->price}}<span>a</span></b></div>
                @endif
            </div>
        </div><!-- .course-buy-line -->
    @endif
    @if($course->block_test != 1)
        <div class="container">
            <div class="row part-header container">
                <h2><a href="{{URL::to('getTests', $course->id)}}">Предварительное тестирование</a></h2>
            </div>
            <div class="row text-left container">
                Предварительное тестирование поможет понять, каким разделам вам стоит уделить особое внимание при
                изучении курса. Тестирование совершенно бесплатно.<br/>
                <div class="divider-line" style="background:#45adcd"></div>
            </div>


        </div>
    @endif

    @foreach ($chapters as $i=>$chapter)
        {{--
        @if(!in_array(Auth::id(),$chapter->owners->lists('id')) and $chapter->promo != 1 and $instructor == false and $author == false)
        --}}
        @if(!Acl::isAllowed($user, $chapter, 'view'))
            <div class="course-parts    disabled bg-gray">
                <div class="container">
                    <div class="row part-header">
                        <div class="col-xs-8 col-md-8">
                            <div class="pull-left part-number">{{$i+1}}.</div>
                            <h2>{{$chapter->name}}</h2>
                        </div>
                        @if($chapter->price != 0)
                            <div class="col-xs-4 col-md-4 part-status">
                                @if($chapter->price)
                                    <b class="price">{{$chapter->price}}<span>a</span></b>
                                    {{--                <b class="price" style="text-decoration: line-through; color: #000000">{{$chapter->price}}<span>a</span></b>
                                                    <b class="price" style="color: #b3282d">{{ Helpers::countPercent($chapter->price)}}<span>a</span></b>--}}
                                @endif
                                @if(!\Gloudemans\Shoppingcart\Facades\Cart::search(array('id' => $chapter->id)))
                                    <a href="#" data-rowid="" data-name="{{$chapter->name}}"
                                       data-chapter="{{$chapter->id}}" data-course="{{$chapter->course_id}}"
                                       class="btn btn-lg btn-info hidden-xs add-cart">добавить в набор</a>
                                @else
                                    <a href="#" data-rowid="" data-chapter=""
                                       class="btn btn-lg btn-info hidden-xs disabled">добавлено в набор</a>
                                @endif
                            </div>
                        @endif
                    </div>
                    @else
                        <div class="course-parts">
                            <div class="container">
                                <div class="row part-header">
                                    <div class="col-xs-9 col-md-8">
                                        <div class="pull-left part-number">{{$i+1}}.</div>
                                        <h2>
                                            <a href="{{URL::to('chapter',array($id,$chapter->id))}}">{{$chapter->name}}</a>
                                        </h2>
                                    </div>
                                    <div class="col-xs-3 col-md-4 part-status text-right">Доступно для изучения</div>
                                </div>
                                @endif
                                <div class="divider-line"></div>
                                <div class="row part-include">
                                    <div class="col-xs-6 part-lessons-count">Уроков: {{$chapter->materials->count()}}
                                        @if($interactive_count != 0) {{-- + интерактивный практикум--}}
                                        @endif
                                    </div>
                                    {{--<div class="col-xs-3 part-consultate text-center">Консультация проведена 28.11.14</div>
                                    <div class="col-xs-3 part-status text-right">Выполнено</div>--}}
                                </div>

                                <div class="lessons-list w-carousel">
                                    @foreach($chapter->materials as $material)
                                        {{--                @if(!Acl::isAllowed($user, $chapter, 'view'))
                                                            <a href="#" class="lesson-item not-active">
                                                        @else--}}
                                        <a href="{{URL::to('chapter',array($id,$chapter->id))}}" class="lesson-item">
                                            {{--                @endif--}}
                                            <div class="lesson-title">
                                                <h3>{{$material->name}}</h3>
                                            </div>
                                        </a>
                                    @endforeach
                                </div><!-- .lessons-list -->
                            </div><!-- .container -->
                        </div><!-- .course-parts -->
                        @endforeach
                        <div class="container">
                            <div class="row">
                                <div class="text-right">
                                    <div class="pluso" data-background="transparent"
                                         data-options="medium,square,line,horizontal,counter,theme=06"
                                         data-services="vkontakte,facebook,twitter,linkedin,odnoklassniki,moimir,google"></div>
                                </div>
                            </div>
                        </div>
                        @if($course->block_cert != 1)
                            <div class="course-sertificate bg-gray">
                                <div class="container">
                                    <div class="row">
                                        <div class="col-lg-8 col-lg-offset-4 col-md-7 col-md-offset-5 col-sm-6 col-sm-offset-6 col-xs-10 col-xs-offset-1">
                                            <p>Для получения <a href="#" style="font-size:18px;" data-toggle="modal"
                                                                data-target="#certificate">сертификата</a> об окончании
                                                курса необходимо сдать финальное задание. Для получения доступа к
                                                финальному заданию нужно успешно пройти более 80% тестовых заданий.</p>
                                            @if($percentage < 80)
                                                <a href="#" class="btn btn-lg btn-primary disabled">финальное
                                                    задание</a>
                                            @else
                                                <a href="{{URL::to('final', $course->id )}}"
                                                   class="btn btn-lg btn-primary">финальное задание</a>
                                            @endif
                                        </div>
                                    </div>
                                </div><!-- .container -->
                            </div><!-- .course-sertificate -->
                        @endif
                        @if($course->price != 0)
                            <div class="bg-dark course-buy-line">
                                <div class="container">
                                    @if(!Auth::check())
                                        <div class="col-xs-12 col-sm-6 buy-attemption">
                                            В данном курсе оплачен доступ не ко всем главам.<br/>
                                            Вы можете получить доступ, докупив их
                                        </div>
                                    @endif
                                    @if(Auth::check())
                                        <div class="col-xs-12 col-sm-6 buy-price" id="chapters-sum">
                                            @if(\App\Helpers\Helpers::cartTotal($course->id) == !0)
                                                <b class="price">{{\App\Helpers\Helpers::cartTotal($course->id)}}<span>a</span></b><a
                                                        href="{{URL::to('cart')}}" class="btn btn-lg btn-info">Купить
                                                    набор</a>
                                                {{--           @else
                                                                <b class="price">{{Helpers::cartTotal($course->id)}}<span>a</span></b><a href="{{URL::to('cart')}}" class="btn btn-lg btn-info disabled">Купить набор</a>--}}
                                            @endif
                                        </div>
                                        <div class="col-xs-12 col-lg-6 col-lg-offset-6 buy-price full-price">
                                            @if(!empty(Auth::user()->referral_discount) && Auth::user()->referral_lifetime > \Carbon\Carbon::now())
                                                <b class="price"
                                                   style="text-decoration: line-through; color: #ffffff">{{$course->price}}
                                                    <span>a</span></b>
                                                <b class="price"
                                                   style="background: #ff714f; color: #ffffff">{{\App\Helpers\Helpers::countPercentReferral($course->price, Auth::user()->referral_discount)}}
                                                    <span>a</span></b> <a href="{{URL::to('cart', $course->id)}}"
                                                                          class="btn btn-lg btn-success" id="buy_course"
                                                                          data-price="{{\App\Helpers\Helpers::countPercentReferral($course->price, Auth::user()->referral_discount)}}"
                                                                          data-course="{{$course->id}}"
                                                                          data-name="{{$course->name}}">Купить курс</a>
                                            @else
                                                <b class="price">{{$course->price}}<span>a</span></b>  <a
                                                        href="{{URL::to('cart', $course->id)}}"
                                                        style="background: #ff714f; color: #ffffff" class="btn btn-lg"
                                                        id="buy_course" data-price="{{$course->price}}"
                                                        data-course="{{$course->id}}" data-name="{{$course->name}}">Купить
                                                    курс</a>
                                            @endif
                                        </div>
                                    @else
                                        <a href="#" data-toggle="modal" data-target="#sign-in">Авторизуйтесь</a> или <a
                                                href="#" data-toggle="modal" data-target="#reg">зарегистрируйтесь</a>,
                                        чтобы приобрести курс
                                        Стоимость курса: <b class="price">{{$course->price}}<span>a</span></b>
                                    @endif
                                </div>
                            </div><!-- .course-buy-line -->

                            <div class="container advantage text-center">
                                <div class="row">
                                    <div class="col-xs-4">
                                        <img src="/assets/site/static/i/advantage-1.png"
                                             class="img-responsive center-block"/>
                                        Если за 72 часа вы разочаруетесь в курсе, мы полностью вернем его стоимость
                                    </div>
                                    <div class="col-xs-4">
                                        <img src="/assets/site/static/i/advantage-2.png"
                                             class="img-responsive center-block"/>
                                        Индивидуальные консультации доступны в течении 90 дней с момента оплаты курса
                                    </div>
                                    <div class="col-xs-4">
                                        <img src="/assets/site/static/i/advantage-3.png"
                                             class="img-responsive center-block"/>
                                        По окончании каждого курса выдается сертификат Autodesk международного образца
                                    </div>
                                </div>
                            </div><!-- .advantage -->
                            <div class="modal fade" id="course-content" tabindex="-1" role="dialog"
                                 aria-labelledby="notifLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <button type="button" class="close" data-dismiss="modal"><span
                                                    aria-hidden="true">&times;</span><span
                                                    class="sr-only">Закрыть</span></button>
                                        <div class="modal-header">Полное содержание курса</div>
                                        <div class="modal-body">
                                            @foreach($course->chapters as $key=>$chapter)
                                                <div class="row top-buffer">
                                                    <div class="col-md-12">
                                                        <a href="#"
                                                           class="toggle-chapter">{{$key+1 .'. '. $chapter->name}}</a>
                                                        <ul style="display: none">
                                                            @foreach($chapter->materials as $mat)
                                                                <li>{{ $mat->name }}</li>
                                                            @endforeach
                                                        </ul>
                                                    </div>
                                                </div>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            </div>
        @endif
@stop