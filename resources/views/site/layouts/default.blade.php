<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="Cache-control" content="public">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="icon"
          type="image/png"
          href="/assets/site/static/i/favicon.ico">

    @if(isset($seo))
    <title>{{ $seo->title }}</title>
    <meta name="keywords" content="{{ $seo->meta_keywords }}">
    <meta name="description" content="{{ $seo->description }}">
    <meta property="og:url" content="{{ $seo->og_url }}">
    <meta property="og:title" content="{{ $seo->og_title }}">
    <meta property="og:description" content="{{ $seo->og_description}} ">
    @else
    <title>Kilonewton - дистанционное образование для архитекторов, инженеров, конструкторов и проектировщиков. AutoCAD, Revit, Robot, Лира, SCAD.</title>
    <meta name="description" content="Kilonewton разрабатыватывает и совершенствует онлайн-курсы по AutoCAD, Revit, Robot, Лира, SCAD и другим популярным CAD- и CAE-комплексам.">
    <meta name="keywords" content="автокад уроки, autocad уроки, автокад самоучитель, курсы автокад, автокад для чайников, revit уроки, курсы архитектора, уроки revit, ревит уроки, уроки ревит, ревит уроки, revit видеоуроки, курсы revit, повышение квалификации инженеров, обучение revit, видеоуроки revit, revit architecture уроки, видеоуроки revit">
    @endif

    <meta property="og:url"           content="https://www.kilonewton.ru" />
    <meta property="og:type"          content="website" />
    <meta property="og:title"         content="Kilonewton" />
    <meta property="og:description"   content="Kilonewton разрабатыватывает и совершенствует онлайн-курсы по AutoCAD, Revit, Robot, Лира, SCAD и другим популярным CAD- и CAE-комплексам." />
    <meta property="og:image"         content="https://kilonewton.ru/assets/site/static/i/logo.png" />

    <link href="/assets/site/static/bootstrap/css/bootstrap.min.css" rel="stylesheet" async>
    <link href="/assets/site/static/css/bootstrap-theme.css" rel="stylesheet" async defer>
    <link href="/assets/site/static/css/style.css" rel="stylesheet" async>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <script type="text/javascript">var login = "<?= Session::get('login_error') ?>";</script>

    <!-- RedHelper -->
    <script id="rhlpscrtg" type="text/javascript" charset="utf-8" async="async"
    	src="https://web.redhelper.ru/service/main.js?c=kln">
    </script>
    <!--/Redhelper -->

    <!-- Facebook -->
    <div id="fb-root"></div>
    <script>(function(d, s, id) {
      var js, fjs = d.getElementsByTagName(s)[0];
      if (d.getElementById(id)) return;
      js = d.createElement(s); js.id = id;
      js.src = "//connect.facebook.net/ru_RU/sdk.js#xfbml=1&version=v2.4";
      fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>
    <!--/Facebook -->
    </head>
<body>
<nav class="navbar navbar-default {{Request::is('/') ? 'navbar-main' : ''}}">
    <div class="container">
        <div class="navbar-header" style="margin-bottom:10px">
{{--            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#header-nav">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>--}}
            @if(Request::is('/'))
            <div><a class="navbar-brand" href="{{URL::to('/')}}"><img src="/assets/site/static/i/logo_background.png"/></a></div>
            <div><a href="{{URL::to('/blog/71')}}" target="_blank"><img src="/assets/site/static/i/autodesk_top.png"/></a></div>
            @else
            <a class="navbar-brand" href="{{URL::to('/')}}"><img src="/assets/site/static/i/logo.png"/></a>
            @endif

        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse navbar-right" id="header-nav">
            <ul class="nav navbar-nav">
                <li><a href="{{URL::to('courses')}}">Все курсы</a></li>
                <li class="divider">|</li>
                <li><a href="{{URL::to('experts')}}">Эксперты</a></li>
                <li class="divider">|</li>
                <li><a href="{{URL::to('blog')}}">Статьи</a></li>
                @if(!Auth::check())
                <li class="sign-in"><a href="#" data-toggle="modal" data-target="#sign-in">войти</a></li>
                <li class="reg"><a href="#" data-toggle="modal" data-target="#reg">зарегистрироваться</a></li>
                @else
                <li class="divider">|</li>
                @if(Auth::user()->role == 'hr')
                    <li><b>Здравствуйте, <a href="{{URL::to('cabinet_hr', Auth::user()->id)}}">{{Auth::user()->username}}</a>!</b></li>
                @elseif(Auth::user()->role == 'author')
                    <li><b>Здравствуйте, <a href="{{URL::to('cabinet_author', Auth::user()->id)}}">{{Auth::user()->username}}</a>!</b></li>
                @elseif(Auth::user()->role == 'instructor')
                    <li><b>Здравствуйте, <a href="{{URL::to('cabinet_instructor', Auth::user()->id)}}">{{Auth::user()->username}}</a>!</b></li>
                @elseif(Auth::user()->role == 'partner')
                    <li><b>Здравствуйте, <a href="{{URL::to('cabinet_partner', Auth::user()->id)}}">{{Auth::user()->username}}</a>!</b></li>
                @else
                    <li><b>Здравствуйте, <a href="{{URL::to('account', Auth::user()->id)}}">{{Auth::user()->username}}</a>!</b></li>
                @endif
                <li><a href="{{URL::to('user/logout')}}">Выход</a></li>
                @endif
            </ul>
        </div><!-- .navbar-collapse -->
    </div>
</nav><!-- .navbar -->

<div class="modal fade" id="sign-in">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-body">
                <form class="form-horizontal" method="post" id="login" {{--action="{{ URL::to('user/login') }}" --}} accept-charset="UTF-8">
                    <div class="form-group">
                        <h4 class="text-center">Вход</h4>
                    </div>
                    <div id="login-errors"></div>
                    <div class="form-group">
                        <div class="col-xs-12">
                            <div class="input-group">
                                <span class="input-group-addon" id="sizing-addon3"><span class="glyphicon glyphicon glyphicon-user"></span></span>
                                <input type="text" name="email" class="form-control" placeholder="логин">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-xs-12">
                            <div class="input-group">
                                <span class="input-group-addon" id="sizing-addon3"><span class="glyphicon glyphicon glyphicon-lock"></span></span>
                                <input type="password" name="password" class="form-control" placeholder="пароль">
                                <input type="hidden" name="csrf-token" value="{{ csrf_token() }}">
                            </div>
                        </div>
                    </div>
                    <a href="{{URL::to('user/forgot')}}">Забыли пароль?</a>
                    <div class="form-group">
                        <div class="col-sm-12">
                            <button type="submit" class="btn btn-danger center-block">Войти</button>
                        </div>
                    </div>
                </form>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<div class="modal fade" id="reg">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <form class="form-horizontal" method="post" id="register" accept-charset="UTF-8">
                    <div class="form-group">
                        <h4 class="text-center">Регистрация</h4>
                    </div>
                    <div id="reg-errors"></div>
                    <div class="form-group" id="email">
                        <label for="email" class="col-xs-4">Email:</label>
                        <div class="col-sm-8">
                            <input type="text" name="email" class="form-control">
                        </div>
                    </div>
                    <div class="form-group" id="password">
                        <label for="password" class="col-xs-4">Пароль:</label>
                        <div class="col-sm-8">
                            <input type="password" name="password" class="form-control">
                        </div>
                    </div>
                    <div class="form-group" id="name">
                        <label for="name" class="col-xs-4">ФИО (Название комании):</label>
                        <div class="col-sm-8">
                            <input type="text" name="name" class="form-control">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="phone" class="col-xs-4">Телефон:</label>
                        <div class="col-sm-8">
                            <input type="text" name="phone" class="form-control">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="legal_entity" class="col-xs-4">Юридическое лицо:</label>
                        <div class="col-sm-8">
                            {{ Form::checkbox('legal_entity') }}
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-12">
                            <button type="submit" class="btn btn-danger center-block">Зарегистрироваться</button>
                        </div>
                    </div>
                </form>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<div class="modal fade" id="certificate">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-body">
                <img src="/assets/site/static/i/certificate.jpg" style="margin-left:35px; width: 800px; height: 600px;">
            </div>
        </div>
    </div>
</div>
    @yield('content')
<div class="footer">
    <div class="container">
        <div class="row">
            <div class="col-lg-7 col-md-8 col-xs-12">
                <div class="row">
                    <div class="col-xs-4">
                        <b>KiloNewton</b>
                        <ul>
                            <li><a href="{{URL::to('about')}}">О компании</a></li>
                            <li><a href="{{URL::to('payment_instruction')}}">Способы оплаты</a></li>
{{--                            <li><a href="{{URL::to('page/8')}}">Команда</a></li>--}}
                            <li><a href="{{URL::to('experts')}}">Эксперты</a></li>
                            {{--<li><a href="{{URL::to('page/2')}}">Вакансии</a></li>--}}
                            <li><a href="{{URL::to('contacts')}}">Контакты</a></li>
{{--
                            <li><a href="{{URL::to('/blog/71')}}" target="_blank"><img src="/assets/site/static/i/autodesk.png"/></a></li>
--}}

                        </ul>
                    </div>
                    <div class="col-xs-4">
                        <b>Деятельность</b>
                        <ul>
                            <li><a href="{{URL::to('courses')}}">Готовые курсы</a></li>
                            @if(\App\Helpers\Helpers::getLocation())
                            <li><a href="{{URL::to('offline')}}">Oчное обучение</a></li>
                            @endif
                            <li><a href="{{URL::to('page/5')}}">Курсы под заказ</a></li>
                            <li><a href="{{URL::to('blog')}}">Статьи</a></li>
                            <li><a href="{{URL::to('page/7')}}">Оферта</a></li>
                        </ul>
                    </div>
                    <div class="col-xs-4">
                        <b>Партнерство</b>
                        <ul>
                            {{--<li><a href="{{URL::to('/')}}">Ключевые партнеры</a></li>--}}
                            <li><a href="{{URL::to('page/10')}}">Ищем экспертов</a></li>
                            {{--<li><a href="{{URL::to('page/9')}}">Акции</a></li>
                            <li><a href="{{URL::to('/')}}">Анонсы мероприятий</a></li>--}}
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-lg-5 col-md-4 col-xs-12">
{{--
                <div class="phone">8(800) 555-20-72 <span>для РФ</span></div>
--}}
                <div class="phone">+7 812 622 10 14 </br>Екатерина Величко</div>
                <div class="social-links">
                    <a href="https://www.facebook.com/KiloNewton.ru?ref=bookmarks" target="_blank" class="fb"></a>
                    <a href="https://vk.com/kilonewton" target="_blank" class="vk"></a>
                    {{--<a href="#" class="in"></a>--}}
                    <a href="https://plus.google.com/u/0/+KilonewtonRuplus/posts" target="_blank" class="gp"></a>
                </div>
            </div>
        </div>
            <div class="row text-center" style="margin-bottom:-43px; padding-top:70px;">
                <span style="font-size: 10px">© 2014-2017 ТОО КИЛОНЬЮТОН. Все права защищены.</span>
            </div>
    </div>

</div>

<div class="modal fade" id="student" tabindex="-1" role="dialog" aria-labelledby="notifLabel" aria-hidden="true">
    <div class="modal-dialog" style="width: 310px;">
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Закрыть</span></button>
            <div class="modal-header">Получи скидку 50%!</div>
            <div class="modal-body">
                <form method="POST" id="student-form">
                <div id="student-message"></div>
                    <div class="form-group">
                        <label>Имя</label>
                        <input type="text" name="name" class="form-control" required="true">
                    </div>
                    <div class="form-group">
                        <label>Email</label>
                        <input type="text" name="email" class="form-control" required="true">
                    </div>
                    <button type="submit" class="btn btn-success">Получить</button>
                </form>
            </div>
        </div>
    </div>
</div>

{{--@if(Request::is('/') || Request::is('courses') || Request::is('cart*') || Request::is('show_cart*'))
<div style="float:left; top: 370px; background-color:#616161; width:50; height:60; padding:8px; position: fixed; z-index:1000; border-radius:4px;">

 <a href="#" data-toggle="modal" data-target="#student" style="text-decoration: none; text-underline: none; font-family:arial; font-size:12px;">
   <b style="color:white;">c<br>
т<br>
у<br>
д<br>
е<br>
н<br>
т<br>
?<br>
@endif--}}

    <script src="/assets/site/static/js/jquery-1.11.2.min.js"></script>
    <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery-cookie/1.3.1/jquery.cookie.min.js" defer></script>
    <script src="/assets/site/static/bootstrap/js/bootstrap.min.js"></script>
    <script src="/assets/site/static/js/owlcarousel/owl.carousel.min.js" defer></script>
    <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
    <script src="/assets/site/static/js/fancybox/jquery.fancybox.pack.js?v=2.1.5" type="text/javascript"></script>
    <script src='https://www.google.com/recaptcha/api.js'></script>
    <script src="/assets/bootstrap/js//confirm-bootstrap.js"></script>


    @if(!Request::is('final*'))
    <script src="/assets/site/static/js/general.js"></script>
    @endif

    <script src="/assets/js/jcrop/js/jquery.Jcrop.js" defer></script>

    @if(Auth::check() and Request::is('user/get_edit/'.Auth::user()->id))
    <script src="/assets/site/static/js/upload-avatar.js"></script>
    @endif

    <script src="/assets/js/jquery-file-upload/js/vendor/jquery.ui.widget.js" defer></script>
    <script src="/assets/js/jquery-file-upload/js/jquery.iframe-transport.js" defer></script>
    <script src="/assets/js/jquery-file-upload/js/jquery.fileupload.js" defer></script>


{{--    <script src="http://ajax.aspnetcdn.com/ajax/jquery.datatables/1.9.4/jquery.datatables.min.js"></script>--}}
    <script src="{{asset('assets/js/jquery.datatables.min.js')}}"></script>
    <script src="{{asset('assets/js/datatables-bootstrap.js')}}"></script>
    <script src="{{asset('assets/js/datatables.fnReloadAjax.js')}}"></script>

    <script src="/assets/js/flot/excanvas.min.js" defer></script>
    <script src="/assets/js/flot/jquery.flot.js" defer></script>
    <script src="/assets/js/flot/jquery.flot.pie.js" defer></script>
    <script src="/assets/js/flot/jquery.flot.time.js" defer></script>
    <script src="/assets/js/flot.tooltip/js/jquery.flot.tooltip.min.js" defer></script>

    <link rel="stylesheet" href="{{asset('/assets/js/jquery-file-upload/css/style.css?v=2.1.5')}}">
    <link rel="stylesheet" href="{{asset('/assets/js/jquery-file-upload/css/jquery.fileupload.css')}}" async defer>
    <link rel="stylesheet" href="{{asset('/assets/js/jquery-file-upload/css/jquery.fileupload-ui.css')}}" async defer>
    <link href="/assets/site/static/js/owlcarousel/assets/owl.carousel.css" rel="stylesheet" async defer>
    <link href="/assets/site/static/js/fancybox/jquery.fancybox.css?v=2.1.5" rel="stylesheet"  type="text/css" media="screen" async defer/>

    <link rel="stylesheet" href="{{asset('/assets/js/jcrop/css/jquery.Jcrop.css')}}" type="text/css" async defer/>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css" async defer>

    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" async>

    <script>
    $('#register').submit(function(event) {
        event.preventDefault();
        $('#reg-errors').empty();
            $.ajax({
                type: "POST",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data: $('form#register').serialize(),
                url: "/user/register",
                success: function(data) {
                    if(data.status != 'success')
                    {
                        $('#reg-errors').append(data.error);
                    }else{
                        alert('Вам на почту отправлено письмо с подтверждением пароля');
                        $('#reg').modal('hide');
                    }
                }
            });
    });

    $('#student-form').submit(function(event) {
        event.preventDefault();
            $.ajax({
                type: "POST",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data: $('form#student-form').serialize(),
                url: "/student",
                success: function(data) {
                    if(data.success != '1')
                    {
                        $('#student-message').append('Произошла ошибка');
                    }else{
                        $('#student-message').append('Ваши данные приняты. Мы свяжемся с вами в ближайшее время');
                    }
                }
            });
    });
    </script>

   <script>
    $('#login').submit(function(event) {
        event.preventDefault();
        $('#login-errors').empty();
        $.ajax({
            type: "POST",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: $('form#login').serialize(),
            url: "/user/login",
            success: function(data) {
                if(data.status != 'success'){
                    $('#login-errors').append(data.login_error);
                }else{
                     $('#sign-in').modal('hide');
                     location.reload();
                }
            }
        });
    });
    </script>

       <script>
               $('[data-toggle="popover"]').popover({ trigger: "hover" });
       </script>

    <script>
    $(function () {
        $('#fileupload').fileupload({
            dataType: 'json',
            done: function (e, data) {
                $.each(data.result.files, function (index, file) {
                    $('<p/>').html('<div class="file-loaded'+file.id+' clearfix">Загружено: '+file.original_name+'<span class="pull-right"><a href="#" class="delete_file" data-file-id="'+file.id+'"><img src="/assets/site/static/i/close.png" /></a></span></div>').appendTo('#files');
                    $('<input type="hidden" name="files_id[]" value="'+file.id+'">').appendTo('#files');
                    $('#loading').hide();

                });
                $('#final-check-button').removeAttr('disabled');
            },
            progressall: function (e, data) {
                $('#loading').show();
            },
            stop: function (e, data) {
                $('#loading').hide();
            }
        });
    });
    </script>
    <script>
        $('#files').on('click', 'a', function() {
            //e.preventDefault();
            var $link = $(this),
                id = Number($link.data('file-id'));
            if(confirm("Удалить?")){
                $.ajax({
                    type: "GET",
                    url: "/delete/" + id,
                    success: function(){
                    }
                });

                $( ".file-loaded"+id).remove();
            }
            return false;
        });
    </script>
    <script>
    $(document).ready(function(){
          $('.test-qst').each(function(){
                // get current ul
                var $ul = $(this);
                // get array of list items in current ul
                var $liArr = $ul.children('li');
                // sort array of list items in current ul randomly
                $liArr.sort(function(a,b){
                      // Get a random number between 0 and 10
                      var temp = parseInt( Math.random()*50 );
                      // Get 1 or 0, whether temp is odd or even
                      var isOddOrEven = temp%2;
                      // Get +1 or -1, whether temp greater or smaller than 5
                      var isPosOrNeg = temp>5 ? 1 : -1;
                      // Return -1, 0, or +1
                      return( isOddOrEven*isPosOrNeg );
                })
                // append list items to ul
                .appendTo($ul);
          });
    });
    </script>
{{--
<script>
redconnect.api.phoneWidget.show()
</script>
--}}

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-52433115-1', 'auto');
  ga('send', 'pageview');

</script>

<script>
$('.vimeo').each(function(){
var $this  = $(this);
$.ajax({
        type:'GET',
        //url: 'http://vimeo.com/api/v2/video/' + this.id  + '.json',
        url: 'http://vimeo.com/api/oembed.json?url=http://vimeo.com/' + this.id,
        //url: 'http://vimeo.com/api/rest/v2?format=json&method=vimeo.videos.getThumbnailUrls&video_id=109588649',// + this.id,
        jsonp: 'callback',
        dataType: 'jsonp',
        success: function(data){
            var video = data[0];
            $this.find('img').attr('src', video.thumbnail_medium);
        }
    });
});
</script>

{{--<script>
$("#partner-discount").change(function(){
var data = document.getElementById('partner-discount');
var value = $("#partner-discount").val();
        //alert(data.dataset.min);
        if(value < data.dataset.dmin){
             alert('Скидка не может быть меньше '+ data.dataset.min);
        }else if(value > data.dataset.dmax) {
            alert('Скидка не может быть больше '+ data.dataset.max);
        }

});
</script>--}}
@if(!Request::is('final*'))
<!-- Yandex.Metrika counter -->
<script type="text/javascript">
(function (d, w, c) {
    (w[c] = w[c] || []).push(function() {
        try {
            w.yaCounter24450503 = new Ya.Metrika({id:24450503,
                    webvisor:true,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true});
        } catch(e) { }
    });

    var n = d.getElementsByTagName("script")[0],
        s = d.createElement("script"),
        f = function () { n.parentNode.insertBefore(s, n); };
    s.type = "text/javascript";
    s.async = true;
    s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js";

    if (w.opera == "[object Opera]") {
        d.addEventListener("DOMContentLoaded", f, false);
    } else { f(); }
})(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="//mc.yandex.ru/watch/24450503" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->
<script type="text/javascript">(function() {
  if (window.pluso)if (typeof window.pluso.start == "function") return;
  if (window.ifpluso==undefined) { window.ifpluso = 1;
    var d = document, s = d.createElement('script'), g = 'getElementsByTagName';
    s.type = 'text/javascript'; s.charset='UTF-8'; s.async = true;
    s.src = ('https:' == window.location.protocol ? 'https' : 'http')  + '://share.pluso.ru/pluso-like.js';
    var h=d[g]('body')[0];
    h.appendChild(s);
  }})();</script>
@endif

  <script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-66145011-1', 'auto');
    ga('send', 'pageview');
  </script>

  <script src="https://apis.google.com/js/platform.js" async defer>
    {lang: 'ru'}
  </script>

<!-- {literal} -->
<script type='text/javascript'>
        window['li'+'veT'+'ex'] = true,
        window['live'+'Tex'+'I'+'D'] = 144316,
        window['liveTe'+'x'+'_o'+'bje'+'ct'] = true;
    (function() {
            var t = document['c'+'reat'+'eE'+'lement']('script');
            t.type ='text/javascript';
            t.async = true;
            t.src = '//cs15.liv'+'etex.ru/js/'+'clie'+'nt.js';
            var c = document['getElemen'+'tsByTa'+'gName']('script')[0];
            if ( c ) c['paren'+'tN'+'od'+'e']['inser'+'tBefo'+'re'](t, c);
            else document['do'+'c'+'umen'+'t'+'Eleme'+'n'+'t']['firs'+'tChi'+'ld']['appe'+'ndChi'+'ld'](t);
    })();
</script>
<!-- {/literal} -->
</body>
</html>
