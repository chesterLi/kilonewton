@extends('admin.layouts.modal')

{{-- Content --}}
@section('content')
     <!-- Tabs -->
<!--         <ul class="nav nav-tabs">
         <li class="active"><a href="#tab-general" data-toggle="tab">Курс</a></li>
         <li><a href="#tab-meta-data" data-toggle="tab">Главы</a></li>
         </ul>-->
     <!-- ./ tabs -->

	{{-- Edit Blog Form --}}
	<form class="form-horizontal" method="post" action="@if (isset($item)){{ URL::to('admin/experts/' . $item->id . '/edit') }}@endif" autocomplete="off" enctype="multipart/form-data">
		<!-- CSRF Token -->
		<input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
		<!-- ./ csrf token -->

		<!-- Tabs Content -->
		<div class="tab-content">
			<!-- General tab -->
			<div class="tab-pane active" id="tab-general">
                <div class="form-group {{{ $errors->has('name') ? 'error' : '' }}}">
                    <label class="col-md-2 control-label" for="name">ФИО</label>
                    <div class="col-md-10">
                        <input class="form-control" type="text" name="name" id="name" value="{{{ Input::old('name', isset($item) ? $item->name : null) }}}" />
                        {{ $errors->first('name', '<span class="help-inline">:message</span>') }}
                    </div>
                </div>
                <div class="form-group {{{ $errors->has('area') ? 'error' : '' }}}">
                    <label class="col-md-2 control-label" for="area">Экспертные области</label>
                    <div class="col-md-10">
                        <input class="form-control" type="text" name="area" id="area" value="{{{ Input::old('area', isset($item) ? $item->area : null) }}}" />
                        {{ $errors->first('area', '<span class="help-inline">:message</span>') }}
                    </div>
                </div>
                <div class="form-group {{{ $errors->has('regalia') ? 'error' : '' }}}">
                    <label class="col-md-2 control-label" for="regalia">Регалии</label>
                    <div class="col-md-10">
                        <input class="form-control" type="text" name="regalia" id="regalia" value="{{{ Input::old('regalia', isset($item) ? $item->regalia : null) }}}" />
                        {{ $errors->first('regalia', '<span class="help-inline">:message</span>') }}
                    </div>
                </div>
                <div class="form-group {{{ $errors->has('contacts') ? 'error' : '' }}}">
                    <label class="col-md-2 control-label" for="contacts">Контакты</label>
                    <div class="col-md-10">
                        <input class="form-control" type="text" name="contacts" id="contacts" value="{{{ Input::old('contacts', isset($item) ? $item->contacts : null) }}}" />
                        {{ $errors->first('contacts', '<span class="help-inline">:message</span>') }}
                    </div>
                </div>
                <div class="form-group {{{ $errors->has('text') ? 'error' : '' }}}">
                    <label class="col-md-2 control-label" for="text">Об эксперте</label>
                    <div class="col-md-10">
						<textarea class="form-control full-width ckeditor" name="text" value="text" rows="10" id="blog-text">{{{ Input::old('text', isset($item) ? $item->text : null) }}}</textarea>
						{{{ $errors->first('text', '<span class="help-block">:message</span>') }}}
                    </div>
                </div>
                <div class="form-group {{{ $errors->has('course') ? 'error' : '' }}}">
                    <label class="col-md-2 control-label" for="course">Курсы</label>
                    <div class="col-md-10">
                        @foreach($courses as $course)
                            @if(isset($item))
                                {{$course->name}} {{ Form::checkbox('courses[]', $course->id, isset($item) ? in_array($course->id, $courses_s) : 1) }}<br>
                            @else
                                {{$course->name}} {{ Form::checkbox('courses[]', $course->id) }}<br>
                            @endif
                        @endforeach
                        {{ $errors->first('course', '<span class="help-inline">:message</span>') }}
                    </div>
                </div>
                <div class="form-group {{{ $errors->has('articles') ? 'error' : '' }}}">
                    <label class="col-md-2 control-label" for="articles">Статьи</label>
                    <div class="col-md-10">
                        @foreach($articles as $article)
                            @if(isset($item))
                                {{$article->title}} {{ Form::checkbox('articles[]', $article->id, isset($item) ? in_array($article->id, $articles_s) : 1) }}<br>
                            @else
                                {{$article->title}} {{ Form::checkbox('articles[]', $article->id) }}<br>
                            @endif
                        @endforeach
                        {{ $errors->first('articles', '<span class="help-inline">:message</span>') }}
                    </div>
                </div>
                <div class="form-group {{{ $errors->has('category') ? 'error' : '' }}}">
                    <label class="col-md-2 control-label" for="category">Категория</label>
                    <div class="col-md-10">
                        {{Form::select('category', array('1' => 'Архитектора', '2' => 'Конструкции', '3' => 'Инжинерные сети', '4' => 'Управление проектами'), Input::old('courses', isset($item) ? $item->category : null),array('class'=>'form-control'))}}
                    </div>
                </div>
                <label class="col-md-2 control-label" for="file">Фотография</label>
                <div class="col-md-10">
                    <span class="btn btn-success fileinput-button">
                        <i class="glyphicon glyphicon-plus"></i>
                        <span>Выберите файл</span>
                        <!-- The file input field used as target for the file upload widget -->
                    <input id="fileupload-ex" data-url="uploads" type="file" name="file">
                    </span>
                    <br>
                    <br>
                    <!-- The global progress bar -->
                    <div id="progress" class="progress">
                        <div class="progress-bar progress-bar-success"></div>
                    </div>
                    <!-- The container for the uploaded files -->
                    <div id="files" class="files">
                        @if(isset($item->file))
                             <img src="{{URL::to('uploads/experts/crop', $item->file)}}" id="uploaded-image">
                        @else
                            <img src="" id="uploaded-image">
                        @endif
                    </div>
                    <input id="file-name" name="file_name" type="hidden">
                    <input type="hidden" name="file_width" value="" id="file-width">
                    <input type="hidden" name="file_height" value="" id="file-height">
                    <input type="hidden" name="file_x" value="" id="file-x">
                    <input type="hidden" name="file_y" value="" id="file-y">
                    <br>
                    {{{ $errors->first('file', ':message') }}}
                </div>
			</div>
		</div>
		<!-- ./ tabs content -->

		<!-- Form Actions -->
		<div class="form-group">
			<div class="col-md-12">
				<element class="btn-cancel close_popup">Отмена</element>
				<button type="submit" class="btn btn-success">Сохранить</button>
			</div>
		</div>
		<!-- ./ form actions -->
	</form>
@stop


