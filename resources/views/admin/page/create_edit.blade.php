@extends('admin.layouts.modal')

{{-- Content --}}
@section('content')

	{{-- Edit Blog Form --}}
	<form class="form-horizontal" method="post" action="@if (isset($item)){{ URL::to('admin/page/' . $item->id . '/edit') }}@endif" autocomplete="off" enctype="multipart/form-data">
		<!-- CSRF Token -->
		<input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
		<!-- ./ csrf token -->

		<!-- Tabs Content -->
		<div class="tab-content">
			<!-- General tab -->
			<div class="tab-pane active" id="tab-general">
                <div class="form-group {{{ $errors->has('title') ? 'error' : '' }}}">
                    <label class="col-md-2 control-label" for="title">Заголовок</label>
                    <div class="col-md-10">
                        <input class="form-control" type="text" name="title" id="title" value="{{{ Input::old('title', isset($item) ? $item->title : null) }}}" />
                        {{ $errors->first('title', '<span class="help-inline">:message</span>') }}
                    </div>
                </div>
                <div class="form-group {{{ $errors->has('text') ? 'has-error' : '' }}}">
					<div class="col-md-12">
                        <label class="control-label" for="text">Текст</label>
						<textarea class="form-control full-width ckeditor" name="text" value="text" rows="10">{{{ Input::old('text', isset($item) ? $item->text : null) }}}</textarea>
						{{{ $errors->first('text', '<span class="help-block">:message</span>') }}}
					</div>
				</div>
			</div>
		<!-- ./ general tab -->
            
		</div>
		<!-- ./ tabs content -->

		<!-- Form Actions -->
		<div class="form-group">
			<div class="col-md-12">
				<element class="btn-cancel close_popup">Отмена</element>
				<button type="submit" class="btn btn-success">Сохранить</button>
			</div>
		</div>
		<!-- ./ form actions -->
	</form>
@stop
