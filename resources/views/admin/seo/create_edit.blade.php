@extends('admin.layouts.modal')

{{-- Content --}}
@section('content')

	{{-- Edit Blog Form --}}
	<form class="form-horizontal" method="post" action="@if (isset($post)){{ URL::to('admin/seo/' . $item->id . '/edit') }}@endif" autocomplete="off">
		<!-- CSRF Token -->
		<input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
		<!-- ./ csrf token -->

		<!-- Tabs Content -->
		<div class="tab-content">
			<!-- General tab -->
			<div class="tab-pane active" id="tab-general">
				<!-- Post Title -->
				<div class="form-group {{{ $errors->has('title') ? 'error' : '' }}}">
                    <label class="col-md-2 control-label" for="title">Title</label>
                    <div class="col-md-10">
                        <input class="form-control" type="text" name="title" id="title" value="{{{ Input::old('title', isset($item) ? $item->title : null) }}}" />
                        {{ $errors->first('title', '<span class="help-inline">:message</span>') }}
                    </div>
                </div>
                <div class="form-group {{{ $errors->has('meta_keywords') ? 'error' : '' }}}">
                    <label class="col-md-2 control-label" for="meta_keywords">Keywords</label>
                    <div class="col-md-10">
                        <textarea class="form-control full-width" name="meta_keywords" value="meta_keywords" rows="10">{{{ Input::old('meta_keywords', isset($item) ? $item->meta_keywords : null) }}}</textarea>
                        {{ $errors->first('meta_keywords', '<span class="help-inline">:message</span>') }}
                    </div>
                </div>
                <div class="form-group {{{ $errors->has('url') ? 'error' : '' }}}">
                    <label class="col-md-2 control-label" for="url">Url</label>
                    <div class="col-md-10">
                        <input class="form-control" type="text" name="url" id="url" value="{{{ Input::old('url', isset($item) ? $item->url : null) }}}" />
                        {{ $errors->first('url', '<span class="help-inline">:message</span>') }}
                    </div>
                </div>
                <div class="form-group {{{ $errors->has('description') ? 'error' : '' }}}">
                    <label class="col-md-2 control-label" for="description">Description</label>
                    <div class="col-md-10">
                        <input class="form-control" type="text" name="description" id="description" value="{{{ Input::old('description', isset($item) ? $item->description : null) }}}" />
                        {{ $errors->first('description', '<span class="help-inline">:message</span>') }}
                    </div>
                </div>
                <div class="form-group {{{ $errors->has('og_url') ? 'error' : '' }}}">
                    <label class="col-md-2 control-label" for="og_url">og_url</label>
                    <div class="col-md-10">
                        <input class="form-control" type="text" name="og_url" id="og_url" value="{{{ Input::old('og_url', isset($item) ? $item->og_url : null) }}}" />
                        {{ $errors->first('og_url', '<span class="help-inline">:message</span>') }}
                    </div>
                </div>
                <div class="form-group {{{ $errors->has('og_title') ? 'error' : '' }}}">
                    <label class="col-md-2 control-label" for="og_title">og_title</label>
                    <div class="col-md-10">
                        <input class="form-control" type="text" name="og_title" id="og_title" value="{{{ Input::old('og_title', isset($item) ? $item->og_title : null) }}}" />
                        {{ $errors->first('og_title', '<span class="help-inline">:message</span>') }}
                    </div>
                </div>
                <div class="form-group {{{ $errors->has('og_description') ? 'error' : '' }}}">
                    <label class="col-md-2 control-label" for="og_description">og_description</label>
                    <div class="col-md-10">
                        <input class="form-control" type="text" name="og_description" id="og_description" value="{{{ Input::old('og_description', isset($item) ? $item->og_description : null) }}}" />
                        {{ $errors->first('og_description', '<span class="help-inline">:message</span>') }}
                    </div>
                </div>
				<!-- ./ post title -->
			</div>
			<!-- ./ general tab -->
		</div>
		<!-- ./ tabs content -->

		<!-- Form Actions -->
		<div class="form-group">
			<div class="col-md-12">
				<element class="btn-cancel close_popup">Отмена</element>
				<button type="submit" class="btn btn-success">Сохранить</button>
			</div>
		</div>
		<!-- ./ form actions -->
	</form>
@stop
