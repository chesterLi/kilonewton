@extends('admin.layouts.modal')

{{-- Content --}}
@section('content')
     <!-- Tabs -->
<!--         <ul class="nav nav-tabs">
         <li class="active"><a href="#tab-general" data-toggle="tab">Курс</a></li>
         <li><a href="#tab-meta-data" data-toggle="tab">Главы</a></li>
         </ul>-->
     <!-- ./ tabs -->

	{{-- Edit Blog Form --}}
	<form class="form-horizontal" method="post" action="@if (isset($item)){{ URL::to('admin/tests/' . $item->id . '/edit') }}@endif" autocomplete="off" enctype="multipart/form-data">
		<!-- CSRF Token -->
		<input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
		<!-- ./ csrf token -->

		<!-- Tabs Content -->
		<div class="tab-content">
			<!-- General tab -->
			<div class="tab-pane active" id="tab-general">
                <div class="form-group {{{ $errors->has('courses') ? 'error' : '' }}}">
                    <label class="col-md-2 control-label" for="tags">Курс</label>
                    <div class="col-md-10">
                        {{ Form::select('courses', array('default' => 'Выберите курс') + $courses, Input::old('courses', isset($item) ? $item->courses_id : null),array('class'=>'form-control','id'=>'get-chapters-test')) }}
                        {{ $errors->first('courses', '<span class="help-inline">:message</span>') }}
                    </div>
                </div>
                <label class="col-md-2 control-label" for="chapters">Главы</label>
                <div class="col-md-10">
                    <select id="mySelect" name="chapter_id"></select>
                </div>

            </div>
			</div>
		<!-- ./ general tab -->

		</div>
		<!-- ./ tabs content -->

		<!-- Form Actions -->
		<div class="form-group">
			<div class="col-md-12">
				<element class="btn-cancel close_popup">Отмена</element>
				<button type="submit" class="btn btn-success">Сохранить</button>
			</div>
		</div>
		<!-- ./ form actions -->
	</form>
@stop


