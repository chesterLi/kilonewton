<!DOCTYPE html>

<html lang="en">

<head id="Starter-Site">

	<meta charset="UTF-8">

	<!-- Always force latest IE rendering engine (even in intranet) & Chrome Frame -->
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

	<title>
		@section('title')
			Administration
		@show
	</title>

	<meta name="keywords" content="@yield('keywords')" />
	<meta name="author" content="@yield('author')" />

	<!-- Google will often use this as its description of your page/site. Make it good. -->
	<meta name="description" content="@yield('description')" />

	<!-- Speaking of Google, don't forget to set your site up: http://google.com/webmasters -->
	<meta name="google-site-verification" content="">

	<!-- Dublin Core Metadata : http://dublincore.org/ -->
	<meta name="DC.title" content="Project Name">
	<meta name="DC.subject" content="@yield('description')">
	<meta name="DC.creator" content="@yield('author')">

	<!--  Mobile Viewport Fix -->
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">

	<!-- This is the traditional favicon.
	 - size: 16x16 or 32x32
	 - transparency is OK
	 - see wikipedia for info on browser support: http://mky.be/favicon/ -->
	<link rel="shortcut icon" href="{{{ asset('assets/ico/favicon.png') }}}">
    <!-- iOS favicons. -->
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="{{{ asset('assets/ico/apple-touch-icon-144-precomposed.png') }}}">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="{{{ asset('assets/ico/apple-touch-icon-114-precomposed.png') }}}">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="{{{ asset('assets/ico/apple-touch-icon-72-precomposed.png') }}}">
    <link rel="apple-touch-icon-precomposed" href="{{{ asset('assets/ico/apple-touch-icon-57-precomposed.png') }}}">

    <!-- CSS adjustments for browsers with JavaScript disabled -->
    <noscript><link rel="stylesheet" href="/assets/js/jquery-file-upload/css/jquery.fileupload-noscript.css"></noscript>
    <noscript><link rel="stylesheet" href="/assets/js/jquery-file-upload/css/jquery.fileupload-ui-noscript.css"></noscript>

    <!-- CSS -->
    <link rel="stylesheet" href="{{asset('bootstrap/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('bootstrap/css/bootstrap-theme.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/wysihtml5/prettify.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/wysihtml5/bootstrap-wysihtml5.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/datatables-bootstrap.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/colorbox.css')}}">
    <!--Datepiker-->
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.0/themes/smoothness/jquery-ui.css">
    <link rel="stylesheet" href="{{asset('assets/css/ui-darkness/jquery-ui-1.10.4.custom.css')}}">
    <!--Fileupload-->
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">
    {{--<link rel="stylesheet" href="http://blueimp.github.io/Gallery/css/blueimp-gallery.min.css">--}}
    <link rel="stylesheet" href="{{asset('/assets/css/blueimp-gallery.min.css')}}">

    <link rel="stylesheet" href="{{asset('/assets/js/jquery-file-upload/css/style.css')}}">
    <link rel="stylesheet" href="{{asset('/assets/js/jquery-file-upload/css/jquery.fileupload.css')}}">
    <link rel="stylesheet" href="{{asset('/assets/js/jquery-file-upload/css/jquery.fileupload-ui.css')}}">

    <link rel="stylesheet" href="{{asset('/assets/js/jcrop/css/jquery.Jcrop.css')}}" type="text/css" />

	<style>
	body {
		padding: 60px 0;
	}
	</style>

	@yield('styles')

	<!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
	<!--[if lt IE 9]>
	<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->

</head>

<body>
	<!-- container -->
	<div class="container">
		<!-- navbar -->
		<div class="navbar navbar-default navbar-inverse navbar-fixed-top">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                        <span class="sr-only">toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                </div>
    			<div class="collapse navbar-collapse navbar-ex1-collapse">
                    <ul class="nav navbar-nav">
                        <li class="dropdown{{ (Request::is('admin/courses*|admin/category*|admin/tests*|admin/tag*') ? ' active' : '') }}">
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                <span class="glyphicon glyphicon-user"></span> Курсы <span class="caret"></span>
                            </a>
                        <ul class="dropdown-menu">
                            <li{{ (Request::is('admin/courses*') ? ' class="active"' : '') }}><a href="{{{ URL::to('admin/courses') }}}"><span class="glyphicon glyphicon-list-alt"></span> Курсы</a></li>
                            <li{{ (Request::is('admin/category*') ? ' class="active"' : '') }}><a href="{{{ URL::to('admin/category') }}}"><span class="glyphicon glyphicon-list-alt"></span> Категории курсов</a></li>
                            <li{{ (Request::is('admin/tests*') ? ' class="active"' : '') }}><a href="{{{ URL::to('admin/tests') }}}"><span class="glyphicon glyphicon-list-alt"></span> Тесты</a></li>
                            <li{{ (Request::is('admin/tag*') ? ' class="active"' : '') }}><a href="{{{ URL::to('admin/tag') }}}"><span class="glyphicon glyphicon-list-alt"></span> Тэги</a></li>
                            <li{{ (Request::is('admin/mtags*') ? ' class="active"' : '') }}><a href="{{{ URL::to('admin/mtags') }}}"><span class="glyphicon glyphicon-list-alt"></span> Тэги для уроков</a></li>
                        </ul>
                        </li>
                        <li class="dropdown{{ (Request::is('admin/page*|admin/experts*') ? ' active' : '') }}">
                            <a class="dropdown-toggle" data-toggle="dropdown" href="{{{ URL::to('admin/page') }}}">
                                <span class="glyphicon glyphicon-user"></span> Текстовые разделы <span class="caret"></span>
                            </a>
                        <ul class="dropdown-menu">
                            <li{{ (Request::is('admin/experts*') ? ' class="active"' : '') }}><a href="{{{ URL::to('admin/experts') }}}"><span class="glyphicon glyphicon-list-alt"></span> Эксперты</a></li>
                            <li{{ (Request::is('admin/page*') ? ' class="active"' : '') }}><a href="{{{ URL::to('admin/page') }}}"><span class="glyphicon glyphicon-list-alt"></span> Текстовые разделы</a></li>
                        </ul>
                        </li>

                        @if(Sentry::getUser()->role == 'instructor')
                        <li class="dropdown{{ (Request::is('admin/exams*|admin/roles*') ? ' active' : '') }}">
                            <a class="dropdown-toggle" data-toggle="dropdown" href="{{{ URL::to('admin/exams') }}}">
                                <span class="glyphicon glyphicon-user"></span> Студенты <span class="caret"></span>
                            </a>
                        <ul class="dropdown-menu">
                            <li{{ (Request::is('admin/exams*') ? ' class="active"' : '') }}><a href="{{{ URL::to('admin/exams') }}}"><span class="glyphicon glyphicon-user"></span> Задания на проверку</a></li>
                            <li{{ (Request::is('admin/discussion*') ? ' class="active"' : '') }}><a href="{{{ URL::to('admin/discussion') }}}"><span class="glyphicon glyphicon-user"></span> Обсуждение тестов</a></li>
                            <li{{ (Request::is('admin/final_discussion*') ? ' class="active"' : '') }}><a href="{{{ URL::to('admin/final_discussion') }}}"><span class="glyphicon glyphicon-user"></span> Обсуждение финальных заданий</a></li>
                        </ul>
                        </li>
                        @endif
                        @if(Sentry::getUser()->role == 'admin')
                        <li class="dropdown{{ (Request::is('admin/users*|admin/roles*|admin/hr_courses*') ? ' active' : '') }}">
                            <a class="dropdown-toggle" data-toggle="dropdown" href="{{{ URL::to('admin/users') }}}">
                                <span class="glyphicon glyphicon-user"></span> Пользователи <span class="caret"></span>
                            </a>
                        <ul class="dropdown-menu">
                            <li{{ (Request::is('admin/users*') ? ' class="active"' : '') }}><a href="{{{ URL::to('admin/users') }}}"><span class="glyphicon glyphicon-user"></span> Пользователи</a></li>
                            {{--<li{{ (Request::is('admin/roles*') ? ' class="active"' : '') }}><a href="{{{ URL::to('admin/roles') }}}"><span class="glyphicon glyphicon-user"></span> Роли</a></li>--}}
                            <li{{ (Request::is('admin/access*') ? ' class="active"' : '') }}><a href="{{{ URL::to('admin/access') }}}"><span class="glyphicon glyphicon-user"></span> Доступы к главам</a></li>
                            <li{{ (Request::is('admin/hr_courses*') ? ' class="active"' : '') }}><a href="{{{ URL::to('admin/hr_courses') }}}"><span class="glyphicon glyphicon-user"></span> Доступы к курсам для HR</a></li>
                        </ul>
                        </li>
                        <li{{ (Request::is('admin/promo*') ? ' class="active"' : '') }}><a href="{{{ URL::to('admin/promo') }}}"><span class="glyphicon glyphicon-list-alt"></span> Промо-коды</a></li>
                        <li{{ (Request::is('admin/partner*') ? ' class="active"' : '') }}><a href="{{{ URL::to('admin/partner') }}}"><span class="glyphicon glyphicon-list-alt"></span> Партнеры</a></li>
                            <li class="dropdown{{ (Request::is('admin/blog*|admin/blog_tag*|blog_comment*') ? ' active' : '') }}">
                                <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                    <span class="glyphicon glyphicon-user"></span> Блог <span class="caret"></span>
                                </a>
                            <ul class="dropdown-menu">
                                <li{{ (Request::is('admin/blog*') ? ' class="active"' : '') }}><a href="{{{ URL::to('admin/blog') }}}"><span class="glyphicon glyphicon-list-alt"></span> Блог</a></li>
                                <li{{ (Request::is('admin/blog_tag*') ? ' class="active"' : '') }}><a href="{{{ URL::to('admin/blog_tag') }}}"><span class="glyphicon glyphicon-list-alt"></span> Теги</a></li>
                                <li{{ (Request::is('admin/blog_comment*') ? ' class="active"' : '') }}><a href="{{{ URL::to('admin/blog_comment') }}}"><span class="glyphicon glyphicon-list-alt"></span> Комментарии</a></li>
                            </ul>
                        </li>
                        @endif
                        <li{{ (Request::is('admin/seo*') ? ' class="active"' : '') }}><a href="{{{ URL::to('admin/seo') }}}"><span class="glyphicon glyphicon-list-alt"></span> SEO</a></li>
                    </ul>
    				<ul class="nav navbar-nav pull-right">
    					<li><a href="{{{ url::to('/') }}}">вернуться на сайт</a></li>
    					<li class="divider-vertical"></li>
    					<li class="dropdown">
    							<a class="dropdown-toggle" data-toggle="dropdown" href="#">
    								<span class="glyphicon glyphicon-user"></span> {{{ Sentry::getUser()->username }}}	<span class="caret"></span>
    							</a>
    							<ul class="dropdown-menu">
    								<li><a href="{{{ url::to('user/logout') }}}"><span class="glyphicon glyphicon-share"></span> выход</a></li>
    							</ul>
    					</li>
    				</ul>
    			</div>
            </div>
		</div>
		<!-- ./ navbar -->

		<!-- notifications -->
		@include('notifications')
		<!-- ./ notifications -->

		<!-- content -->
		@yield('content')
		<!-- ./ content -->

		<!-- footer -->
		<footer class="clearfix">
			@yield('footer')
		</footer>
		<!-- ./ footer -->

	</div>
	<!-- ./ container -->

	<!-- javascripts -->

    <script src="//code.jquery.com/jquery-1.10.2.js"></script>
    <script src="//code.jquery.com/ui/1.11.0/jquery-ui.js"></script>
    <script src="/assets/js/browser.js"></script>
    <script src="/assets/js/main.js"></script>
    <script src="/assets/js/addfiles.js"></script>

    <script src="{{asset('bootstrap/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('assets/js/wysihtml5/wysihtml5-0.3.0.js')}}"></script>
    <script src="{{asset('assets/js/wysihtml5/bootstrap-wysihtml5.js')}}"></script>
    <script src="{{asset('assets/js/jquery.datatables.min.js')}}"></script>
{{--    <script src="http://ajax.aspnetcdn.com/ajax/jquery.datatables/1.9.4/jquery.datatables.min.js"></script>--}}
    <script src="{{asset('assets/js/datatables-bootstrap.js')}}"></script>
    <script src="{{asset('assets/js/datatables.fnReloadAjax.js')}}"></script>
    <script src="{{asset('assets/js/jquery.colorbox.js')}}"></script>
    <script src="{{asset('assets/js/prettify.js')}}"></script>
    <script src="{{asset('assets/js/ckeditor/ckeditor.js')}}"></script>

    <script type="text/javascript" src="/assets/js/jquery-file-upload/js/jquery.fileupload.js"></script>
    <script type="text/javascript" src="/assets/js/jquery-file-upload/js/jquery.fileupload-process.js"></script>
    <script type="text/javascript" src="/assets/js/jquery-file-upload/js/jquery.iframe-transport.js"></script>

    <script type="text/javascript">
        $(prettyPrint);
        $(function() {
            $( "#datepicker" ).datepicker({ dateFormat: 'yy-mm-dd' }).val();
            CKEDITOR.replaceAll( 'ckeditor' );
        });
    </script>

    @yield('scripts')
</body>

</html>