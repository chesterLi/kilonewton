<?php

namespace App\Helpers;

use App\Models\Chapter;
use App\Models\Course;
use App\Models\Exam;
use App\Models\Expert;
use App\Models\Material;
use App\Models\Order;
use App\Models\UserChapter;
use App\Models\UserStat;
use App\Models\UsersTests;
use App\Models\UsersVideos;
use Gloudemans\Shoppingcart\Facades\Cart;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

class Helpers {

    public static function isAdmin()
    {
        return Auth::user()->role == 'admin';
    }

    public static function sendSms($input) {

        $params = array(
            'login' => 'zolotovsem',
            'psw' => 'zolotovsem',
            'phones' => '+77713332333',
            'mes' => 'Name: '.$input['name'].' Surname: '.$input['surname'].' Date: '.$input['date_d'].'/'.$input['date_m'].' Time: '.$input['hour'].':'.$input['minute'].' Phone: '.$input['phone'].' Message: '.$input['message'],
            'translit' => '1',
            'charset' => 'utf-8'
        );

        $url =  "http://smsc.ru/sys/send.php";
        $session = curl_init( $url );
        curl_setopt( $session, CURLOPT_HEADER, 0 );
        curl_setopt( $session, CURLOPT_RETURNTRANSFER, 1 );
        curl_setopt( $session, CURLOPT_POSTFIELDS,$params );
        $response = curl_exec( $session );
        curl_close( $session );

    }

    public static function sendMail($input)
    {
        Mail::send('emails.review', $input, function($message)
        {
            $message->to(array('getsugarmusic@gmail.com','dengoose@gmail.com','droga-natalya@yandex.com'), 'Zoloto')->subject('Отзыв');
        });

    }

    public static function getThumbnail($course, $chapter, $material)
    {
        //$hash = unserialize(file_get_contents("http://vimeo.com/api/v2/video/$id.php"));
        //$hash[0]['thumbnail_large'];

        if(Auth::check() && Helpers::viewedVideo($course, $chapter, Auth::user()->id, $material) == 0){
            return '/assets/site/static/i/lesson_start.png';
        }elseif(!Auth::check()){
            return '/assets/site/static/i/lesson_start.png';
        }else{
            return '/assets/site/static/i/lesson_viewed.jpg';
        }

    }

    public static function shuffleAssoc($array)
    {
          if (!is_array($array)) return $array;

          $keys = array_keys($array);
          shuffle($keys);
          $random = array();
          foreach ($keys as $key) {
            $random[$key] = $array[$key];
          }
          return $random;
    }

    public static function viewedVideo($course_id, $chapter_id, $user_id, $video_id)
    {
        $viwed  = UsersVideos::where('user_id', '=', $user_id)->where('course_id', '=', $course_id)->where('chapter_id', '=', $chapter_id)->where('video_id', '=', $video_id)->count();
        return $viwed;
    }

    public static function countChapters($course_id)
    {
        $chapters  = Course::find($course_id)->with('chapters')->count();
        return $chapters;
    }

    public static function countTotalPoints($course_id)
    {
        $chapters  = Course::find($course_id)->with('chapters')->count();
        $lessons   = Material::where('course_id', '=', $course_id)->count();
        $total     = ($chapters * 20) + 30 + $lessons;

        return $total;
    }

    public static function cartTotal($id)
    {
        $cart = Cart::content();

        foreach($cart as $row){
            if($row->options->course_id == $id)
                return $row->subtotal;
        }
        return 0;
    }

    public static function getOrder($user_id)
    {
        $orders = array();

        $order = Order::where('user_id','=', $user_id)->get();
        if(!$order->isEmpty()) {
            array_push($orders, $order);
        }
        dd($orders);
        return $orders;
    }

    public static function generatePassword($length = 8) {
        $chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
        $count = mb_strlen($chars);

        for ($i = 0, $result = ''; $i < $length; $i++) {
            $index = rand(0, $count - 1);
            $result .= mb_substr($chars, $index, 1);
        }

        return $result;
    }

    public static function getUserRating($id)
    {
        $points = UserStat::where('user_id', '=', $id)->lists('points');

        $rating = 0;
        foreach($points as $point){
            $rating = $rating + $point;
        }

        return $rating;
    }

    public static function countUserCourses($id)
    {

        $userCourses = UserChapter::where('user_id', $id)->get();

        $array = array();

        foreach($userCourses as $userCourse){
            if(!in_array($userCourse->course_id, $array)){
                array_push($array, $userCourse->course_id);
            }
        }

        return count($array);

    }

    public static function getExamResults($id)
    {
        return Exam::where('user_id', '=', $id)->where('status', '=', 'checked')->count();
    }

    public static function getCourseName($id)
    {
        return Course::find($id)->name;
    }

    public static function getUserCourseProgress($course_id, $user_id)
    {
        $course_chapters    = Course::find($course_id)->chapters()->count();
        $user_chapters      = UsersTests::where('user_id', '=', $user_id)->where('course_id','=',$course_id)->where('passed', '=', 1)->count();

        if($course_chapters != 0 and $user_chapters != 0) {
            $percentage = round(($user_chapters / $course_chapters) * 100);
        }else{
            $percentage = 0;
        }

        return $percentage;
    }

    public static function generatePromo()
    {
        $length = 10;
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    public static function ExpertId($name)
    {
       return Expert::where('name', '=', $name)->pluck('id');
    }

    public static function getChapterName($id)
    {
        $chapter = Chapter::find($id);
        return $chapter->name;
    }

    public static function getVideoViaProxy()
    {

/*        $url = 'http://player.vimeo.com/video/109568432';

        $aContext = array(
            'http' => array(
                'proxy' => 'tcp://178.141.248.162:8080',
                'request_fulluri' => true,
            ),
        );
        $cxContext = stream_context_create($aContext);

        $sFile = file_get_contents($url, False, $cxContext);

        echo $sFile;*/

        $fp = fsockopen("http://player.vimeo.com/video/109568432", 80, $errno, $errstr, 30);
        if (!$fp) {
            echo "$errstr ($errno)<br />\n";
        } else {
            $out = "GET / HTTP/1.1\r\n";
            $out .= "Host: www.example.com\r\n";
            $out .= "Connection: Close\r\n\r\n";
            fwrite($fp, $out);
            while (!feof($fp)) {
                echo fgets($fp, 128);
            }
            fclose($fp);
        }

    }

    public static function quicksort($seq)
    {

        if(!count($seq)){
            return $seq;
        } else{

        $pivot = $seq[0];

        $low = $high = array();

        $lenght = count($seq);

        for($i=1; $i < $lenght; $i++){
            if($seq[$i] < $pivot){
                $low[] = $seq[$i];
            }else{
                $high[] = $seq[$i];
            }
        }
        }

        return array_merge(self::quicksort($low), array($pivot), self::quicksort($high));
    }

    public static function countPercent($sum)
    {
        $discount   = 60;
        $percent    = ($sum/100)*$discount;
        $new_price    = $sum - $percent;

        return $new_price;
    }

    public static function countPercentReferral($sum, $discount)
    {
        $percent    = ($sum/100)*$discount;
        $new_price    = $sum - $percent;

        return $new_price;
    }

    public static function getChapterObject($id)
    {
        $res = Chapter::find($id);
        return $res;
    }

    public static function getLocation()
    {
/*        $user_ip = getenv('REMOTE_ADDR');
        $geo = unserialize(file_get_contents("http://www.geoplugin.net/php.gp?ip=$user_ip"));
        $country = $geo["geoplugin_countryName"];
        $city = $geo["geoplugin_city"];
        if($country == 'Kazakhstan'){
            return true;
        }else{
            return false;
        }*/

        return true;
    }

    public static function getRates($price)
    {
        $url = "http://www.nationalbank.kz/rss/rates_all.xml";
        $dataObj = simplexml_load_file($url);
        	if ($dataObj){
        	foreach ($dataObj->channel->item as $item){
                if($item->title == 'RUB'){
                    return $item->description * $price;
                }
        	}
        }
    }

}