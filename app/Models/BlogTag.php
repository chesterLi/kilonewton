<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\URL;

class BlogTag extends Model {

    protected $table = 'blog_tags';

    public $timestamps = false;

    public function blogs()
    {
        return $this->belongsToMany('App\Models\Blog', 'blogs_tags')->where('hide', '=', 0)->orderBy('id', 'DESC');
    }

}
