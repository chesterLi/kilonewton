<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Sentry;
use Zizaco\Entrust\HasRole;
use Carbon\Carbon;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableInterface;

class User extends Model
{

    protected $table = 'users';

    /**
     * Get user by username
     * @param $username
     * @return mixed
     */

    protected $guarded = array('_token');
    public $autoHydrateEntityFromInput = true;


    public function isAdmin()
    {
        if (Sentry::getUser()->role == 'admin') {
            return true;
        }
    }

    public function isInstructor()
    {
        if (SentryFacade::getUser()->role == 'instructor') {
            return true;
        }
    }

    public function isAuthor()
    {
        if (SentryFacade::getUser()->role == 'author') {
            return true;
        }
    }

    public function chapters()
    {
        return $this->belongsToMany('App\Models\Chapter', 'user_chapter');
    }

    public function userChapters()
    {
        return $this->hasMany('App\Models\UserChapter');
    }

    public function courses()
    {
        return $this->belongsToMany('Course', 'user_chapter')->distinct();
    }

    public function hasCourse($courseId)
    {
        return $this->courses()->has($courseId);
    }

    public function discussions()
    {
        return $this->hasMany('App\Models\Discussion');
    }

    public function exams()
    {
        return $this->hasMany('App\Models\Exam');
    }

    public function logs()
    {
        return $this->hasMany('App\Models\UserLog');
    }

    public function orders()
    {
        return $this->hasMany('App\Models\Order');
    }

    public function statistics()
    {
        return $this->hasMany('App\Models\UserStat');
    }

    public function promoCodes()
    {
        return $this->hasMany('App\Models\PromoCode');
    }

    public function tests()
    {
        return $this->hasMany('App\Models\UsersTests');
    }


}
