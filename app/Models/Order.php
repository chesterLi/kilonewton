<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{

    protected $table = 'orders';

    public function items()
    {
        return $this->hasMany('App\Models\OrderItem');
    }

    public function username()
    {
        return $this->belongsTo('App\Models\User', 'user_id');
    }
}