<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ReferralCourse extends Model {

    protected $table = 'referral_courses';

    public function course()
    {
        return $this->belongsTo('App\Models\Course');
    }

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

}
