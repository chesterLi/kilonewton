<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\URL;

class Discussion extends Model {

    protected $table = 'discussion';

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    public function course()
    {
        return $this->belongsTo('App\Models\Course');
    }

    public function chapter()
    {
        return $this->belongsTo('App\Models\Chapter');
    }
}
