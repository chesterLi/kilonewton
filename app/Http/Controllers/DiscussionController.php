<?php
class DiscussionController extends BaseController
{

    protected $discussion;

    public function __construct(Discussion $discussion)
    {
        parent::__construct();
        $this->discussion = $discussion;
    }


    public function postDiscussion()
    {

        $rules = array(
            'text' => 'required',
        );

        // Validate the inputs
        $validator = Validator::make(Input::all(), $rules);

        // Check if the form validates with success
        if ($validator->passes()) {

            $file  = Input::file('discussion_file');

            if($file){
                $ext_file  = $file->guessExtension();
                $file_name = str_random(20) . '.' . $ext_file;
                $file->move('uploads/discussion/', $file_name);
                $this->discussion->file       = $file_name;
            }

            $this->discussion->text             = Input::get('text');
            $this->discussion->user_id          = Sentry::getUser()->id;
            $this->discussion->chapter_id       = Input::get('chapter_id');
            $this->discussion->instructor_id    = Input::get('instructor_id');
            $this->discussion->course_id        = Input::get('course_id');

            $this->discussion->save();

            $time = date('Y-m-d h-i-s');

            $username = Sentry::getUser()->username;
            $chapter  = Chapter::find(Input::get('chapter_id'));

            Mail::send('emails.discussion', array('username' => $username, 'comment' => Input::get('text'), 'chapter' => $chapter), function ($message) {
                $message->to(array('info@kilonewton.ru', 'alex@kilonewton.ru'), 'Kilonewton')->subject('Новый комментарий в тестах');
            });

            $arr            = array('rating' => '-', 'link' => 'chapter/' . Input::get('course_id') .'/'.  Input::get('chapter_id'));
            $json           = json_encode($arr);
            $log            = new UserLog;
            $log->user_id   = Sentry::getUser()->id;
            $log->action    = 'comment';
            $log->data      = $json;
            $log->save();

            return Response::json(array('text' => $this->discussion->text, 'name' => Sentry::getUser()->username, 'time' => $time, 'success' => 1));
            //return Redirect::back()->with('success', Lang::get('admin/course/messages.create.success'));
        } else {

            return Redirect::back()->with('error', Lang::get('admin/course/messages.create.error'));
        }
    }

    public function getDiscussion($course_id, $chapter_id, $user_id)
    {

        $discussion = DIscussion::where('chapter_id' ,'=', $chapter_id)->where('user_id', '=', $user_id)->orWhere('user_id', '=', Sentry::getUser()->id)->get();
        $course     = Course::find($course_id);

        return View::make('site/blog/discussion', compact(array('discussion', 'course', 'chapter_id')));
    }
}
