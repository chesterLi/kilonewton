<?php

use Carbon\Carbon;
use Illuminate\Support\Facades\Log;

class CartController extends BaseController

{

    /**
     *
     * @return string
     */

    private $shopId         = '149674'; //'149674';
    private $scId           = '144761'; //'556590';
    private $shopPassword   = 'Atcpss';
    private $url            = '"https://money.yandex.ru/eshop.xml'; //'https://demomoney.yandex.ru/eshop.xml';

    public function payYandex()
    {

        if(Session::has('discount')){
            $price = Session::get('discount');
        }elseif(Input::has('course_id')){
            $course = Course::find(Input::get('course_id'));
            $price  = $course->price;
        }else{
            $price = Cart::total();
        }

        $params = [
            'scId'              =>  $this->scId,
            'shopId'            =>  $this->shopId,
            'customerNumber'    =>  Sentry::getUser()->email,
            'course_id'         =>  Input::get('course_id'),
            'cartItems'         =>  urlencode(Cart::content()),
            'user_id'           =>  Sentry::getUser()->id,
            'sum'               =>  $price,
            'paymentType'       =>  Input::get('paymentType'),
            ];

        $client = new \GuzzleHttp\Client();
        $res = $client->request('POST', $this->url, [
            'form_params' => $params
        ]);

        $payment_log = new PaymentLog();
        $payment_log->request = print_r($params, 1);
        $payment_log->response = print_r($res, 1);
        $payment_log->method = 'payYandex';
        $payment_log->save();

        $html   = $res->getBody()->getContents();
        $res    = preg_replace('/href="(\/\w(.+?))"/', "href=\"https://money.yandex.ru$1href\"", $html);
        return $res;

    }

    public function getCart()
    {

        $items      = Cart::content();
        $total      = Cart::total();
        $type       = Cart::search(array('options' => array('type' => 'course')));
        $cartItems  = urlencode($items);

        return View::make('site/blog/cart', compact('items', 'total', 'type', 'cartItems'));
    }

    public function showCart($course_id)
    {
        $product = Course::find($course_id);
        return View::make('site/blog/cart_course', compact('product'));
    }

    public function addToCart($course_id, $chapter_id=null)
    {

        if(Input::get('caller') == 'fail'){

            return Redirect::to('cart')->with('fail','1');
        }

        if(empty($chapter_id)) {

            $product = Course::find($course_id);

            $user  = Sentry::getUser();

/*            Mail::send('emails.add_cart', array('name' => $product->name, 'price' => $product->price, 'username' => $user->name, 'email' => $user->email), function ($message) {
                $message->to(array('ragim@kilonewton.ru', 'info@kilonewton.ru'), 'Kilonewton')->subject('Добавлено в корзину');
            });*/

            return Response::json(array('success'=>1));

        }else{
            $product        = Chapter::find($chapter_id);
            $course         = Course::find($course_id);;
            $course_name    = $course->name;
            $course_id      = $course->id;

            Cart::add(array(
                'id'        => $product->id,
                'name'      => $product->name,
                'qty'       => 1,
                'price'     => $product->price,
/*                'price'     => Helpers::countPercent($product->price),*/
                'options'   => array('type' => 'chapter', 'course' => $course_name, 'course_id' => $course_id)
            ));

            $user  = Sentry::getUser();

/*            Mail::send('emails.add_cart', array('name' => $product->name, 'price' => $product->price, 'username' => $user->name, 'email' => $user->email), function ($message) {
                $message->to('ragim@kilonewton.ru', 'Kilonewton')->subject('Добавлено в корзину');
            });*/

            $rowid = Cart::search( array('id'   => $product->id, 'name'  => $product->name));
            //return Redirect::to('cart')->with('course_name', $course_name);
            return Response::json(array( 'total' => Cart::total(), 'rowid' => $rowid[0]));

        }

    }

    public function addItemToCart($course_id, $chapter_id)
    {

            $product        = Chapter::find($chapter_id);
            $course         = Course::find($course_id);;
            $course_name    = $course->name;
            $course_id      = $course->id;

            Cart::add(array(
                'id'        => $product->id,
                'name'      => $product->name,
                'qty'       => 1,
                'price'     => $product->price,
                'options'   => array('type' => 'chapter', 'course' => $course_name, 'course_id' => $course_id)
            ));

        $user  = Sentry::getUser();

/*        Mail::send('emails.add_cart', array('name' => $product->name, 'price' => $product->price, 'username' => $user->name, 'email' => $user->email), function ($message) {
            $message->to('ragim@kilonewton.ru', 'Kilonewton')->subject('Добавлено в корзину');
        });*/

        return Redirect::to('cart')->with('course_name', $course_name);

    }
    public function removeFromCart($rowId)
    {
        Cart::remove($rowId);

        return Redirect::to('cart');
    }


    public function sendDetails($course_id=null)
    {
        if(!$course_id){

            $input = Input::all();
            $items = Cart::content();
            $item  = null;
            if (Input::has('discount')){
                $total = Input::get('discount');
            }else {
                $total = Cart::total();
            }
        }else{

            $course = Course::find($course_id);

            $input  = Input::all();
            $item   = $course;
            $items  = null;

            if (Input::has('discount')){
                $total = Input::get('discount');
            }else {
                $total = $course->price;
            }

        }

        $rules = array(

            'details'   => 'required',
            'email'     => 'required|email',
        );

        $validator = Validator::make($input, $rules);

        if ($validator->fails()) {

            return Redirect::back()->withErrors($validator);

        } else {

           // Mail::send('emails.details', array('input' => $input['details'], 'items' => $items, 'item' => $item, 'total' => $total, 'email' => $input['email']), function ($message) {
           //     $message->to(array('ragim@kilonewton.ru','info@kilonewton.ru' /*'a@kilonewton.ru'*/), 'Kilonewton')->subject('Реквизиты');
         //   });

            Cart::destroy();
        }

        return Redirect::back()->with('details_result',1);
    }

    public function promoCart($course_id)
    {
        $code    = Input::get('promo-code');
        $now     = date('Y-m-d H-i-s');

        

        $result  = PromoCode::where('code', '=', $code)->where('course_id', '=', $course_id)->where('activated', '=', 0)
            ->where('lifetime', '>=', $now)->first();

        $course  = Course::find($course_id);

        if(!$result){
            return Redirect::back()->with('error','Такого промо-кода нет, он уже используется или истек срок действия.');
        }else{

            $session = Session::put('promo_code_id', $result->id);

            if($result->discount != 100) {
                $activated              = PromoCode::find($result->id);
                $activated->activated   = 1;
                $activated->date        = date('Y-m-d');
                $activated->user_id     = Sentry::getUser()->id;
                $activated->save();

                $discount       = ($course->price / 100) * $result->discount;
                $new_price      = $course->price - $discount;

                return Redirect::back()->with('discount', $new_price);

            }else {

                if (Sentry::getUser()->role == 'hr') {

                    if(!HrCourse::where('user_id', Sentry::getUser()->id)->where('course_id', $course_id)->get()->isEmpty()){
                        $course = HrCourse::where('user_id', Sentry::getUser()->id)->where('course_id', $course_id)->first();
                        $course->increment('number');

                        $activated = PromoCode::find($result->id);
                        $activated->activated = 1;
                        $activated->date = date('Y-m-d');
                        $activated->user_id = Sentry::getUser()->id;
                        $activated->save();

                    }else {

                        $item = new HrCourse();
                        $item->course_id = $course_id;
                        $item->number = 1;
                        $item->course_name = Course::find($course_id)->name;
                        $item->user_id = Sentry::getUser()->id;
                        $item->save();

                        $activated = PromoCode::find($result->id);
                        $activated->activated = 1;
                        $activated->date = date('Y-m-d');
                        $activated->user_id = Sentry::getUser()->id;
                        $activated->save();
                    }

                } else {


                $activated = PromoCode::find($result->id);
                $activated->activated = 1;
                $activated->date = date('Y-m-d');
                $activated->user_id = Sentry::getUser()->id;
                $activated->save();

                $chapters = $course->chapters->lists('id');

                foreach ($chapters as $chapter) {

                    $user_chapter = new UserChapter();
                    $user_chapter->user_id = Sentry::getUser()->id;
                    $user_chapter->course_id = $course->id;
                    $user_chapter->chapter_id = $chapter;
                    $user_chapter->save();
                }
            }

                return Redirect::back()->with(array('course_id' => $course_id, 'course_name' => $course->name));
            }

        }
    }


    public function getFail()
    {
        return  Redirect::to('cart')->with('fail','1');
    }

    public function getSuccess()
    {
        Log::info('success get');

        return Redirect::to('/')->with(array('success_payment' => '1'/*, 'course_name' => 'course', 'course_id' => 1*/));
    }

    public function check()
    {

        Log::info('check');

        $input = Input::all();

        Log::info(print_r($input, 1));

        if (!$input) {
            return false;
        }

        $hash = md5($input['action'] . ';' . $input['orderSumAmount'] . ';' . $input['orderSumCurrencyPaycash']
            . ';' . $input['orderSumBankPaycash'] . ';' . $this->shopId . ';' . $input['invoiceId']
            . ';' . $input['customerNumber'] . ';' . $this->shopPassword);

        if (strtolower($hash) != strtolower($input['md5'])){
	        $code = 1;
        } else {

/*            if(!Input::has('course_id')){
                $cart_items = urldecode($input['cartItems']);
                $total      = 0;
                foreach($cart_items as $item){
                    $chapter = Chapter::find($item->id);
                    $total = $total + $chapter->price;
                }

                if($total == $input['orderSumAmount']){
                    $code = 0;
                }else{
                    $code = 1;
                }

            }else{
               $course = Course::find($input['course_id']);
                if($course->price == $input['orderSumAmount']){
                    $code = 0;
                }else{
                    $code = 1;
                }
            }*/

	        $code = 0;

            if(!Input::has('course_id')) {

                $cart_items         = urldecode($input['cartItems']);

                $order              = new Order();
                $order->invoice     = $input['invoiceId'];
                $order->user_id     = $input['user_id'];
                $order->save();

                foreach(json_decode($cart_items) as $item){

                    $order_item             = new OrderItem();
                    $order_item->product    = $item->name;
                    $order_item->order_id   = $order->id;
                    $order_item->save();
                }

            } else {

                $course = Course::find($input['course_id']);

                $order = new Order();
                $order->invoice = $input['invoiceId'];
                $order->sum = $course->price;
                $order->user_id = $input['user_id'];
                //$order->promo_code_id   = 'test';//Session::get('promo_code_id');
                $order->save();

                $order_item = new OrderItem();
                $order_item->product = $course->name;
                $order_item->order_id = $order->id;
                $order_item->save();
            }

        }

        if (!Input::has('course_id')) {
/*            $response = array(
                'requestDatetime' => $input['requestDatetime'],
                'code'            => $code,
                'invoiceId'       => $input['invoiceId'],
                'shopId'          => $this->shopId,
                'user_id'         => $input['user_id'],
                'cartItems'       => $input['cartItems']);*/

            $response = '<?xml version="1.0" encoding="UTF-8"?><checkOrderResponse performedDatetime="' . $input['requestDatetime'] .'" code="' . $code .  '" invoiceId="' .$input['invoiceId'] .' "shopId="' . $this->shopId . '" user_id="' . $input['user_id'] .'" cartItems="' . $input['cartItems'] .'"/>';

            //$contents = View::make('site/blog/yapayCart')->with($response);

        } else {
/*            $response = [
                'requestDatetime' => $input['requestDatetime'],
                'code'            => $code,
                'invoiceId'       => $input['invoiceId'],
                'shopId'          => $this->shopId,
                'course_id'       => $input['course_id'], 'user_id' => $input['user_id']
            ];*/



/*            $response = '<?xml version="1.0" encoding="UTF-8"?><checkOrderResponse performedDatetime="' . $input['requestDatetime'] . '" code="' . $code . '" invoiceId="' . $input['invoiceId'] . '" shopId="' . $this->shopId .'"/>';*/

            $response = '<?xml version="1.0" encoding="UTF-8"?><checkOrderResponse performedDatetime="' . $input['requestDatetime'] . '" code="' . $code . '" invoiceId="' . $input['invoiceId'] . '" shopId="' . $this->shopId .'"/>';
           // $contents = View::make('site/blog/yapay')->with($response);
        }

        $payment_log = new PaymentLog();
        $payment_log->request = print_r($input, 1);
        $payment_log->response = print_r($response, 1);
        $payment_log->method = 'check';
        $payment_log->save();

        return $this->sendResponse($response);
    }

    public function transfer()
    {

        Log::info('transfer');

        $input = Input::all();

        if (!$input) {
            return false;
        }

/*        if ($input['customerNumber'] != 'p.balobanov@brownie-soft.com' &&
            $input['customerNumber'] != 'brownie-tester@outlook.com' &&
            $input['customerNumber'] != 'mk.1001001@gmail.com') {
            Log::info('false');
            exit;
        }*/

        $hash = md5($input['action'].';'.$input['orderSumAmount'].';'.$input['orderSumCurrencyPaycash'].';'
            .$input['orderSumBankPaycash'].';'.$this->shopId.';'.$input['invoiceId'].';'.$input['customerNumber'].';'
            .$this->shopPassword);

        if (strtolower($hash) != strtolower($input['md5'])) {
	        $code = 1;
        } else {
	        $code = 0;

            $order              = Order::where('invoice', '=', $input['invoiceId'])->first();
            $order->payed       = 1;
            $order->save();

            if(Input::has('course_id')) {

                if (Input::has('role_hr') && Input::get('hr_course') == 0){

                    if(!HrCourse::where('user_id', $input['user_id'])->where('course_id', $input['course_id'])->get()->isEmpty()){
                        $course = HrCourse::where('user_id', $input['user_id'])->where('course_id', $input['course_id'])->first();
                        $course->increment('number');

                    }else{

                        $item = new HrCourse();
                        $item->course_id = $input['course_id'];
                        $item->number = $input['amount'];
                        $item->course_name = Course::find($input['course_id'])->name;
                        $item->user_id = $input['user_id'];
                        $item->save();
                    }
                } else {
                $chapters = Chapter::where('course_id', '=', $input['course_id'])->lists('id');

                foreach ($chapters as $chapter) {

                    $user_chapter = new UserChapter();
                    $user_chapter->user_id      = $input['user_id'];
                    $user_chapter->course_id    = $input['course_id'];
                    $user_chapter->chapter_id   = $chapter;
                    $user_chapter->save();
                }

                    $user = User::find($input['user_id']);

                    $referral               = new ReferralCourse();
                    $referral->user_id      = $input['user_id'];
                    $referral->partner_id   = $user->partner_id;
                    $referral->course_id    = $input['course_id'];
                    $referral->date         = Carbon::now();
                    $referral->save();
            }
                Session::put('purchased_course', $input['course_id']);

            }else{

                $cart_items = urldecode($input['cartItems']);

                    foreach (json_decode($cart_items) as $item) {

                        $user_chapter               = new UserChapter();
                        $user_chapter->user_id      = $input['user_id'];
                        $user_chapter->course_id    = $item->options->course_id;
                        $user_chapter->chapter_id   = $item->id;
                        $user_chapter->save();
                    }

                    Session::put('purchased_chapters', $cart_items);

                    Cart::destroy();
                }
            }

        $response = '<?xml version="1.0" encoding="UTF-8"?><paymentAvisoResponse performedDatetime="'.$input['requestDatetime'] .'" code="'. $code .'" invoiceId="'. $input['invoiceId'] .'" shopId="'. $this->shopId .'"/>';

        $payment_log = new PaymentLog();
        $payment_log->request = print_r($input, 1);
        $payment_log->response = print_r($response, 1);
        $payment_log->method = 'transfer';
        $payment_log->save();

        return $this->sendResponse($response);
    }

    public function refreshPrice()
    {
        $product    = Course::find(Input::get('course_id'));
        $new_price  = $product->price * Input::get('amount');

        return Redirect::back()->with(array('new_price' => $new_price, 'amount' =>  Input::get('amount')));
    }

    private function sendResponse($responseBody) {
        header("HTTP/1.0 200");
        header("Content-Type: application/xml");
        echo $responseBody;
        exit;
    }
}