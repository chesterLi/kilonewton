<?php
namespace App\Http\Controllers;

use App\Models\Chapter;
use App\Models\ChapterTest;
use App\Models\ChapterTestFails;
use App\Models\Course;
use App\Models\Question;
use App\Models\User;
use App\Models\UserStat;
use App\Models\UsersTests;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;
use App\Http\Middleware\VerifyCsrfToken;

class UserCheckTestController extends Controller
{

    protected $test;

    protected $users_tests;

    public function __construct(ChapterTest $test, UsersTests $users_tests, ChapterTestFails $second_attempt)
    {
        $this->middleware(\App\Http\Middleware\VerifyCsrfToken::class);
        $this->test             = $test;
        $this->users_tests      = $users_tests;
        //$this->second_attempt   = $second_attempt;
    }

    public function getTests($course_id)
    {
        $test       = ChapterTest::where('course_id', '=', $course_id)->first();
        $nextTest   = ChapterTest::where('course_id', '=', $course_id)->where('id', '>', $test->id)->limit(1)->first();
        $total      = ChapterTest::where('course_id', '=', $course_id)->count();
        $paginator = 0;

        return view('site/blog/course_test', compact('test', 'nextTest', 'total', 'paginator'));
    }

    public function getNextTest($test_id)
    {

            $test       = ChapterTest::find($test_id);
        if($test){
            $lastTests = ChapterTest::where('id', '<', $test_id)->where('course_id', '=', $test->course_id)->count();
            $nextTest   = ChapterTest::where('id', '>', $test_id)->where('course_id', '=', $test->course_id)->limit(1)->first();
            $total      = ChapterTest::where('course_id', '=', $test->course_id)->count();
            $paginator = ($lastTests*5);
            return view('site/blog/course_test', compact('test', 'nextTest', 'total', 'paginator'));
        }else{
            return view('site/blog/test_result_error');
        }

    }

    public function checkCourseTest()
    {
        $input      = Input::get('test');
        $course_id  = Input::get('course_id');

        $stack = [];

        Log::info($input);

        if(is_array($input)){
            foreach($input as $key => $item){

                $right_answer = Question::find($key);
                if($item == $right_answer->var) {
                    array_push($stack, $key);
                }
            }

            $test_result = count($stack);

            if($test_result < 3) {
                Session::push('CourseTest' .$course_id. '.chapters', Input::get('chapter_id'));
            }

            $user_id  = Auth::user()->id;
            $user = User::find($user_id);
            $hr = User::find($user->hr_id);
            $username = $user->username;
            $course = Course::find($course_id);
            $course_chapters = Chapter::where('course_id', '=', $course_id)->count();
            $user_chapters = UsersTests::where('user_id', '=', $user_id)->where('course_id', '=', $course_id)
                ->where('passed', '=', 1)->count();

            if ($course_chapters != 0 and $user_chapters != 0) {
                $percentage = round(($user_chapters / $course_chapters) * 100);
            } else {
                $percentage = 0;
            }

            if ($percentage >= 80) {
                Mail::send('emails.hr_course_passed', array('hr_username' => $hr->username, 'student_name' => $username, 'course' => $course->name), function ($message) use ($hr) {
                    $message->to($hr->email, 'Kilonewton')->subject('Прохождение курса');
                });
            }

            return 'ok';
        }

    }

    public function getResult($course_id)
    {
        if($course_id){
            $res  = Session::get('CourseTest'.$course_id);
            if(isset($res)){
                $chapters   = array_unique($res['chapters']);

                $payments_chart[]   = [8, 9];
                $chart = json_encode($payments_chart);

                return view('site/blog/get_test_result', compact('chapters', 'course_id', 'chart'));
            }else{
                return view('site/blog/test_result_error', compact('course_id'));
            }
        }else{
            return view('site/blog/test_result_error');
        }


    }


    public function checkUserTest($test_id, $course_id, $chapter_id)
    {

        if(Input::get('test')){

        $input = Input::get('test');
        $stack = [];

       foreach($input as $key => $item){

            $right_answer = Question::find($key);
            if($item == $right_answer->var) {
                array_push($stack, $key);
            }
        }

        $test_result = count($stack);

        $second_attempt = UsersTests::where('user_id', Auth::user()->id)->where('test_id', $test_id)->where('second_attempt', '=' ,1)->first();
            $user_id            = Auth::user()->id;
            $user = User::find($user_id);
            $hr = User::find($user->hr_id);
            $username = $user->username;
            $course = Course::find($course_id);
            $course_chapters = Chapter::where('course_id', '=', $course_id)->count();
            $user_chapters = UsersTests::where('user_id', '=', $user_id)->where('course_id', '=', $course_id)
                ->where('passed', '=', 1)->count();

            if ($course_chapters != 0 and $user_chapters != 0) {
                $percentage = round(($user_chapters / $course_chapters) * 100);
            } else {
                $percentage = 0;
            }

            if ($percentage >= 80) {
                Mail::send('emails.hr_course_passed', array('hr_username' => $hr->username, 'student_name' => $username, 'course' => $course->name), function ($message) use ($hr) {
                    $message->to($hr->email, 'Kilonewton')->subject('Прохождение курса');
                });
            }

        if($test_result < 4){

            $message     = 'Пройдите главу еще раз';

            if(!$second_attempt) {

                $this->users_tests->user_id         = Auth::user()->id;
                $this->users_tests->test_id         = $test_id;
                $this->users_tests->course_id       = $course_id;
                $this->users_tests->second_attempt  = 1;
                $this->users_tests->right_answers   = $test_result;
                $this->users_tests->points          = 10;
                $this->users_tests->chapter_id      = $chapter_id;
                $this->users_tests->save();

            }else{

                $second_attempt->second_attempt     = 1;
                $second_attempt->right_answers      = $test_result;
                $second_attempt->save();

            }

            return redirect()->back()->with(array('test_result' => $test_result, 'message' => $message));

        }else{

            $message = 'Вы успешно сдали тест!';

            $stat   = UserStat::where('user_id', '=', Auth::user()->id)->where('course_id', '=', $course_id)->first();

            if(!$second_attempt) {

                $this->users_tests->user_id         = Auth::user()->id;
                $this->users_tests->test_id         = $test_id;
                $this->users_tests->course_id       = $course_id;
                $this->users_tests->right_answers   = $test_result;
                $this->users_tests->points          = 20;
                $this->users_tests->passed          = 1;
                $this->users_tests->chapter_id      = $chapter_id;
                $this->users_tests->save();

                if($stat){
                    $stat->points           = $stat->points + 20;
                    $stat->chapters         = $stat->chapters + 1;
                    $stat->save();
                }else{
                    $new_stat = new UserStat();
                    $new_stat->user_id      = Auth::user()->id;
                    $new_stat->course_id    = $course_id;
                    $new_stat->points       = $new_stat->points + 20;
                    $new_stat->chapters     = $new_stat->chapters + 1;
                    $new_stat->save();
                }

            $arr            = array('rating' => 20, 'link' => 'chapter/' . $course_id .'/'. $chapter_id);
            $json           = json_encode($arr);
            $log            = new UserLog;
            $log->user_id   = Auth::user()->id;
            $log->action    = 'test';
            $log->data      = $json;
            $log->save();

            }else{

                $second_attempt->right_answers      = $test_result;
                $second_attempt->passed             = 1;
                $second_attempt->save();

                if($stat){
                    $stat->points           = $stat->points + 10;
                    $stat->chapters         = $stat->chapters + 1;
                    $stat->save();
                }else{
                    $new_stat = new UserStat();
                    $new_stat->user_id      = Auth::user()->id;
                    $new_stat->course_id    = $course_id;
                    $new_stat->points       = $new_stat->points + 10;
                    $new_stat->chapters     = $new_stat->chapters + 1;
                    $new_stat->save();
                }

            $arr            = array('rating' => 10, 'link' => 'chapter/' . $course_id .'/'. $chapter_id);
            $json           = json_encode($arr);
            $log            = new UserLog;
            $log->user_id   = Auth::user()->id;
            $log->action    = 'test';
            $log->data      = $json;
            $log->save();

            }

            return redirect()->back()->with(array('test_result' => $test_result, 'message' => $message));
        }
        }else{

            $test_result = 0;
            $message     = 'Пройдите главу еще раз';

            return redirect()->back()->with(array('test_result' => $test_result, 'message' => $message));
        }


    }
}