<?php

class BlogController extends BaseController {

	/**
	 *
	 * @return View
	 */

    protected $comment;


    public function __construct(BlogComment $comment)
    {
        parent::__construct();
        $this->comment = $comment;
    }

	public function getBlog()
	{
        $articles   = Blog::where('hide', '=', '0')->with('tags')->orderBy('id','DESC')->get();
        $courses    = Course::where('hide','=',0)->get();
        $tags       = BlogTag::all();

        $seo = Seo::where('url','=','blog')->first();

        return View::make('site/blog/blog', compact(array('articles', 'courses', 'tags', 'seo')));
	}

    public function getBlogById($id)
    {
        $blog = Blog::find($id);
        $similar_blogs = array();
        $courses = Course::where('hide','=',0)->get();

        $seo = Seo::where('url','=','blog/'.$id)->first();

        if ($blog) {
            foreach ($blog->tags as $key => $tag) {
                $blog_tag = BlogTag::find($tag->id);
                array_push($similar_blogs, $blog_tag->blogs);
            }

            return View::make('site/blog/blog_inner', compact(array('blog', 'similar_blogs', 'courses', 'seo')));
        }else{

            return Redirect::to('blog');
        }
    }

    public function postComment()
    {
        $rules = array(
            'text' => 'required',
        );

        $validator = Validator::make(Input::all(), $rules);

        if ($validator->passes()) {
            $this->comment->text       = Input::get('text');
            $this->comment->user_id    = Sentry::getUser()->id;
            $this->comment->blog_id    = Input::get('blog_id');
            $this->comment->save();
            $time = date('Y-m-d h-i-s');

            $username   = User::find(Sentry::getUser()->id)->pluck('username');
            $blog       = Blog::find(Input::get('blog_id'));

            Mail::send('emails.comment', array('username' => $username, 'comment' => Input::get('text'), 'blog' => $blog), function ($message) {
                $message->to(array('ragim@kilonewton.ru', 'info@kilonewton.ru', 'a@kilonewton.ru', 'alex@kilonewton.ru'), 'Kilonewton')->subject('Новый комментарий');
            });

            return Response::json(array('text' => $this->comment->text, 'name' => Sentry::getUser()->username, 'time' => $time, 'success' => 1));

        } else {

            return Redirect::back()->with('error', Lang::get('admin/course/messages.create.error'));
        }
    }

    public function subscribe()
    {
        $email_address  = Input::get('email');
        $list_id        = '2030631';
        $api_key        = '347aeb8e790fe848ee0b1872a7d35dc1';

        $ML_Subscribers = new ML_Subscribers($api_key);

        		$subscriber = array(
        		    'email' => $email_address,
        		);
        		$result = $ML_Subscribers->setId($list_id)->add($subscriber);

        return Redirect::back()->with(array('success' => 1));
    }

    public function getByTag($id)
    {
        $r          = BlogTag::find($id);
        $courses    = Course::all();
        $tags       = BlogTag::all();
        $articles   = $r->blogs;
        $seo        = Seo::where('url','=','tag_filter/'.$id)->first();

        return View::make('site/blog/blog', compact(array('articles', 'tags', 'courses', 'seo')));
    }
}