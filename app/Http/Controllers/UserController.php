<?php
namespace App\Http\Controllers;

use App\Models\Partner;
use App\Models\User;
use App\Models\UserLog;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Intervention\Image\Facades\Image;

class UserController extends Controller
{

    /**
     * User Model
     * @var User
     */
    protected $user;

    /**
     * Inject the models.
     * @param User $user
     */
    public function __construct(User $user)
    {
//        parent::__construct();
        $this->user = $user;
    }

    /**
     * Users settings page
     *
     * @return View
     */
    public function getIndex()
    {
        list($user, $redirect) = $this->user->checkAuthAndRedirect('user');
        if ($redirect) {
            return $redirect;
        }

        // Show the page
        return View::make('site/user/index', compact('user'));
    }

    /**
     * Stores new user
     *
     */
    public function postIndex()
    {
        $this->user->username   = Input::get('username');
        $this->user->email      = Input::get('email');

        $password               = Input::get('password');
        $passwordConfirmation   = Input::get('password_confirmation');

        if (!empty($password)) {
            if ($password === $passwordConfirmation) {
                $this->user->password = $password;
                // The password confirmation will be removed from model
                // before saving. This field will be used in Ardent's
                // auto validation.
                $this->user->password_confirmation = $passwordConfirmation;
            } else {
                // Redirect to the new user page
                return Redirect::to('user/create')
                    ->withInput(Input::except('password', 'password_confirmation'))
                    ->with('error', Lang::get('admin/users/messages.password_does_not_match'));
            }
        } else {
            unset($this->user->password);
            unset($this->user->password_confirmation);
        }

        // Save if valid. Password field will be hashed before save
        $this->user->save();

        if ($this->user->id) {
            // Redirect with success message, You may replace "Lang::get(..." for your custom message.
            return Redirect::to('user/login')
                ->with('notice', Lang::get('user/user.user_account_created'));
        } else {
            // Get validation errors (see Ardent package)
            $error = $this->user->errors()->all();

            return Redirect::to('user/create')
                ->withInput(Input::except('password'))
                ->with('error', $error);
        }
    }

    /**
     * Edits a user
     *
     */
    public function postEdit($user)
    {
        $rules = array(
            'username'      => 'required',
            'email'         => 'required',
        );

        $validator = Validator::make(Input::all(), $rules);


        if ($validator->passes()) {

            $user->username     = Input::get( 'username' );
            $user->email        = Input::get( 'email' );
            $user->password     = Input::get( 'password' );

            $user->username     = Input::get('username');
            $user->email        = Input::get('email');
        }

        if (empty($error)) {
            return Redirect::to('user')
                ->with('success', Lang::get('user/user.user_account_updated'));
        } else {
            return Redirect::to('user')
                ->withInput(Input::except('password', 'password_confirmation'))
                ->with('error', 'Ошибка!');
        }
    }

    /**
     * Displays the form for user creation
     *
     */
    public function getCreate()
    {
        return View::make('site/user/create');
    }


    /**
     * Displays the login form
     *
     */
    public function getLogin()
    {
        $user = Auth::user();
        if (!empty($user->id)) {
            return Redirect::to('/');
        }

        return View::make('site/user/login');
    }

    /**
     * Attempt to do login
     *
     */
    public function postLogin()
    {
        try {
            $credentials = array(
                'email'     => Input::get('email'),
                'password'  => Input::get('password'),
            );

            $email = Input::get('email');
            $password = Input::get('password');

            if (Auth::attempt(['email' => $email, 'password' => $password])) {
                if(Session::has('refferal_data')){

                    $partner                    = Partner::where('referral', Session::pull('refferal_data'))->first();
                    $user                       = User::find(Auth::user()->id);

                    $user->partner_id           = $partner->id;
                    $user->referral_lifetime    = Carbon::now()->addMonths(3);
                    $user->referral_discount    = $partner->referral_discount;
                    $user->save();
                }

//            $user = Sentry::authenticate($credentials, true);

            

            $log = new UserLog();
            $log->user_id   = Auth::user()->id;
            $log->action    = 'authorized';
            $log->save();

            return Response::json(array('status' => 'success'));
            }
        } catch (Cartalyst\Sentry\Users\LoginRequiredException $e) {

            $err_msg = 'Введите логин.';
            return Response::json(array('login_error' => $err_msg, 'status' => 'error'));

        } catch (Cartalyst\Sentry\Users\PasswordRequiredException $e) {

            $err_msg =  'Введите пароль.';
            return Response::json(array('login_error' => $err_msg, 'status' => 'error'));

        } catch (Cartalyst\Sentry\Users\WrongPasswordException $e) {

            $err_msg = 'Неправильный пароль.';
            return Response::json(array('login_error' => $err_msg, 'status' => 'error'));

        } catch (Cartalyst\Sentry\Users\UserNotFoundException $e) {

            $err_msg = 'Пользователь не найден.';
            return Response::json(array('login_error' => $err_msg, 'status' => 'error'));

        } catch (Cartalyst\Sentry\Users\UserNotActivatedException $e) {
            $user = Sentry::findUserByLogin(Input::get('email'));
            if($user->deleted == 1){
                $err_msg = 'Пользователь был удален.';
            }else{
                $err_msg = 'Пользователь не активирован.';
            }
            return Response::json(array('login_error' => $err_msg, 'status' => 'error'));

        } // The following is only required if the throttling is enabled
        catch (Cartalyst\Sentry\Throttling\UserSuspendedException $e) {

            $err_msg = 'User is suspended.';
            return Response::json(array('login_error' => $err_msg, 'status' => 'error'));

        } catch (Cartalyst\Sentry\Throttling\UserBannedException $e) {

            $err_msg = 'User is banned.';
            return Response::json(array('login_error' => $err_msg, 'status' => 'error'));

        }
    }

    /**
     * Attempt to confirm account with code
     *
     * @param  string $code
     */

    public function getConfirm($id, $code)
    {
        $user = Sentry::findUserById($id);

        if ($user->attemptActivation($code))
        {
            Mail::send('emails.auth.success', array('username' => $user->username), function ($message) use ($user){
                $message->to($user->email, 'Kilonewton')->subject('Спасибо за регистрацию на KiloNewton.ru');
            });

            return Redirect::to('/')->with('notice', 'Спасибо за регистрацию!');
        }
        else
        {
            return Redirect::to('/')->with('error', 'Ошибка активации');
        }
    }

    /**
     * Displays the forgot password form
     *
     */
    public function getForgot()
    {
        return View::make('site/user/forgot');
    }

    /**
     * Attempt to reset password with given email
     *
     */
    public function postForgot()
    {
        try
        {
            $user       = Sentry::findUserByLogin(Input::get('email'));
            $resetCode  = $user->getResetPasswordCode();

            Mail::send('emails.auth.reset', array('code' => $resetCode, 'id' => $user->id, 'username' => $user->username), function ($message) {
                $message->to(Input::get('email'), 'Kilonewton')->subject('Kilonewton Восстановление пароля');
            });

            return Redirect::back()->with('message', 'Вам на почту выслано письмо восстановления пароля!');

        }
        catch (Cartalyst\Sentry\Users\UserNotFoundException $e)
        {
            return Redirect::back()->with('message', 'Пользователь с таким email не найден');
        }
    }

    /**
     * Shows the change password form with the given token
     *
     */
    public function getReset($id, $code)
    {
        return View::make('site/user/reset')
            ->with(array('code' => $code, 'id' => $id));
    }


    /**
     * Attempt change password of the user
     *
     */
    public function postReset()
    {

        $code       = Input::get('code');
        $id         = Input::get('user_id');
        $password   = Input::get('password');

        try
        {
            $user = Sentry::findUserById($id);

            if ($user->checkResetPasswordCode($code))
            {
                if ($user->attemptResetPassword($code, $password))
                {
                    return Redirect::to('/');
                }
                else
                {
                    return Redirect::back()->with('message', 'Ошибка :(');
                }
            }
            else
            {
                return Redirect::back()->with('message', 'Неверный код');
            }
        }
        catch (Cartalyst\Sentry\Users\UserNotFoundException $e)
        {
            return Redirect::back()->with('message', 'Пользователь не найден');
        }

    }

    /**
     * Log the user out of the application.
     *
     */
    public function getLogout()
    {
        Sentry::logout();

        return Redirect::to('/');
    }

    /**
     * Get user's profile
     * @param $username
     * @return mixed
     */
    public function getProfile($username)
    {
        $userModel = new User;
        $user = $userModel->getUserByUsername($username);

        // Check if the user exists
        if (is_null($user)) {
            return App::abort(404);
        }

        return View::make('site/user/profile', compact('user'));
    }

    public function getSettings()
    {
        list($user, $redirect) = User::checkAuthAndRedirect('user/settings');
        if ($redirect) {
            return $redirect;
        }

        return View::make('site/user/profile', compact('user'));
    }

    /**
     * Process a dumb redirect.
     * @param $url1
     * @param $url2
     * @param $url3
     * @return string
     */
    public function processRedirect($url1, $url2, $url3)
    {
        $redirect = '';
        if (!empty($url1)) {
            $redirect = $url1;
            $redirect .= (empty($url2) ? '' : '/' . $url2);
            $redirect .= (empty($url3) ? '' : '/' . $url3);
        }
        return $redirect;
    }

    public function postRegister()
    {
        try
        {
            if(Input::get('legal_entity') == 1){
                $role = 'hr';
            }else{
                $role = 'user';
            }

            $is_added_by_hr = SentEmail::where('email', Input::get('email'))->first();

            if($is_added_by_hr){
                $user = Sentry::register(array(
                    'email'     => Input::get('email'),
                    'password'  => Input::get('password'),
                    'role'      => $role,
                    'hr_id'     => $is_added_by_hr->hr_id,
                    'username'  => Input::get('name'),
                ));

                SentEmail::where('email', '=', Input::get('email'))->delete();

            }else{
                $user = Sentry::register(array(
                    'email'     => Input::get('email'),
                    'password'  => Input::get('password'),
                    'role'      => $role,
                    'username'  => Input::get('name'),
                ));
            }

            $activationCode = $user->getActivationCode();

            Mail::send('emails.auth.confirmation', array('code' => $activationCode, 'id' => $user->id, 'username' => Input::get('name')), function ($message) {
                $message->to(Input::get('email'), 'Kilonewton')->subject('Подтверждение регистрации на платформе Kilonewton.ru');
            });

            return Response::json(array( 'status' => 'success' ));

        }
        catch (Cartalyst\Sentry\Users\LoginRequiredException $e)
        {
            $error = 'Введите логин.';
            return Response::json(array( 'error' => $error, 'status' => 'error' ));
        }
        catch (Cartalyst\Sentry\Users\PasswordRequiredException $e)
        {
            $error = 'Введите пароль.';
            return Response::json(array( 'error' => $error, 'status' => 'error' ));
        }
        catch (Cartalyst\Sentry\Users\UserExistsException $e)
        {
            $error =  'Пользователь с таким логином уже существует.';
            return Response::json(array( 'error' => $error, 'status' => 'error' ));
        }

    }

    /*public function getAccount($id)
    {
        $stat   = UserStat::where('user_id', '=', $id)->with('course')->get();
        $user   = User::find($id);
        $tests  = UsersTests::where('user_id', '=', $id)->get();

        $userCourses = UserChapter::where('user_id', $id)->get();

        $array = array();

        foreach($userCourses as $userCourse){
            if(!in_array($userCourse->course_id, $array)){
                array_push($array, $userCourse->course_id);
            }
        }
        $started_courses = array();

        foreach($array as $course_id){
            array_push($started_courses, Course::find($course_id)->toArray());
        }

        $course_cat  = UserStat::where('user_id', '=', $id)->with('course')->first();
        if($course_cat) {
            $recommend = Course::where('category_id', '=', $course_cat->course->category_id)->where('hide',0)->get();
        }
       $points = UserStat::where('user_id', '=', $id)->lists('points');

        $rating = 0;
        foreach($points as $point){
            $rating = $rating + $point;
        }

        $rating = Helpers::getUserRating($id);

        return View::make('site/blog/account', compact('user', 'stat', 'tests', 'recommend', 'rating', 'started_courses'));

    }*/

    public function getAccount($id)
        {
            $stat   = UserStat::where('user_id', '=', $id)->with('course')->get();
            $user   = User::find($id);
            $tests  = UsersTests::where('user_id', '=', $id)->get();

            $userCourses =  DB::table('user_chapter')->where('user_id', $id)->groupBy('course_id')->get();

            $course_cat  = UserStat::where('user_id', '=', $id)->with('course')->first();
            if($course_cat) {
                $recommend = Course::where('category_id', '=', $course_cat->course->category_id)->get();
            }
    /*        $points = UserStat::where('user_id', '=', $id)->lists('points');

            $rating = 0;
            foreach($points as $point){
                $rating = $rating + $point;
            }*/

            $rating = Helpers::getUserRating($id);

            return View::make('site/blog/account', compact('user', 'stat', 'tests', 'recommend', 'rating', 'userCourses'));

        }

    public function getEditPage($id)
    {
        $user  = User::find($id);

        return View::make('site/blog/edit_account', compact('user'));
    }

    public function userEdit()
    {

        $rules = array(
            'username'    => 'required',
            'email'       => 'required|email',
        );

        // Validate the inputs
        $validator = Validator::make(Input::all(), $rules);


        if ($validator->passes())
        {
            $this_user = Sentry::getUser(Input::get('user_id'));

            $file_name  = Input::get('file_name');
            $no_cache   = rand(2,2);

            if(Input::has('file_name')) {
                $path = 'uploads/users/' . $file_name;
                $width = Input::get('file_width');
                $height = Input::get('file_height');
                $x = Input::get('file_x');
                $y = Input::get('file_y');
                Image::make($path)->crop($width, $height, $x, $y)->resize(180,180)->save('uploads/users/'.$no_cache.$file_name);
                //Image::make($path)->fit(670, 230)->save('uploads/users'.$file_name); //476, 270

                $this_user->file     = $no_cache.$file_name;
            }

            $this_user->username     = Input::get('username');
            $this_user->email        = Input::get('email');
            $this_user->phone        = Input::get('phone');
            $this_user->gender       = Input::get('gender');
            if(Input::has('password')){
                $this_user->password     = Input::get('password');
            }
            $this_user->save();

        } else {
            return Redirect::back()
                            ->withInput(Input::except('password', 'password_confirmation'))
                            ->withErrors($validator);
        }

        if(empty($error)) {
            // Redirect to the new user page
            return Redirect::back()
                ->with('success', 'Данные успешно сохранены!');
        } else {
            return Redirect::back()
                ->withInput(Input::except('password', 'password_confirmation'))
                ->with('error', $error);
        }
    }

    public function postFile()
    {
        $file    = Input::file('file');

        if($file){
            $ext_file  = $file->guessExtension();
            $file_name = str_random(20) . '.' . $ext_file;
            $file->move('uploads/users/', $file_name);
            $path = 'uploads/users/' . $file_name;
            Image::make($path)->resize(500, null, function ($constraint) {
                $constraint->aspectRatio();
            })->save('uploads/users/'.$file_name);
        }

        return Response::json(array('file'=>$file_name));
    }

}
