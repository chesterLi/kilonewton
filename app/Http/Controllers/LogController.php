<?php
class LogController extends BaseController{

    protected $log;

    public function __construct(UserLog $log)
    {
        parent::__construct();
        $this->log  = $log;
    }

    public static function addData()
    {

    }

    public function getData($id)
    {
        $data = UserLog::where('user_id', '=', $id)->get();

        return View::make('site/blog/log', compact('data'));
    }

    public function generateExcel($id)
    {

        $data = UserLog::where('user_id', '=', $id)->get();

        Excel::create('Лог пользователя', function($excel) use($data) {

            $excel->sheet('Лог пользователя', function($sheet) use($data) {

                $sheet->loadView('site.blog.excel_log', ['data' => $data->toArray()]);

            });

        })->export('xls');
    }

}