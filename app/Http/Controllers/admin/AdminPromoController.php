<?php

class AdminPromoController extends AdminController {


    /**
     * Question Model
     * @var Question
     */
    protected $item;

    public function __construct(PromoCode $item)
    {
        parent::__construct();

        $this->item = $item;

    }

    /**
     * Show a list of all the blog posts.
     *
     * @return View
     */

    public function getIndex()
    {
        $title = 'Добавление промо-кодов';

        return View::make('admin/promo/index', compact('title'));
    }

    public function getCreate()
    {
        $title = 'Добавление промо-кода';

        $courses = Course::all()->lists('name','id');

        return View::make('admin/promo/create_edit', compact('title','courses'));
    }

    public function postCreate()
    {
        // Declare the rules for the form validation
        $rules = array(
            'code'          => 'required',
        );

        // Validate the inputs
        $validator = Validator::make(Input::all(), $rules);

        // Check if the form validates with success
        if ($validator->passes())
        {

            $this->item->code           = Input::get('code');
            $this->item->course_id      = Input::get('courses');
            $this->item->discount       = Input::get('discount');
            $this->item->lifetime       = Input::get('lifetime');


            // Was the blog post created?
            if($this->item->save())
            {

                // Redirect to the new blog post page
                return Redirect::to('admin/promo')->with('success', Lang::get('admin/chapter/messages.create.success'));
            }

            // Redirect to the blog post create page
            return Redirect::to('admin/promo/create')->with('error', Lang::get('admin/chapter/messages.create.error'));
        }

        // Form validation failed
        return Redirect::to('admin/questions/create')->withInput()->withErrors($validator);
    }



    public static function generatePromo()
    {
        $length = 10;
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return Response::json($randomString);
    }


    public function getData()
    {
        $sections = PromoCode::leftjoin('courses', 'promo_codes.course_id', '=', 'courses.id')->leftjoin('users', 'promo_codes.user_id', '=', 'users.id')
            ->select(array('promo_codes.id','courses.name as name', 'promo_codes.code', 'promo_codes.discount' ,'promo_codes.lifetime', 'promo_codes.created_at','promo_codes.activated','promo_codes.date','users.username as username'));

        return Datatables::of($sections)

            ->add_column('actions', '<a href="{{{ URL::to(\'admin/promo/\' . $id . \'/delete\' ) }}}" class="btn btn-xs btn-danger iframe">{{{ Lang::get(\'button.delete\') }}}</a>')
            ->make();
    }

    public function getDelete($item)
       {
           // Title
           $title = 'Удалить промо-код?';

           // Show the page
           return View::make('admin/promo/delete', compact('item', 'title'));
       }

       /**
        * Remove the specified resource from storage.
        *
        * @param $post
        * @return Response
        */
       public function postDelete($item)
       {
           // Declare the rules for the form validation
           $rules = array(
               'id' => 'required|integer'
           );

           // Validate the inputs
           $validator = Validator::make(Input::all(), $rules);

           // Check if the form validates with success
           if ($validator->passes())
           {
               $id = $item->id;
               $item->delete();

               // Was the blog post deleted?
               $item = PromoCode::find($id);
               if(empty($item))
               {
                   // Redirect to the blog posts management page
                   return Redirect::to('admin/promo')->with('success', Lang::get('admin/course/messages.delete.success'));
               }
           }
           // There was a problem deleting the blog post
           return Redirect::to('admin/promo')->with('error', Lang::get('admin/course/messages.delete.error'));
       }

}