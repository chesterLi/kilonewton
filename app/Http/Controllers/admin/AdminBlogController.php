<?php

class AdminBlogController extends AdminController {


    /**
     * Tag Model
     * @var Tag
     */
    protected $item;

    private $tags;

    public function __construct(Blog $item, BlogTag $tags)
    {
        parent::__construct();
        $this->item = $item;
        $this->tags = $tags;
    }

    /**
     * Show a list of all the blog posts.
     *
     * @return View
     */
    public function getIndex()
    {
        $title = 'Редактирование';

        $item = $this->item;

        return View::make('admin/blog/index', compact('item', 'title'));
    }

    public function getCreate()
    {
        $title          = 'Добавление';
        $tags           = $this->tags->all();
        $selectedTags   = Input::old('roles', array());
        $mode           = 'create';

        return View::make('admin/blog/create_edit', compact('title', 'tags', 'selectedTags', 'create', 'mode'));
    }

    public function postCreate()
    {
        // Declare the rules for the form validation
        $rules = array(
            'title'      => 'required',
            'text'       => 'required',
            'tags'       => 'required',
        );

        // Validate the inputs
        $validator = Validator::make(Input::all(), $rules);

        // Check if the form validates with success
        if ($validator->passes())
        {
            $this->item->title        = Input::get('title');
            $this->item->text         = Input::get('text');
            $this->item->file         = Input::get('file_name');
            $this->item->hide         = (\Input::get('hide') == 1) ? 1 : 0;

            $file_name  = Input::get('file_name');
            if($file_name) {
                $path = 'uploads/blog/' . $file_name;
                $width = Input::get('file_width');
                $height = Input::get('file_height');
                $x = Input::get('file_x');
                $y = Input::get('file_y');
                Image::make($path)->crop($width, $height, $x, $y)->save('uploads/blog/crop/' . $file_name);
                Image::make($path)->fit(670, 230)->save('uploads/blog/crop/inner/' . $file_name); //476, 270
            }

            if($this->item->save())
            {
                $this->item->tags()->sync(Input::get('tags'));
                // Redirect to the new blog post page
                return Redirect::to('admin/blog/' . $this->item->id . '/edit')->with('success', Lang::get('admin/blogs/messages.create.success'));
            }

            // Redirect to the blog post create page
            return Redirect::to('admin/blog/create')->with('error', Lang::get('admin/blogs/messages.create.error'));
        }

        // Form validation failed
        return Redirect::to('admin/blog/create')->withInput()->withErrors($validator);
    }


	public function getEdit($item)
	{
        $title  = 'Редактирование';
        $tags   = $this->tags->all();;
        $mode   = 'edit';

        return View::make('admin/blog/create_edit', compact('item', 'title', 'tags', 'mode'));
	}

	public function postEdit($item)
	{
        $rules = array(
            'title'    => 'required',
            'text'     => 'required',
            'tags'     => 'required',
        );
        
        $validator = Validator::make(Input::all(), $rules);

        if ($validator->passes())
        {
            $item->title    = Input::get('title');
            $item->text     = Input::get('text');
            $item->hide     = (\Input::get('hide') == 1) ? 1 : 0;

            $file_name  = Input::get('file_name');

            if(Input::has('file_name')) {
                $path = 'uploads/blog/' . $file_name;
                $width = Input::get('file_width');
                $height = Input::get('file_height');
                $x = Input::get('file_x');
                $y = Input::get('file_y');
                Image::make($path)->crop($width, $height, $x, $y)->save('uploads/blog/crop/'.$file_name);
                Image::make($path)->fit(670, 230)->save('uploads/blog/crop/inner/'.$file_name); //476, 270

                $item->file     = Input::get('file_name');
            }
            if($item->save())
            {

                $item->tags()->sync(Input::get('tags'));

                return Redirect::to('admin/blog/' . $item->id . '/edit')->with('success', Lang::get('admin/blogs/messages.update.success'));
            }

            return Redirect::to('admin/blog/' . $item->id . '/edit')->with('error', Lang::get('admin/blogs/messages.update.error'));
        }

        return Redirect::to('admin/blog/' . $item->id . '/edit')->withInput()->withErrors($validator);
	}


    public function getData()
    {
        $cat = Blog::select(array('blog.id', 'blog.title', 'blog.hide'));

        return Datatables::of($cat)
            ->edit_column('hide','@if($hide)
                                Да
                            @else
                                Нет
                            @endif')

            ->add_column('actions', '<a href="{{{ URL::to(\'admin/blog/\' . $id . \'/edit\' ) }}}" class="iframe btn btn-xs btn-default">{{{ Lang::get(\'button.edit\') }}}</a>
                                     <a href="{{{ URL::to(\'admin/blog/\' . $id . \'/delete\' ) }}}" class="btn btn-xs btn-danger iframe">{{{ Lang::get(\'button.delete\') }}}</a>

                ')

        ->make();
    }

    public function getDelete($item)
    {
        // Title
        $title = 'Удалить?';

        // Show the page
        return View::make('admin/blog/delete', compact('item', 'title'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $post
     * @return Response
     */
    public function postDelete($item)
    {
        // Declare the rules for the form validation
        $rules = array(
            'id' => 'required|integer'
        );

        // Validate the inputs
        $validator = Validator::make(Input::all(), $rules);

        // Check if the form validates with success
        if ($validator->passes())
        {
            $id = $item->id;
            $item->delete();

            $cat = Tag::find($id);
            if(empty($cat))
            {
                // Redirect to the blog posts management page
                return Redirect::to('admin/blog')->with('success', Lang::get('admin/blogs/messages.delete.success'));
            }
        }
        // There was a problem deleting the blog post
        return Redirect::to('admin/blog')->with('error', Lang::get('admin/blogs/messages.delete.error'));
    }

    public function postFile()
    {
        $file    = Input::file('file');

        if($file){
            $ext_file  = $file->guessExtension();
            $file_name = str_random(20) . '.' . $ext_file;
            $file->move('uploads/blog/', $file_name);
        }

        return Response::json(array('file'=>$file_name));
    }
}