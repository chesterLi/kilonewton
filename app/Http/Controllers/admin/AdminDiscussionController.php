<?php

class AdminDiscussionController extends AdminController {


    /**
     * Course Model
     * @var Course
     */
    protected $item;

    protected $course;

    protected $chapter;

    protected $user;

    public function __construct(Discussion $item, Course $course, Chapter $chapter, User $user)
    {
        parent::__construct();
        $this->item         = $item;
        $this->chapter      = $chapter;
        $this->course       = $course;
        $this->user         = $user;
    }

    /**
     * Show a list of all the blog posts.
     *
     * @return View
     */
    public function getIndex()
    {
        $title = 'Обсуждение заданий';
        $item = $this->item;

        return View::make('admin/discussion/index', compact('item', 'title'));
    }


	public function getEdit($item)
	{
        $title      = 'Обсуждение задания';

        return View::make('admin/discussion/create_edit', compact('item', 'title'));
	}

    /**
     * Update the specified resource in storage.
     *
     * @param $sections
     * @return Sections
     */
	public function postEdit($item)
	{
        $rules = array(
            'chapter_id'          => 'required',
        );
        
        $validator = Validator::make(Input::all(), $rules);

        if ($validator->passes())
        {

            $this->item->chapter_id       = Input::get('chapter_id');
            $this->item->user_id          = Input::get('user_id');
            $this->item->text             = Input::get('answer');

            if($this->item->save())
            {

                return Redirect::to('admin/discussion/' . $item->id . '/edit')->with('success', Lang::get('admin/course/messages.update.success'));
            }

            return Redirect::to('admin/discussion/' . $item->id . '/edit')->with('error', Lang::get('admin/course/messages.update.error'));
        }

        return Redirect::to('admin/discussion/' . $item->id . '/edit')->withInput()->withErrors($validator);
	}


    public function getData()
    {
        $id = Sentry::getUser()->id;
        $sections =  Course::whereRaw("author_id = $id OR instructor_id = $id")->with('chapters.exams')->get();

        $exams = DB::table('courses')
                            //->where('instructor_id','=', $id)
                            //->orWhere('author_id', $id)
                            ->join('chapters', 'chapters.course_id', '=', 'courses.id')
                            ->join('discussion','discussion.chapter_id','=','chapters.id')
                            ->join('users','discussion.user_id','=','users.id')
                            ->select('courses.id', 'chapters.name as name', 'discussion.id as discussion_id', 'users.username as username','discussion.created_at');

        return Datatables::of($exams)

            ->add_column('actions', '<a href="{{{ URL::to(\'admin/discussion/\' . $discussion_id . \'/edit\' ) }}}" class="iframe btn btn-xs btn-default">Проверить</a>

                ')
        ->remove_column('id')
        ->remove_column('discussion_id')
        ->make();
    }

}