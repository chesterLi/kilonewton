<?php

class AdminUsersController extends AdminController
{


    /**
     * User Model
     * @var User
     */
    protected $user;

    /**
     * Inject the models.
     * @param User $user
     * @param Role $role
     * @param Permission $permission
     */
    public function __construct(User $user)
    {
        parent::__construct();
        $this->user = $user;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function getIndex()
    {
        // Title
        $title = Lang::get('admin/users/title.user_management');

        // Grab all the users
        $users = $this->user;

        // Show the page
        return View::make('admin/users/index', compact('users', 'title'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function getCreate()
    {
        $title = 'Добавление пользователя';
        $mode = 'create';

        return View::make('admin/users/create_edit', compact('roles', 'permissions', 'selectedRoles', 'selectedPermissions', 'title', 'mode'));
    }

    public function postCreate()
    {

        $rules = array(
            'username' => 'required',
            'email' => 'required|unique:users|email',
            'password' => 'required',
        );

        $validator = Validator::make(Input::all(), $rules);

        if ($validator->passes()) {

            $user = Sentry::createUser(array(
                'email' => Input::get('email'),
                'password' => Input::get('password'),
                'username' => Input::get('username'),
                'role' => Input::get('role'),
                'activated' => true,
            ));

            return Redirect::to('admin/users/' . $user->id . '/edit')->with('success', Lang::get('admin/blogs/messages.update.success'));

        }

        return Redirect::to('admin/users/create')->withInput()->withErrors($validator);
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param $user
     * @return Response
     */
    public function getEdit($user)
    {
        $title = 'Редактирование пользователя';

        if ($user->id) {
            $mode = 'edit';

            return View::make('admin/users/create_edit', compact('user', 'roles', 'permissions', 'title', 'mode'));

        } else {

            return Redirect::to('admin/users')->with('error', Lang::get('admin/users/messages.does_not_exist'));
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param $user
     * @return Response
     */

    public function PostEdit($user)
    {

        $rules = array(
            'username' => 'required',
            'email' => 'required',
        );

        $validator = Validator::make(Input::all(), $rules);

        if ($validator->passes()) {

            $user->username = Input::get('username');
            $user->email = Input::get('email');
            $user->activated = Input::get('activated');
            $user->role = Input::get('role');

            if ($user->save()) {

                return Redirect::to('admin/users/' . $user->id . '/edit')->with('success', Lang::get('admin/course/messages.update.success'));
            }

            return Redirect::to('admin/users/' . $user->id . '/edit')->with('error', Lang::get('admin/course/messages.update.error'));
        }

        return Redirect::to('admin/users/' . $user->id . '/edit')->withInput()->withErrors($validator);
    }

    /**
     * Remove user page.
     *
     * @param $user
     * @return Response
     */
    public function getDelete($user)
    {
        // Title
        $title = 'Удаление пользователя';

        // Show the page
        return View::make('admin/users/delete', compact('user', 'title'));
    }

    /**
     * Remove the specified user from storage.
     *
     * @param $user
     * @return Response
     */
    public function postDelete($user)
    {
        // Check if we are not trying to delete ourselves
        if ($user->id === Sentry::getUser()->id) {
            // Redirect to the user management page
            return Redirect::to('admin/users')->with('error', Lang::get('admin/users/messages.delete.impossible'));
        }
        $id = $user->id;
//        DB::table('users_tests')->where('user_id', $id)->delete();
//        $user->with('chapters')->with('promoCodes')->with('tests')->delete();

        $user->deleted = 1;
        $user->activated = 0;
        $user->update();

        // Was the comment post deleted?
        $user = User::find($id);
        //        if ( empty($user) )
//        {
//            return Redirect::to('admin/users')->with('success', Lang::get('admin/users/messages.delete.success'));
//        }
//        else
//        {
//            // There was a problem deleting the user
//            return Redirect::to('admin/users')->with('error', Lang::get('admin/users/messages.delete.error'));
//        }
        return Redirect::back()->with('success', Lang::get('admin/users/messages.delete.success'));
    }

    public function getRecovery($user){
        $user->deleted = 0;
        $user->activated = 1;
        $user->update();

        return Redirect::back();
    }

    /**
     * Show a list of all the users formatted for Datatables.
     *
     * @return Datatables JSON
     */
    public function getData()
    {
        $users = User::/*leftjoin('assigned_roles', 'assigned_roles.user_id', '=', 'users.id')
                    ->leftjoin('roles', 'roles.id', '=', 'assigned_roles.role_id')
                    ->*/
        select(array('users.id', 'users.username', 'users.email', 'users.deleted', 'phone', 'role', 'users.activated', 'users.created_at', 'users.last_login'));

        return Datatables::of($users)
            // ->edit_column('created_at','{{{ Carbon::now()->diffForHumans(Carbon::createFromFormat(\'Y-m-d H\', $test)) }}}')

            ->edit_column('activated', '@if($activated)
                            Да
                        @else
                            Нет
                        @endif')
            ->add_column('actions', '<a href="{{{ URL::to(\'admin/users/\' . $id . \'/edit\' ) }}}" class="iframe btn btn-xs btn-default">{{{ Lang::get(\'button.edit\') }}}</a>
                                @if($username == \'admin\')
                                @elseif($deleted != 1)
                                    <a href="{{{ URL::to(\'admin/users/\' . $id . \'/delete\' ) }}}" class="iframe btn btn-xs btn-danger">{{{ Lang::get(\'button.delete\') }}}</a>
                                @else
                                   <a href="{{{ URL::to(\'admin/users/\' . $id . \'/recovery\' ) }}}" class="btn btn-xs btn-danger">Восстановить</a>

                                @endif
            ')
            ->remove_column('id')
            ->remove_column('deleted')
            ->make();
    }
}
