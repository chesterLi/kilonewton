<?php

class AdminPagesController extends AdminController {


    /**
     * About Model
     * @var About
     */
    protected $about;

    public function __construct(Page $item)
    {
        parent::__construct();
        $this->item = $item;
    }

    /**
     * Show a list of all the blog posts.
     *
     * @return View
     */
    public function getIndex()
    {
        $title = 'Редактирование текстовых страниц';

        $item = $this->item;

        // Show the page
        return View::make('admin/page/index', compact('item', 'title'));
    }

    public function getCreate()
        {
            $title = 'Добавление текстовой страницы';

            return View::make('admin/page/create_edit', compact('title'));
        }

        public function postCreate()
        {
            // Declare the rules for the form validation

            $rules = array(
                'text'    => 'required',
            );

            // Validate the inputs
            $validator = Validator::make(Input::all(), $rules);

            // Check if the form validates with success
            if ($validator->passes())
            {
                $this->item->title       = Input::get('title');
                $this->item->text        = Input::get('text');


                if($this->item->save())
                {

                    // Redirect to the new blog post page
                    return Redirect::to('admin/page/' . $this->item->id . '/edit')->with('success', Lang::get('admin/blogs/messages.create.success'));
                }

                // Redirect to the blog post create page
                return Redirect::to('admin/page/create')->with('error', Lang::get('admin/blogs/messages.create.error'));
            }

            // Form validation failed
            return Redirect::to('admin/page/create')->withInput()->withErrors($validator);
        }



	public function getEdit($item)
	{
        // Title
        $title = 'Редактирование текстовой страницы';

        // Show the page
        return View::make('admin/page/create_edit', compact('item', 'title'));
	}

    /**
     * Update the specified resource in storage.
     *
     * @param $contacts
     * @return Contacts
     */
	public function postEdit($item)
	{

        // Declare the rules for the form validation
        $rules = array(
            'text'   => 'required|min:3',
        );

        // Validate the inputs
        $validator = Validator::make(Input::all(), $rules);

        // Check if the form validates with success
        if ($validator->passes())
        {
            // Update the blog post data
            $item->title       = Input::get('title');
            $item->text        = Input::get('text');

            // Was the blog post updated?
            if($item->save())
            {
                // Redirect to the new blog post page
                return Redirect::to('admin/page/' . $item->id . '/edit')->with('success', Lang::get('admin/blogs/messages.update.success'));
            }

            // Redirect to the blogs post management page
            return Redirect::to('admin/page/' . $item->id . '/edit')->with('error', Lang::get('admin/blogs/messages.update.error'));
        }

        // Form validation failed
        return Redirect::to('admin/page/' . $item->id . '/edit')->withInput()->withErrors($validator);
	}


    /**
     * Show a list of all the blog posts formatted for Datatables.
     *
     * @return Datatables JSON
     */
    public function getData()
    {
        $item = Page::select(array('pages.id', 'pages.title'));

        return Datatables::of($item)

            ->add_column('actions', '<a href="{{{ URL::to(\'admin/page/\' . $id . \'/edit\' ) }}}" class="iframe btn btn-xs btn-default">{{{ Lang::get(\'button.edit\') }}}</a>
                    ')

        ->make();
    }

}