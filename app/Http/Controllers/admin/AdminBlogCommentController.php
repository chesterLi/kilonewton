<?php

class AdminBlogCommentController extends AdminController {


    /**
     * Tag Model
     * @var Tag
     */
    protected $item;

    public function __construct(BlogComment $item)
    {
        parent::__construct();
        $this->item = $item;
    }

    /**
     * Show a list of all the blog posts.
     *
     * @return View
     */
    public function getIndex()
    {
        $title = 'Модерация комментариев';

        $item = $this->item;

        return View::make('admin/blog_comment/index', compact('item', 'title'));
    }

    public function getData()
    {
        $cat = BlogComment::leftjoin('users', 'users.id', '=', 'blog_comments.user_id')
            ->select(array('blog_comments.id','users.username as username', 'blog_comments.text'));

        return Datatables::of($cat)

            ->add_column('actions', '<a href="{{{ URL::to(\'admin/blog_comment/\' . $id . \'/delete\' ) }}}" class="btn btn-xs btn-danger iframe">{{{ Lang::get(\'button.delete\') }}}</a>

                ')

        ->make();
    }

    public function getDelete($item)
    {
        // Title
        $title = 'Удалить?';

        // Show the page
        return View::make('admin/blog_comment/delete', compact('item', 'title'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $post
     * @return Response
     */
    public function postDelete($item)
    {
        // Declare the rules for the form validation
        $rules = array(
            'id' => 'required|integer'
        );

        // Validate the inputs
        $validator = Validator::make(Input::all(), $rules);

        // Check if the form validates with success
        if ($validator->passes())
        {
            $id = $item->id;
            $item->delete();

            $cat = BlogComment::find($id);
            if(empty($cat))
            {
                // Redirect to the blog posts management page
                return Redirect::to('admin/blog_comment')->with('success', Lang::get('admin/blogs/messages.delete.success'));
            }
        }
        // There was a problem deleting the blog post
        return Redirect::to('admin/blog_comment')->with('error', Lang::get('admin/blogs/messages.delete.error'));
    }
}