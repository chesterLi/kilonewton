<?php

class AdminQuestionController extends AdminController {


    /**
     * Question Model
     * @var Question
     */
    protected $item;

    protected $test;

    public function __construct(Question $item, ChapterTest $test)
    {
        parent::__construct();

        $this->item = $item;
        $this->test = $test;

    }

    /**
     * Show a list of all the blog posts.
     *
     * @return View
     */
    public function getQuestions($test)
    {
        $title = 'Редактирование вопросов теста';
        
        return View::make('admin/questions/index', compact('title','test'));
    }

    public function getIndex($id)
    {
        $title = 'Редактирование вопросов теста';

        $test = $id;

        return View::make('admin/questions/index', compact('item', 'title', 'test'));
    }

    public function getCreate($test)
    {
        $title = 'Добавление вопроса';

        return View::make('admin/questions/create_edit', compact('title', 'test'));
    }

    public function postCreate()
    {
        // Declare the rules for the form validation
        $rules = array(
            'question'          => 'required',
            'var'               => 'required',
            'var1'              => 'required',
            'var2'              => 'required',
            'var3'              => 'required',
        );

        // Validate the inputs
        $validator = Validator::make(Input::all(), $rules);

        // Check if the form validates with success
        if ($validator->passes())
        {

            $this->item->question      = Input::get('question');
            $this->item->var           = Input::get('var');
            $this->item->var1          = Input::get('var1');
            $this->item->var2          = Input::get('var2');
            $this->item->var3          = Input::get('var3');
            $this->item->var4          = Input::get('var4');
            $this->item->test_id       = Input::get('test_id');


            // Was the blog post created?
            if($this->item->save())
            {

                // Redirect to the new blog post page
                return Redirect::to('admin/questions/' . $this->item->id . '/edit')->with('success', Lang::get('admin/chapter/messages.create.success'));
            }

            // Redirect to the blog post create page
            return Redirect::to('admin/questions/create')->with('error', Lang::get('admin/chapter/messages.create.error'));
        }

        // Form validation failed
        return Redirect::to('admin/questions/create')->withInput()->withErrors($validator);
    }


	public function getEdit($item)
	{
        $title = 'Редактирование вопроса';

        return View::make('admin/questions/create_edit', compact('item', 'title'));
	}

    /**
     * Update the specified resource in storage.
     *
     * @param $sections
     * @return Sections
     */
	public function postEdit($item)
	{
        $rules = array(
            'question'          => 'required',
            'var'               => 'required',
            'var1'              => 'required',
            'var2'              => 'required',
            'var3'              => 'required',
        );
        
        $validator = Validator::make(Input::all(), $rules);

        if ($validator->passes())
        {

            $item->question      = Input::get('question');
            $item->var           = Input::get('var');
            $item->var1          = Input::get('var1');
            $item->var2          = Input::get('var2');
            $item->var3          = Input::get('var3');
            $item->var4          = Input::get('var4');
            $item->test_id       = Input::get('test_id');

            if($item->save())
            {

                return Redirect::to('admin/questions/' . $item->id . '/edit')->with('success', Lang::get('admin/chapter/messages.update.success'));
            }

            return Redirect::to('admin/questions/' . $item->id . '/edit')->with('error', Lang::get('admin/chapter/messages.update.error'));
        }

        return Redirect::to('admin/questions/' . $item->id . '/edit')->withInput()->withErrors($validator);
	}


    public function getData($id)
    {
        $sections = Question::select(array('questions.id', 'questions.question'))->where('questions.test_id', '=', $id);

        return Datatables::of($sections)

            ->add_column('actions', '<a href="{{{ URL::to(\'admin/questions/\' . $id . \'/edit\' ) }}}" class="iframe btn btn-xs btn-default">{{{ Lang::get(\'button.edit\') }}}</a>
                                     <a href="{{{ URL::to(\'admin/questions/\' . $id . \'/delete\' ) }}}" class="btn btn-xs btn-danger iframe">{{{ Lang::get(\'button.delete\') }}}</a>

                ')

        ->make();
    }

    public function getDelete($item)
    {
        // Title
        $title = 'Удалить вопрос?';
        
        // Show the page
        return View::make('admin/questions/delete', compact('item', 'title'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $post
     * @return Response
     */
    public function postDelete($item)
    {
        // Declare the rules for the form validation
        $rules = array(
            'id' => 'required|integer'
        );

        // Validate the inputs
        $validator = Validator::make(Input::all(), $rules);

        // Check if the form validates with success
        if ($validator->passes())
        {
            $id = $item->id;

            $item->delete();

            // Was the blog post deleted?
            $item = Question::find($id);
            if(empty($item))
            {
                // Redirect to the blog posts management page
                return Redirect::to('admin/questions/'.$course_id)->with('success', Lang::get('admin/chapter/messages.delete.success'));
            }
        }
        // There was a problem deleting the blog post
        return Redirect::to('admin/questions/'.$course_id)->with('error', Lang::get('admin/chapter/messages.delete.error'));
    }

    public function deleteMaterial($id)
    {

        $material = Material::find($id);

        File::delete(public_path().'/uploads/'.$material->file);
        File::deleteDirectory(public_path().'/uploads/zip/'.$material->file);

        $material->delete;

        return Redirect::back();

    }

}