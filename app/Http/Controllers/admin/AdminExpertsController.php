<?php

class AdminExpertsController extends AdminController
{


    /**
     * UserChapter Model
     * @var UserChapter
     */
    protected $item;


    public function __construct(Expert $item, Blog $articles)
    {
        parent::__construct();
        $this->item = $item;
        $this->articles = $articles;
    }

    /**
     * Show a list of all the blog posts.
     *
     * @return View
     */
    public function getIndex()
    {
        $title = 'Редактирование экспертов';
        $item = $this->item;

        return View::make('admin/experts/index', compact('item', 'title'));
    }

    public function getCreate()
    {
        $title = 'Редактирование экспертов';
        $courses = Course::all();
        $articles = Blog::all();

        return View::make('admin/experts/create_edit', compact('title', 'articles', 'courses'));
    }

    public function postCreate()
    {
        // Declare the rules for the form validation
        $rules = array(
            'name' => 'required',
        );

        // Validate the inputs
        $validator = Validator::make(Input::all(), $rules);

        // Check if the form validates with success
        if ($validator->passes()) {

            $courses = Input::get('courses');
            $articles = Input::get('articles');

            /*            $array_c = array();
                        $array_a = array();

                        if($courses) {
                            foreach ($courses as $course) {
                                $course = Course::find($course);
                                array_push($array_c, array(
                                        'id' => $course->id,
                                        'name' => $course->name
                                    )
                                );
                            }
                        }

                        if($articles) {
                            foreach ($articles as $article) {
                                $article = Blog::find($article);
                                array_push($array_a, array(
                                        'id' => $article->id,
                                        'title' => $article->title
                                    )
                                );
                            }
                        }

                        $json_c           = json_encode($array_c);
                        $json_a           = json_encode($array_a);*/

            $file_name = Input::get('file_name');
            $file = url('uploads/experts/', $file_name);
            $fileWidth = 250;
            $fileHeight = 250;
            if ($file) {
                $fileSizes = getimagesize($file);
                if ($fileSizes) {
                    $fileWidth = $fileSizes['0'];
                    $fileHeight = $fileSizes['1'];
                }
            }
            if ($file_name) {
                $path = 'uploads/experts/' . $file_name;
                $width = Input::get('file_width') ? Input::get('file_width') : $fileWidth;
                $height = Input::get('file_height') ? Input::get('file_height') : $fileHeight;
                $x = Input::get('file_x') ? Input::get('file_x'): 0;
                $y = Input::get('file_y') ? Input::get('file_y'): 0;
                Image::make($path)->crop($width, $height, $x, $y)->resize(180, 180)->save('uploads/experts/crop/' . $file_name);

                $this->item->file = $file_name;
            }

            $this->item->name = Input::get('name');
            $this->item->area = Input::get('area');
            $this->item->regalia = Input::get('regalia');
            $this->item->contacts = Input::get('contacts');
            $this->item->category = Input::get('category');
            $this->item->text = Input::get('text');
            //$this->item->courses      = $json_c;
            //$this->item->articles     = $json_a;

            if ($this->item->save()) {
                if (is_array($articles)) {
                    $this->item->articles()->sync($articles);
                }
                if (is_array($courses)) {
                    $this->item->courses()->sync($courses);
                }

                return Redirect::to('admin/experts/' . $this->item->id . '/edit')->with('success', Lang::get('admin/course/messages.update.success'));
            }

            return Redirect::to('admin/experts/' . $this->item->id . '/edit')->with('error', Lang::get('admin/course/messages.update.error'));
        }

        return Redirect::to('admin/experts/' . $this->item->id . '/edit')->withInput()->withErrors($validator);
    }


    public function getEdit($item)
    {
        $title = 'Редактирование';
        $courses = Course::all();
        $articles = Blog::all();
        $articles_s = ExpertBlog::where('expert_id', '=', $item->id)->lists('blog_id');
        $courses_s = ExpertCourse::where('expert_id', '=', $item->id)->lists('course_id');

        return View::make('admin/experts/create_edit', compact('item', 'title', 'courses', 'articles', 'articles_s', 'courses_s'));
    }

    public function postEdit($item)
    {
        $rules = array(
            'name' => 'required',
        );

        $validator = Validator::make(Input::all(), $rules);

        if ($validator->passes()) {

            $courses = Input::get('courses');
            $articles = Input::get('articles');

            $file_name = Input::get('file_name');
            $file = url('uploads/experts/', $file_name);
            $fileWidth = 250;
            $fileHeight = 250;
            if ($file) {
                $fileSizes = getimagesize($file);
                if ($fileSizes) {
                    $fileWidth = $fileSizes['0'];
                    $fileHeight = $fileSizes['1'];
                }
            }
            if ($file_name) {
                $path = 'uploads/experts/' . $file_name;
                $width = Input::get('file_width') ? Input::get('file_width') : $fileWidth;
                $height = Input::get('file_height') ? Input::get('file_height') : $fileHeight;
                $x = Input::get('file_x') ? Input::get('file_x'): 0;
                $y = Input::get('file_y') ? Input::get('file_y'): 0;
                Image::make($path)->crop($width, $height, $x, $y)->resize(180, 180)->save('uploads/experts/crop/' . $file_name);

                $item->file = $file_name;
            }

            $item->name = Input::get('name');
            $item->area = Input::get('area');
            $item->regalia = Input::get('regalia');
            $item->contacts = Input::get('contacts');
            $item->category = Input::get('category');
            $item->text = Input::get('text');

            if ($item->save()) {

                if (is_array($articles)) {
                    $item->articles()->sync($articles);
                } else {
                    $item->articles()->detach();
                }
                if (is_array($courses)) {
                    $item->courses()->sync($courses);
                } else {
                    $item->courses()->detach();
                }
                return Redirect::to('admin/experts/' . $item->id . '/edit')->with('success', Lang::get('admin/blogs/messages.update.success'));
            }

            return Redirect::to('admin/experts/' . $item->id . '/edit')->with('error', Lang::get('admin/blogs/messages.update.error'));
        }

        return Redirect::to('admin/experts/' . $item->id . '/edit')->withInput()->withErrors($validator);
    }


    public function getData()
    {
        $experts = Expert::select(array('experts.id', 'experts.name'));

        return Datatables::of($experts)
            ->add_column('actions', '<a href="{{{ URL::to(\'admin/experts/\' . $id . \'/edit\' ) }}}" class="iframe btn btn-xs btn-default">{{{ Lang::get(\'button.edit\') }}}</a>
                                     <a href="{{{ URL::to(\'admin/experts/\' . $id . \'/delete\' ) }}}" class="btn btn-xs btn-danger iframe">{{{ Lang::get(\'button.delete\') }}}</a>

                ')
            ->make();
    }

    public function getDelete($item)
    {
        // Title
        $title = 'Удалить эксперта?';

        // Show the page
        return View::make('admin/experts/delete', compact('item', 'title'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $post
     * @return Response
     */
    public function postDelete($item)
    {
        // Declare the rules for the form validation
        $rules = array(
            'id' => 'required|integer'
        );

        // Validate the inputs
        $validator = Validator::make(Input::all(), $rules);

        // Check if the form validates with success
        if ($validator->passes()) {
            $id = $item->id;
            $item->delete();

            // Was the blog post deleted?
            $item = Expert::find($id);
            if (empty($item)) {
                // Redirect to the blog posts management page
                return Redirect::to('admin/experts')->with('success', Lang::get('admin/expert/messages.delete.success'));
            }
        }
        // There was a problem deleting the blog post
        return Redirect::to('admin/experts')->with('error', Lang::get('admin/expert/messages.delete.error'));
    }


    public function postFile()
    {
        $file = Input::file('file');

        if ($file) {
            $ext_file = $file->guessExtension();
            $file_name = str_random(20) . '.' . $ext_file;
            $file->move('uploads/experts/', $file_name);
        }

        return Response::json(array('file' => $file_name));
    }

}