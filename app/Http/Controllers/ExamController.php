<?php
class ExamController extends BaseController{

    protected $exam;

    protected $files;

    public function __construct(Exam $exam,ExamFile $files)
    {
        parent::__construct();
        $this->files = $files;
        $this->exam  = $exam;
    }

    public function uploadExamFile()
       {

           $files = Input::file('files');

           if($files){

               foreach ($files as $file) {

                   //$ext             = $file->guessExtension();

                   $original        = $file->getClientOriginalName();
                   $ext             = substr(strrchr($original, '.'), 1);

                   $original_name   = preg_replace("/\\.[^.\\s]{3,4}$/", "", $original);
                   $filename        = str_random(20) . '.' . $ext;

                   $file->move('uploads/exam/', $filename);

                   $this->files->file  = $filename;
                   $this->files->save();

                   $id = $this->files->id;

                   $name = 'uploads/exam/' . $filename;

                   $results[] = compact('id', 'name','original_name');

               }

                   return array(
                       'files' => $results
                   );

               } else{

                   return false;

               }

           }

    public function deleteExamFile($id)
    {

        $file = ExamFile::find($id);

        File::delete(public_path().'/uploads/exam/'.$file->file);
        $file->delete();

        return Redirect::back();

    }

    public function postExamFile()
    {
        $rules = array(
            'user_id' => 'required'
        );

        $validator = Validator::make(Input::all(), $rules);

        // Check if the form validates with success
        if ($validator->passes()) {

            $files_id                 = Input::get('files_id');

            $this->exam->user_id      = Input::get('user_id');
            $this->exam->chapter_id   = Input::get('chapter_id');
            $this->exam->course_id    = Input::get('course_id');
            $this->exam->status       = 1;

            // Was the blog post created?
            if ($this->exam->save()) {

                if ($files_id) {
                    foreach ($files_id as $file) {

                        $item = $this->files->find($file);
                        if (!$item)
                            throw new Exception('No such file');

                        $item->exam_id = $this->exam->id;
                        $item->save();

                        $arr            = array('rating' => '-', 'link' => 'download/exam/' . $item->file);
                        $json           = json_encode($arr);
                        $log            = new UserLog;
                        $log->user_id   = Sentry::getUser()->id;
                        $log->action    = 'exam';
                        $log->data      = $json;
                        $log->save();
                    }
                }

                $username       = Sentry::getUser()->username;
                $userEmail = Sentry::getUser()->email;
                $course_name    = Course::find(Input::get('course_id'))->pluck('name');

                Mail::send('emails.final', array('username' => $username, 'course' => $course_name), function ($message) {
                    $message->to(array('alex@kilonewton.ru', 'info@kilonewton.ru'), 'Kilonewton')->subject('Новое финальное задание');
                });
                Mail::send('emails.final_sended', array('username' => $username), function ($message) use ($userEmail) {
                    $message->to(($userEmail), 'Kilonewton')->subject('Финальное задание');
                });

               return Redirect::back()->with(array('success' => 1));
            }
        }
    }

}

