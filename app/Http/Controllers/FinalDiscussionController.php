<?php
class FinalDiscussionController extends BaseController
{

    protected $discussion;

    public function __construct(FinalDiscussion $discussion)
    {
        parent::__construct();
        $this->discussion = $discussion;
    }


    public function postDiscussion()
    {

        $rules = array(
            'text' => 'required',
        );

        // Validate the inputs
        $validator = Validator::make(Input::all(), $rules);

        // Check if the form validates with success
        if ($validator->passes()) {

            $file  = Input::file('discussion_file');

            if($file){
                $ext_file  = $file->guessExtension();
                $file_name = str_random(20) . '.' . $ext_file;
                $file->move('uploads/discussion/', $file_name);
                $this->discussion->file       = $file_name;
            }

            $this->discussion->text       = Input::get('text');
            $this->discussion->user_id    = Sentry::getUser()->id;
            $this->discussion->course_id  = Input::get('course_id');

            $this->discussion->save();

            $username = Sentry::getUser()->username;
            $course   = Course::find(Input::get('course_id'));

            Mail::send('emails.final_discussion', array('username' => $username, 'comment' => Input::get('text'), 'course' => $course), function ($message) {
                $message->to(array('info@kilonewton.ru', 'alex@kilonewton.ru'), 'Kilonewton')->subject('Новый комментарий в финальном задании');
            });

            return Redirect::back()->with('success', Lang::get('admin/course/messages.create.success'));
        } else {

            return Redirect::back()->with('error', Lang::get('admin/course/messages.create.error'));
        }
    }
}
