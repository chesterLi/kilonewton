<?php
namespace Kilonewton\Access;

use  Zend\Permissions\Acl\Resource\ResourceInterface as RI;

interface ResourceInterface extends RI
{

    const RESOURCE_CHAPTER = 'chapter';

    const ACTION_VIEW = 'view';


}