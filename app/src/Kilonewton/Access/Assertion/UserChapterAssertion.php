<?php
namespace Kilonewton\Access\Assertion;

use App\Models\UserChapter;
use Kilonewton\Access\AssertionInterface;
use Zend\Permissions\Acl\Acl;
use Zend\Permissions\Acl\Resource\ResourceInterface;
use Zend\Permissions\Acl\Role\RoleInterface;

class UserChapterAssertion implements AssertionInterface
{

    public function assert(Acl $acl, RoleInterface $role = null, ResourceInterface $resource = null, $privilege = null)
    {
        if($resource->demo || $resource->promo){
            return true;
        }

        $payed = UserChapter::where('user_id', '=', $role->id)->where('chapter_id', '=', $resource->id)->get();

        if(!$payed->isEmpty()){
            return true;
        }

        return false;
    }


}