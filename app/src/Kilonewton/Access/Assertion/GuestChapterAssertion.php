<?php
namespace Kilonewton\Access\Assertion;

use Kilonewton\Access\AssertionInterface;
use Zend\Permissions\Acl\Acl;
use Zend\Permissions\Acl\Resource\ResourceInterface;
use Zend\Permissions\Acl\Role\RoleInterface;

class GuestChapterAssertion implements AssertionInterface
{

    public function assert(Acl $acl, RoleInterface $role = null, ResourceInterface $resource = null, $privilege = null)
    {

        return $resource->promo;

    }


}