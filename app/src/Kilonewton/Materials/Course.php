<?php

use Illuminate\Support\Facades\URL;

class Course extends \Illuminate\Database\Eloquent\Model {

    protected $table = 'courses';

    public function chapters()
    {
        return $this->hasMany('Chapter');
    }

    public function chapter_ids()
    {
        return $this->hasMany('Chapter')->select(array('id'));
    }

    public function author()
    {
        return $this->hasOne('User','id', 'author_id');
    }

    public function category()
    {
        return $this->belongsTo('CourseCat', 'id');
    }

    public function tag()
    {
        return $this->belongsTo('Tag', 'id');
    }

    public function test()
    {
        return $this->hasMany('ChapterTest', 'id');
    }

}
