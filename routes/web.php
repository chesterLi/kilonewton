<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::model('user', 'User');
//Route::model('role', 'Role');
//Route::model('courses', 'Course');
//Route::model('chapter', 'Chapter');
//Route::model('materials', 'Material');
//Route::model('category', 'CourseCat');
//Route::model('exams', 'Exam');
//Route::model('discussion', 'Discussion');
//Route::model('final_discussion', 'FinalDiscussion');
//Route::model('tag', 'Tag');
//Route::model('tests', 'ChapterTest');
//Route::model('questions', 'Question');
//Route::model('access', 'UserChapter');
//Route::model('page', 'Page');
//Route::model('promo', 'PromoCode');
//Route::model('blog', 'Blog');
//Route::model('blog_tag', 'BlogTag');
//Route::model('blog_comment', 'BlogComment');
//Route::model('seo', 'Seo');
//Route::model('experts', 'Expert');
//Route::model('hr_courses', 'HrCourse');
//Route::model('mtags', 'Mtag');
//Route::model('partner', 'Partner');

/** ------------------------------------------
 *  Route constraint patterns
 *  ------------------------------------------
 */
Route::pattern('user', '[0-9]+');
Route::pattern('courses', '[0-9]+');
Route::pattern('materials', '[0-9]+');
Route::pattern('chapter', '[0-9]+');
Route::pattern('category', '[0-9]+');
Route::pattern('exams', '[0-9]+');
Route::pattern('discussion', '[0-9]+');
Route::pattern('final_discussion', '[0-9]+');
Route::pattern('tag', '[0-9]+');
Route::pattern('questions', '[0-9]+');
Route::pattern('tests', '[0-9]+');
Route::pattern('access', '[0-9]+');
Route::pattern('promo', '[0-9]+');
Route::pattern('page', '[0-9]+');
Route::pattern('blog', '[0-9]+');
Route::pattern('blog_tag', '[0-9]+');
Route::pattern('blog_comment', '[0-9]+');
Route::pattern('seo', '[0-9]+');
Route::pattern('experts', '[0-9]+');
Route::pattern('hr_courses', '[0-9]+');
Route::pattern('partner', '[0-9]+');
Route::pattern('mtags', '[0-9]+');
Route::pattern('token', '[0-9a-z]+');
Route::pattern('courses_info', '[0-9]+');

/** ------------------------------------------
 *  Admin Routes
 *  ------------------------------------------
 */



Route::group(array('prefix' => 'admin', 'before' => 'auth|admin'), function()
{
    # User Management
    Route::get('users/{user}/show', 'AdminUsersController@getShow');
    Route::get('users/{user}/edit', 'AdminUsersController@getEdit');
    Route::post('users/{user}/edit', 'AdminUsersController@postEdit');
    Route::get('users/{user}/delete', 'AdminUsersController@getDelete');
    Route::post('users/{user}/delete', 'AdminUsersController@postDelete');
    Route::get('users/{user}/recovery', 'AdminUsersController@getRecovery');
    Route::resource('users', 'AdminUsersController');
    # User Role Management
    Route::get('roles/{role}/show', 'AdminRolesController@getShow');
    Route::get('roles/{role}/edit', 'AdminRolesController@getEdit');
    Route::post('roles/{role}/edit', 'AdminRolesController@postEdit');
    Route::get('roles/{role}/delete', 'AdminRolesController@getDelete');
    Route::post('roles/{role}/delete', 'AdminRolesController@postDelete');
    Route::resource('roles', 'AdminRolesController');

    # Main Management
    Route::get('courses/{courses}/edit', 'AdminCourseController@getEdit');
    Route::post('courses/{courses}/edit', 'AdminCourseController@postEdit');
    Route::get('courses/{courses}/delete', 'AdminCourseController@getDelete');
    Route::post('courses/{courses}/delete', 'AdminCourseController@postDelete');
    Route::resource('courses', 'AdminCourseController');

    # Test Management
    Route::get('tests/{tests}/edit', 'AdminTestController@getEdit');
    Route::post('tests/{tests}/edit', 'AdminTestController@postEdit');
    Route::post('tests/get_chapters/{id}', 'AdminTestController@getChapters');
    Route::get('tests/{tests}/delete', 'AdminTestController@getDelete');
    Route::post('tests/{tests}/delete', 'AdminTestController@postDelete');
    Route::resource('tests', 'AdminTestController');

    # Main Management
    Route::get('questions/{questions}/edit', 'AdminQuestionController@getEdit');
    Route::post('questions/{questions}/edit', 'AdminQuestionController@postEdit');
    Route::get('questions/{questions}/delete', 'AdminQuestionController@getDelete');
    Route::post('questions/{questions}/delete', 'AdminQuestionController@postDelete');
    Route::get('questions/delete_mat/{id}', 'AdminQuestionController@deleteMaterial');
    Route::get('questions/{id}', 'AdminQuestionController@getQuestions');
    Route::resource('questions', 'AdminQuestionController');

    # Category Management
    Route::get('category/{category}/edit', 'AdminCourseCatController@getEdit');
    Route::post('category/{category}/edit', 'AdminCourseCatController@postEdit');
    Route::get('category/{category}/delete', 'AdminCourseCatController@getDelete');
    Route::post('category/{category}/delete', 'AdminCourseCatController@postDelete');
    Route::resource('category', 'AdminCourseCatController');

    # ChapterAccess Management
    Route::get('access/{access}/edit', 'AdminChapterAccessController@getEdit');
    Route::post('access/{access}/edit', 'AdminChapterAccessController@postEdit');
    Route::get('access/{access}/delete', 'AdminChapterAccessController@getDelete');
    Route::post('access/{access}/delete', 'AdminChapterAccessController@postDelete');
    Route::post('access/get_chapters/{id}', 'AdminChapterAccessController@getChapters');
    Route::resource('access', 'AdminChapterAccessController');

    # Category Management
    Route::get('exams/{exams}/edit', 'AdminExamController@getEdit');
    Route::post('exams/{exams}/edit', 'AdminExamController@postEdit');
    Route::get('exams/{exams}/delete', 'AdminExamController@getDelete');
    Route::post('exams/{exams}/delete', 'AdminExamController@postDelete');
    Route::resource('exams', 'AdminExamController');

    # Category Management
    Route::get('discussion/{discussion}/edit', 'AdminDiscussionController@getEdit');
    Route::post('discussion/{discussion}/edit', 'AdminDiscussionController@postEdit');
    Route::resource('discussion', 'AdminDiscussionController');

    # FinalDiscussion Management
    Route::get('final_discussion/{final_discussion}/edit', 'AdminFinalDiscussionController@getEdit');
    Route::post('final_discussion/{final_discussion}/edit', 'AdminFinalDiscussionController@postEdit');
    Route::resource('final_discussion', 'AdminFinalDiscussionController');

    # Tag Management
    Route::get('tag/{tag}/edit', 'AdminTagController@getEdit');
    Route::post('tag/{tag}/edit', 'AdminTagController@postEdit');
    Route::get('tag/{tag}/delete', 'AdminTagController@getDelete');
    Route::post('tag/{tag}/delete', 'AdminTagController@postDelete');
    Route::resource('tag', 'AdminTagController');

    # Main Management
    Route::get('chapter/{chapter}/edit', 'AdminChapterController@getEdit');
    Route::post('chapter/{chapter}/edit', 'AdminChapterController@postEdit');
    Route::get('chapter/{chapter}/delete', 'AdminChapterController@getDelete');
    Route::post('chapter/{chapter}/delete', 'AdminChapterController@postDelete');
    Route::get('chapter/delete_mat/{id}', 'AdminChapterController@deleteMaterial');
    Route::get('chapter/{id}', 'AdminChapterController@getChapters');
    Route::resource('chapter', 'AdminChapterController');

    # Promo Management
    Route::get('promo/{promo}/edit', 'AdminPromoController@getEdit');
    Route::post('promo/{promo}/edit', 'AdminPromoController@postEdit');
    Route::get('promo/{promo}/delete', 'AdminPromoController@getDelete');
    Route::post('promo/{promo}/delete', 'AdminPromoController@postDelete');
    Route::get('promo/generate', 'AdminPromoController@generatePromo');
    Route::resource('promo', 'AdminPromoController');

    # Pages Management
    Route::get('page/{page}/edit', 'AdminPagesController@getEdit');
    Route::post('page/{page}/edit', 'AdminPagesController@postEdit');
    Route::resource('page', 'AdminPagesController');

    # BlogTag Management
    Route::get('blog_tag/{blog_tag}/edit', 'AdminBlogTagController@getEdit');
    Route::post('blog_tag/{blog_tag}/edit', 'AdminBlogTagController@postEdit');
    Route::get('blog_tag/{blog_tag}/delete', 'AdminBlogTagController@getDelete');
    Route::post('blog_tag/{blog_tag}/delete', 'AdminBlogTagController@postDelete');
    Route::resource('blog_tag', 'AdminBlogTagController');

    # BlogTag Management
    Route::get('mtags/{mtags}/edit', 'AdminMTagController@getEdit');
    Route::post('mtags/{mtags}/edit', 'AdminMTagController@postEdit');
    Route::get('mtags/{mtags}/delete', 'AdminMTagController@getDelete');
    Route::post('mtags/{mtags}/delete', 'AdminMTagController@postDelete');
    Route::resource('mtags', 'AdminMTagController');

    # Blog Management
    Route::get('blog/{blog}/edit', 'AdminBlogController@getEdit');
    Route::post('blog/{blog}/edit', 'AdminBlogController@postEdit');
    Route::get('blog/{blog}/delete', 'AdminBlogController@getDelete');
    Route::post('blog/{blog}/delete', 'AdminBlogController@postDelete');
    Route::post('blog/uploads', 'AdminBlogController@postFile');
    Route::post('blog/{blog}/uploads', 'AdminBlogController@postFile');
    Route::resource('blog', 'AdminBlogController');

    # BlogComment Management
    Route::get('blog_comment/{blog_comment}/delete', 'AdminBlogCommentController@getDelete');
    Route::post('blog_comment/{blog_comment}/delete', 'AdminBlogCommentController@postDelete');
    Route::resource('blog_comment', 'AdminBlogCommentController');

    # Seo Management
    Route::get('seo/{seo}/edit', 'AdminSeoController@getEdit');
    Route::post('seo/{seo}/edit', 'AdminSeoController@postEdit');
    Route::get('seo/{seo}/delete', 'AdminSeoController@getDelete');
    Route::post('seo/{seo}/delete', 'AdminSeoController@postDelete');
    Route::resource('seo', 'AdminSeoController');

    # Experts Management
    Route::get('experts/{experts}/edit', 'AdminExpertsController@getEdit');
    Route::post('experts/{experts}/edit', 'AdminExpertsController@postEdit');
    Route::get('experts/{experts}/delete', 'AdminExpertsController@getDelete');
    Route::post('experts/{experts}/delete', 'AdminExpertsController@postDelete');
    Route::post('experts/uploads', 'AdminExpertsController@postFile');
    Route::post('experts/{experts}/uploads', 'AdminExpertsController@postFile');
    Route::resource('experts', 'AdminExpertsController');

    # HrCourse Management
    Route::get('hr_courses/{hr_courses}/edit', 'AdminHrCourseController@getEdit');
    Route::post('hr_courses/{hr_courses}/edit', 'AdminHrCourseController@postEdit');
    Route::get('hr_courses/{hr_courses}/delete', 'AdminHrCourseController@getDelete');
    Route::post('hr_courses/{hr_courses}/delete', 'AdminHrCourseController@postDelete');
    Route::resource('hr_courses', 'AdminHrCourseController');

    # Partners Management
    Route::get('partner/{partner}/edit', 'AdminPartnerController@getEdit');
    Route::post('partner/{partner}/edit', 'AdminPartnerController@postEdit');
    Route::get('partner/{partner}/delete', 'AdminPartnerController@getDelete');
    Route::post('partner/{partner}/delete', 'AdminPartnerController@postDelete');
    Route::resource('partner', 'AdminPartnerController');

    # CourseInfo Management
    //Route::resource('course_info', 'AdminCourseInfoController');

    # Admin Dashboard
    Route::resource('/', 'AdminDashboardController');
});


/** ------------------------------------------
 *  Frontend Routes
 *  ------------------------------------------
 */
//:: User Account Routes ::
Route::post('user/{user}/edit', 'UserController@postEdit');//postEdit

Route::get('user/forgot', 'UserController@getForgot');
Route::get('zip', 'SiteController@zip');
Route::post('user/forgot', 'UserController@postForgot');
Route::get('user/reset', 'UserController@getReset');
Route::post('user/reset', 'UserController@postReset');

Route::post('user/confirm/{id}/{code}', 'UserController@postEdit');//postEdit

Route::post('user/edit', 'UserController@userEdit');
Route::get('user/get_edit/{user_id}', array('before'=>'auth|currentUserEdit','uses'=> 'UserController@getEditPage'));
Route::post('account_fileupload', 'UserController@postFile');

//:: User Account Routes ::
Route::post('user/login', 'UserController@postLogin');
Route::post('user/register', 'UserController@postRegister');

# User RESTful Routes (Login, Logout, Register, etc)
Route::resource('user', 'UserController');

Route::post('send_reg_form', 'SiteController@postRegForm');
Route::post('send_form', 'SiteController@postForm');
# Index Page - Last route, no matches
Route::get('/', array('before' => 'detectLang','uses' => 'SiteController@getIndex'));

Route::get('download/{path}/{file}', 'SiteController@getDownload');

Route::get('courses/{id?}', 'SiteController@getCourses');
Route::get('course/{id}', 'SiteController@getCourseById');
Route::get('course1/{id}', 'SiteController@getCourseById1');
Route::get('tag/{id}', 'SiteController@getByTag');

Route::get('viewed/{course_id}/{chapter_id}/{user_id}/{video_id}', 'SiteController@markAsViewed');

Route::get('chapter/{course_id}/{id}', array('before' => 'access', 'uses' => 'SiteController@getChapterById'));


Route::post('discussion', 'DiscussionController@postDiscussion');
Route::post('final_discussion', 'FinalDiscussionController@postDiscussion');

Route::post('fileupload', 'ExamController@uploadExamFile');
Route::get('delete/{id}', 'ExamController@deleteExamFile');
Route::post('examfile', 'ExamController@postExamFile');


Route::post('check_test/{test_id}/{course_id}/{chapter_id}', 'UserCheckTestController@checkUserTest');

/* ***Cart*** */
Route::get('cart', 'CartController@getCart');
Route::post('cart/payYandex', 'CartController@payYandex');
Route::get('cart/fail', 'CartController@getFail');
Route::get('cart/success', 'CartController@getSuccess');
Route::post('cart/check', 'CartController@check');
Route::post('cart/transfer', 'CartController@transfer');
Route::post('cart/testtransfer', 'CartController@transfer');

Route::get('cart/{course_id}/{chapter_id?}', 'CartController@addToCart');
Route::get('cart_item/{course_id}/{chapter_id?}', 'CartController@addItemToCart');
Route::post('cart/promo/{course_id}', 'CartController@promoCart');
Route::post('cart/payment/{course_id?}/{sum?}/{id?}/{user?}', 'CartController@postPayment');
Route::get('cart/success','CartController@getSuccess');
Route::get('remove/{rowid}', 'CartController@removeFromCart');
Route::post('send_details/{course_id?}', 'CartController@sendDetails');

Route::get('show_cart/{course_id}', 'CartController@showCart');

Route::get('final/{id}', array('before' => 'auth|final', 'uses'=> 'SiteController@getFinal'));

Route::get('account/{user_id}', array('before' => 'auth|currentUser', 'uses' => 'UserController@getAccount'));

Route::get('page/{id}','SiteController@getPage');
Route::get('page/{id}','SiteController@getPage');

Route::get('contacts','SiteController@getContacts');
Route::post('contacts/sendForm', array('before' => 'csrf', 'uses' => 'SiteController@sendForm'));

Route::get('test', 'TestAclController@test');
Route::get('get_users', 'SiteController@getUsers');

Route::post('course_count/{id}', 'SiteController@countCourse');


Route::get('blog', 'BlogController@getBlog');
Route::get('blog/{id}', 'BlogController@getBlogById');
Route::get('tag_filter/{id}', 'BlogController@getByTag');
Route::post('send_comment', 'BlogController@postComment');
Route::post('subscribe', 'BlogController@subscribe');

Route::get('about', 'SiteController@getAbout');

Route::get('experts', 'SiteController@getExperts');
Route::get('expert/{id}', 'SiteController@getExpert');
Route::get('experts/{id}', 'SiteController@getExpertsById');

Route::get('cabinet_author/{id}', 'CabinetController@getAuthor');

Route::get('cabinet_hr/{id}', 'CabinetController@getHr');
Route::post('hr/add_user', 'CabinetController@addUserHr');
Route::get('hr_report/{id}', 'CabinetController@getHrReport');
Route::get('hr_statistics/{id}', 'CabinetController@getHrStatistics');
Route::get('report/{id}', 'CabinetController@report');
Route::get('hr/user_delete/{id}', 'CabinetController@HrDeleteUser');
Route::get('hr/user_delete_sent/{id}', 'CabinetController@HrDeleteUserSent');
Route::post('send_invitation', 'CabinetController@sendMailHr');
Route::get('register_me/{hr_id}/{course_id}/{email}', 'CabinetController@registerChild');

Route::get('cabinet_instructor/{id}', 'CabinetController@getInstructor');

Route::get('cabinet_partner/{id}', 'CabinetController@getPartner');
Route::post('make_promo', 'CabinetController@postPartner');
Route::post('make_referral', 'CabinetController@postReferral');
Route::get('referral/{code}', 'CabinetController@getReferral');

Route::get('user_log/{id}', 'LogController@getData');
Route::get('generate_excel/{id}', 'LogController@generateExcel');

Route::get('discussion/{course_id}/{chapter_id}/{user_id}', 'DiscussionController@getDiscussion');

Route::post('partner_search', 'CabinetController@search');
Route::get('partner_download/{code}/{course_id}', 'CabinetController@downloadPDF');

Route::get('testmail', 'SiteController@testmail');

Route::post('cart/testfail', 'CartController@fail');
Route::post('cart/testcheck', 'CartController@check');

Route::get('cart/testtransfer', 'CartController@testtransfer');

Route::get('generateXml', 'SiteController@xmlGenerate');
Route::get('getTests/{course_id}', 'UserCheckTestController@getTests');
Route::post('checkCourseTest', 'UserCheckTestController@checkCourseTest');
Route::get('getResult/{course_id}', 'UserCheckTestController@getResult');
Route::get('getNextTest/{test_id}', 'UserCheckTestController@getNextTest');

Route::get('autocad_level1', function(){
    return Redirect::to('http://testlpgenerator.ru/autocad_level1/');
});
Route::get('revit_architecture', function(){
    return Redirect::to('http://testlpgenerator.ru/revit_architecture/');
});
Route::get('revit_structure', function(){
    return Redirect::to('http://testlpgenerator.ru/revit_structure/');
});
Route::get('autocad_civil3d ', function(){
    return Redirect::to('http://testlpgenerator.ru/autocad_civil3d/');
});
Route::get('offline', function(){

    return view('site/blog/offline');
});
Route::get('payment_instruction', function(){

    return view('site/blog/payment_instruction');
});
Route::post('offline', 'SiteController@sendOfflineForm');

Route::post('refresh_price', 'CartController@refreshPrice');

Route::post('student', 'SiteController@student');

