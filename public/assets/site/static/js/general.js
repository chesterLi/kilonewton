$(document).ready(function(){

    $(".lessons-list.w-carousel").owlCarousel({
        margin: 25,
        responsive: {
            0: {
                items: 1,
                mouseDrag: true,
                touchDrag: true,
                nav: false,
                dots: true
            },
            470: {
                items: 2,
                mouseDrag: true,
                touchDrag: true,
                nav: false,
                dots: true
            },
            768: {
                items: 3,
                mouseDrag: true,
                touchDrag: true,
                nav: true,
                navText: ['',''],
                dots: false
            }
        }
    });


    // lessons-list
    $(".reviews-list").owlCarousel({
        items: 1,
        responsive: {
            0: {
                mouseDrag: true,
                touchDrag: true,
                nav: false,
                dots: true
            },
            470: {
                mouseDrag: true,
                touchDrag: true,
                nav: false,
                dots: true
            },
            768: {
                mouseDrag: true,
                touchDrag: true,
                nav: true,
                navText: ['',''],
                dots: false
            }
        }
    });

    if($('#notif').size()) $('#notif').modal('show');

    //if($('#kz-popup').size()) $('#kz-popup').modal('show');

    if(jQuery.cookie('popup') != 'seen'){
        jQuery.cookie('popup', 'seen', { expires: 365, path: '/' }); // Set it to last a year, for example.
        $('#kz-popup').modal('show');
    };

    // lessons-list
    $(".partners-slider").owlCarousel({
        margin: 10,
        loop: true,
        mouseDrag: true,
        touchDrag: true,
        responsive: {
            0: {
                items: 2,
                nav: false,
                dots: true
            },
            470: {
                items: 3,
                nav: false,
                dots: true
            },
            768: {
                items: 4,
                nav: true,
                navText: ['',''],
                dots: false
            },
            990: {
                items: 5,
                nav: true,
                navText: ['',''],
                dots: false
            }
        }
    });
    
    // fix courses list height
    resizeCoursesList();
    $(window).resize(function() {
        resizeCoursesList();
    });

    $(".check-viewed").click(function(e){
            e.preventDefault();
            var user = $(this).data("user");
            var course = $(this).data("course");
            var chapter = $(this).data("chapter");
            var material = $(this).data("material");
            var _this = $(this);
            $.ajax({
                type: 'GET',
                dataType: 'json',
                url: "/viewed/" + course + "/" + chapter + '/' + user + '/' + material
            });
            $(this).text('Просмотрено');
    });

    $(".add-cart").click(function(e){
            e.preventDefault();
            var course = $(this).data("course");
            var chapter = $(this).data("chapter");
            var chapter_name = $(this).data("name");
            var _this = $(this);
            $.ajax({
                type: 'GET',
                dataType: 'json',
                url: "/cart/" + course + "/" + chapter,
                success: function(data) {
                    $("#chapters-sum .price").html(data.total + "<span>a</span>");
                    $("#chapters-sum a").removeClass('disabled');

                    convead('event', 'update_cart', {
                      product_id: chapter,
                      qnt: 1,
                      price: data.total
                      //product_name: chapter_name
                    });

                }
            });

        $(this).addClass("disabled").addClass("cart-remove").text("добавлено в набор").attr('data-rowid', data.rowid).attr('data-chapter', chapter);

        });

        $("#buy_course").click(function(e){
            e.preventDefault();

            var id = $(this).data("course");
            var name = $(this).data("name");
            var price = $(this).data("price");

            $.ajax({
                type: 'GET',
                dataType: 'json',
                url: "/cart/" + id,
                success: function(data) {
                    if(data.success == 1) {
                        window.location.href = "/show_cart/"+id;
                    }
                }
            });
        });

    $("#buy_course1").click(function(e){
        e.preventDefault();

        var id = $(this).data("course");
        var name = $(this).data("name");
        var price = $(this).data("price");

        $.ajax({
            type: 'GET',
            dataType: 'json',
            url: "/cart/" + id,
            success: function(data) {
                if(data.success == 1) {
                    window.location.href = "/show_cart/"+id;
                }
            }
        });
    });

        $(".cart-remove").click(function(e){
            e.preventDefault();
            var row = $(this).data("row");
            var chapter = $(this).data("chapter");
            $.ajax({
                type: 'GET',
                dataType: 'json',
                url: "/remove/" + row,
                success: function(data) {
                    convead('event', 'remove_from_cart', {
                    product_id: chapter,
                    qnt: 1
                    });
                }
            });
        });

    $('.test-bar').each(function(){
        var $div            = $(this);
        var correct         = $div.data("correct");
        var total           = 5;
        var progress = Math.round((correct / total) * 100);
        $div.css("width", progress + "%");
    });


    $('.toggle-chapter').click(function(event) {
      event.preventDefault();
      $(this).next().toggle();
    });

    $("#count-test").click(function(event) {
        event.preventDefault();
        var $a        = $(this);
        var url         = $a.data("url");

        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            type: "POST",
            url: "/checkCourseTest",
            data: $('#course-test').serialize()
        })
            .done(function (data) {
                console.log(data);
                window.location = '/getNextTest/' + url;
            });
    });

    $('.points-bar').each(function(){
        var $div            = $(this);
        var points          = $div.data("points");
        var id              = $div.data("course");
             $.ajax({
                type: "POST",
                url: "/course_count/" + id,
                success: function(data) {
                    var progress = Math.round((points / data.total) * 100);
                    $div.css("width", progress + "%");
                    $div.parent().prev('.score').text(points + "/" +data.total);
                }
             });
    });

    $('.chapters-bar').each(function(){
        var $div             = $(this);
        var chapters        = $div.data("chapters");
        var user_chapters   = $div.data("user");
        var progress = Math.round((user_chapters / chapters) * 100);
        $div.css("width", progress + "%");
    });

    $(".responsive-popup").fancybox({
        width: '95%',
        height: '95%',
        autoSize: false            
    });

    $(function() {
      $( "#datepicker" ).datepicker({ dateFormat: 'yy-mm-dd' });
    });

/*    $('.datepicker').each(function(){
        $(this).datepicker();
    });*/

    /*$('#partner_table').dataTable().columnFilter();*/


});//--- $(document).reday

function showCoords(c)
 {
     $('#file-width').val(c.w);
     $('#file-height').val(c.h);
     $('#file-x').val(c.x);
     $('#file-y').val(c.y);
 }

function showPreview(c)
{
	var rx = 100 / c.w;
	var ry = 100 / c.h;

    $('#file-width').val(c.w);
    $('#file-height').val(c.h);
    $('#file-x').val(c.x);
    $('#file-y').val(c.y);

	$('#preview-user').css({
		width: Math.round(rx * 500) + 'px',
		height: Math.round(ry * 370) + 'px',
		marginLeft: '-' + Math.round(rx * c.x) + 'px',
		marginTop: '-' + Math.round(ry * c.y) + 'px'
	});
}

//$('#upload').on('click', function() {
$("input[type=file]").on('change',function(){
       var file_data = $('#user-photo').prop('files')[0];
       var form_data = new FormData();
       form_data.append('file', file_data);

       $.ajax({
           url: '/account_fileupload',
           dataType: 'json',
           cache: false,
           contentType: false,
           processData: false,
           data: form_data,
           type: 'post',
           success: function(data){
               /*document.getElementById("uploaded-image").src = '/uploads/users/' + data.file;*/
               $('#uploaded-image').attr('src', '/uploads/users/' + data.file);
               console.log($('#uploaded-image'));
               $('#file-name').val(data.file);
               $('#uploaded-image').Jcrop({
               		onChange: showCoords,
               		onSelect: showCoords,
                    aspectRatio: 1
               	});
               document.getElementById('user-photo').value= null;
           }
        });
   });
function resizeCoursesList() {
    if($(".courses-list").size()) {
        $(".courses-list .courses-item").height('auto');
        var maxHeight = 0;
        $(".courses-list .courses-item").each(function(indx, el) {
            if($(el).height() > maxHeight)
                maxHeight = $(el).height();
        });
        $(".courses-list .courses-item").height(maxHeight);
    };
 }

$('#send-message').submit(function(event) {
   event.preventDefault();
 $.ajax({
    type: "POST",
    data: $('form#send-message').serialize(),
    url: "/discussion",
    success: function(data) {
        if(data.success == 1) {
            $('#all-discussions').append('<div class="comment"><div class="name">' + data.name + ' <time>[' + data.time + ']</time></div><p>' + data.text + '</p></div>');
            $('#message-textarea').val('');
        }
        }
    });
});

$('#send-comment').submit(function(event) {
   event.preventDefault();
 $.ajax({
    type: "POST",
    data: $('form#send-comment').serialize(),
    url: "/send_comment",
    success: function(data) {
        if(data.success == 1) {
            $('#all-comments').append('<div class="comment"><div class="name">' + data.name + ' <time>[' + data.time + ']</time></div><p>' + data.text + '</p></div>');
            $('#message-textarea').val('');
        }
        }
    });

});

//$('#company_search').change(function(e) {
function searchFunction(){

var obj = $(this);
var type = obj.data('type');
var partner = obj.data('partner');
var value = obj.val();
      $.ajax({
          type: 'POST',
          dataType: 'html',
          url: "/partner_search",
          data: {value: value, type: type, partner: partner},
          success: function(data) {
          console.log(data);
              $("#partner_table").html(data);
          }
      });
}

function searchFunctionOrders(){

var obj = $(this);
var type = obj.data('type');
var partner = obj.data('partner');
var value = obj.val();
      $.ajax({
          type: 'POST',
          dataType: 'html',
          url: "/partner_search",
          data: {value: value, type: type, partner: partner},
          success: function(data) {
          console.log(data);
              $("#partner_orders").html(data);
          }
      });
}