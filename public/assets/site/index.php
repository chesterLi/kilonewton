<?php
    $tpl = $_GET['part'] ? $_GET['part'] : "main";
    
    if(in_array($tpl, array("_item","_order","_thanks"))) {
        include $_SERVER['DOCUMENT_ROOT']."/templates/".$tpl.".php";
        exit();
    }
?>


<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Kilonewton</title>

    <link href="static/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="static/js/fancybox/jquery.fancybox.css" rel="stylesheet">
    <link href="static/js/owlcarousel/assets/owl.carousel.css" rel="stylesheet">
    <link href="static/css/bootstrap-theme.css" rel="stylesheet">
    <link href="static/css/style.css" rel="stylesheet">
    
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    </head>
<body>
    <?php include __DIR__."/templates/header.php"; ?>
    
    <?php if($tpl!="main") include __DIR__."/templates/breadcrumbs.php"; ?>

    <?php include __DIR__."/templates/".$tpl.".php"; ?>
    
    <?php include __DIR__."/templates/footer.php"; ?>
    
    
    <script src="static/js/jquery-1.11.2.min.js"></script>
    <script src="static/bootstrap/js/bootstrap.min.js"></script>
    <script src="static/js/fancybox/jquery.mousewheel-3.0.6.pack.js"></script>
    <script src="static/js/fancybox/jquery.fancybox.pack.js"></script>
    <script src="static/js/owlcarousel/owl.carousel.min.js"></script>
    <script src="static/js/general.js"></script>
</body>
</html>