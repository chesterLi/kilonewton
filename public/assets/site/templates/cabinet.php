<div class="user-courses">
    <div class="container">
        <div class="row">
            <div class="col-md-3 col-sm-12 col-xs-12">
                <div class="user-photo">
                    <div class="row visible-md visible-lg">
                        <div class="col-md-12 col-xs-9">
                            <a href="#" class="user-setting pull-left"><img src="static/i/icon-setting.png"></a>
                            <div class="user-info">
                                <div class="user-name">Иван Иванов</div>
                                <div class="user-rating">Рейтинг: 60</div>
                            </div>
                        </div>
                        <div class="col-md-12 col-xs-3">
                            <img src="static/i/_temp/instructor.png" class="img-responsive img-circle">
                        </div>
                    </div>
                    <div class="row hidden-lg hidden-md">
                        <div class="col-xs-3">
                            <img src="static/i/_temp/instructor.png" class="img-responsive img-circle">
                        </div>
                        <div class="col-xs-9">
                            <a href="#" class="user-setting pull-left"><img src="static/i/icon-setting.png"></a>
                            <div class="user-info">
                                <div class="user-name">Иван Иванов</div>
                                <div class="user-rating">Рейтинг: 60</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-9 col-xs-12 courses-started">
                <div class="h1">Начатые курсы:</div>
                <div class="row">
                    <div class="col-md-4 col-xs-6 col-header">Название курса</div>
                    <div class="col-md-4 col-xs-3 col-header">Пройдено глав</div>
                    <div class="col-md-4 col-xs-3 col-header">Заработано баллов</div>
                </div>
                <div class="row">
                    <div class="col-md-4 col-xs-6"><a href="#">BIM. Основы платформы Revit</a></div>
                    <div class="col-md-4 col-xs-3">
                        <span class="progress-score">12/16</span>
                        <div class="progress progress-rounded">
                            <div class="progress-bar progress-bar-success" role="progressbar" style="width: 75%"></div>
                        </div>
                    </div>
                    <div class="col-md-4 col-xs-3">
                        <span class="progress-score">45/120</span>
                        <div class="progress progress-rounded">
                            <div class="progress-bar progress-bar-success" role="progressbar" style="width: 35%"></div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4 col-xs-6"><a href="#">Revit Architecture</a></div>
                    <div class="col-md-4 col-xs-3">
                        <span class="progress-score">9/16</span>
                        <div class="progress progress-rounded">
                            <div class="progress-bar progress-bar-success" role="progressbar" style="width: 55%"></div>
                        </div>                        
                    </div>
                    <div class="col-md-4 col-xs-3">
                        <span class="progress-score">90/120</span>
                        <div class="progress progress-rounded">
                            <div class="progress-bar progress-bar-success" role="progressbar" style="width: 75%"></div>
                        </div>                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</div><!-- .user-courses -->

<div class="user-courses-stat bg-gray">
    <div class="container">
        
        <div class="h2">Результаты проверок</div>
        
        <div class="courses-ended">
            <div class="row">
                <div class="col-sm-3 col-xs-4 col-header">Название курса</div>
                <div class="col-sm-3 col-xs-4 col-header">Вид контроля</div>
                <div class="col-xs-2 hidden-xs col-header">Результат в баллах</div>
                <div class="col-xs-2 hidden-xs col-header">Дата и время</div>
                <div class="col-sm-2 col-xs-4 col-header">Статус</div>
            </div>
            <div class="row">
                <div class="col-sm-3 col-xs-4"><a href="#">BIM. Основы платформы Revit</a></div>
                <div class="col-sm-3 col-xs-4">Тест к главе №1</div>
                <div class="col-xs-2 hidden-xs">
                    <span class="progress-score">8/10</span>
                    <div class="progress progress-rounded">
                        <div class="progress-bar progress-bar-success" role="progressbar" style="width: 80%"></div>
                    </div>
                </div>
                <div class="col-xs-2 hidden-xs">21.02.15 15:35</div>
                <div class="col-sm-2 col-xs-4"><img src="static/i/close-inverse.png"> Сдано</div>
            </div>
            <div class="row">
                <div class="col-sm-3 col-xs-4"><a href="#">Revit Architecture</a></div>
                <div class="col-sm-3 col-xs-4">Тест к главе №3</div>
                <div class="col-xs-2 hidden-xs">
                    <span class="progress-score">10/10</span>
                    <div class="progress progress-rounded">
                        <div class="progress-bar progress-bar-success" role="progressbar" style="width: 100%"></div>
                    </div>
                </div>
                <div class="col-xs-2 hidden-xs">20.01.15 12:46</div>
                <div class="col-sm-2 col-xs-4"><img src="static/i/icon-success-inverse.png"> Сдано</div>
            </div>
            <div class="row">
                <div class="col-sm-3 col-xs-4"><a href="#">BIM. Основы платформы Revit</a></div>
                <div class="col-sm-3 col-xs-4">Работа к главе №2</div>
                <div class="col-xs-2 hidden-xs">
                    <span class="progress-score">0/10</span>
                    <div class="progress progress-rounded">
                        <div class="progress-bar progress-bar-success" role="progressbar" style="width: 0%"></div>
                    </div>
                </div>
                <div class="col-xs-2 hidden-xs">12.01.15 13:12</div>
                <div class="col-sm-2 col-xs-4"><img src="static/i/icon-success-inverse.png"> Пересдать</div>
            </div>
        </div>        
        
        
        
        <div class="h2">Рекомендуемые курсы</div>
        <div class="row courses-list">
           <div class="col-md-3 col-sm-4 col-xs-6">
               <div class="courses-item bg-green">
                   <div class="courses-title">
                       <h2>Revit Architecture Базовый</h2>
                   </div>
                   <div class="courses-content">
                       <p class="author">Автор: Жуков Г.</p>
                       <p class="finished">Курс прошли 23 человека</p>
                        <a href="?part=course_single" class="courses-start">начать бесплатно<span class="glyphicon glyphicon-chevron-right pull-right" aria-hidden="true"></span></a>
                   </div>
               </div>
           </div>
           <div class="col-md-3 col-sm-4 col-xs-6">
               <div class="courses-item bg-blue">
                   <div class="courses-title">
                       <h2>Revit Architecture Базовый</h2>
                   </div>
                   <div class="courses-content">
                       <p class="author">Автор: Жуков Г.</p>
                       <p class="finished">Курс прошли 23 человека</p>
                        <a href="?part=course_single" class="courses-start">начать бесплатно<span class="glyphicon glyphicon-chevron-right pull-right" aria-hidden="true"></span></a>
                   </div>
               </div>
           </div>
           <div class="col-md-3 col-sm-4 col-xs-6">
               <div class="courses-item bg-red">
                   <div class="courses-title">
                       <h2>Revit Architecture Базовый</h2>
                   </div>
                   <div class="courses-content">
                       <p class="author">Автор: Жуков Г.</p>
                       <p class="finished">Курс прошли 23 человека</p>
                        <a href="#" class="courses-start">начать бесплатно<span class="glyphicon glyphicon-chevron-right pull-right" aria-hidden="true"></span></a>
                   </div>
               </div>
           </div>
           <div class="col-md-3 col-sm-4 col-xs-6">
               <div class="courses-item bg-yellow">
                   <div class="courses-title">
                       <h2>Revit Architecture Базовый</h2>
                   </div>
                   <div class="courses-content">
                       <p class="author">Автор: Жуков Г.</p>
                       <p class="finished">Курс прошли 23 человека</p>
                        <a href="#" class="courses-start">начать бесплатно<span class="glyphicon glyphicon-chevron-right pull-right" aria-hidden="true"></span></a>
                   </div>
               </div>
           </div>        
       </div><!-- .courses-list -->
       
    </div>
</div><!-- .user-courses-statt -->