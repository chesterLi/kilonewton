<div class="container p-b-35">
    <div class="row">
        <div class="col-xs-3 hidden-xs">
            <img src="static/i/_temp/hr-photo.png" class="img-circle img-responsive">
        </div>
        <div class="col-sm-9 col-xs-12">
            <h1 class="block-header" style="padding: 0 0 0.4em 0;">Кабинет автора</h1>
            <div class="user-info">
                <div class="user-name">ФИО: Парфенова Наталья Семеновна</div>
                <div>Количество курсов/глав: <b><a href="#">2/12</a></b></div>
                <div>Обучается/закончили: <b>10/25</b></div>
                <div><img src="static/i/icon-setting.png"> <a href="#" class="user-setting">Настройки аккаунта</a></div>
            </div>
        </div>
    </div>
</div>
        
<div class="bg-gray p-tb-35">
    <div class="container">
        <div class="h2">Курсы</div>
        <div class="row">
            <div class="col-xs-12 col-sm-6">
                <p><b>BIM. Основы Revit</b></p>
                <p>Главы 2, 3, 8</p>
            </div>
            <div class="col-xs-12 col-sm-6">
                <p><b>Revit Mep</b></p>
                <p>Главы 2, 3, 8,9, 11, 12</p>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-sm-6">
                <p><b>Revit Structure</b></p>
                <p>Главы 1, 3, 11</p>
            </div>
        </div>
    </div>
</div>

<div class="p-tb-35">
    <div class="container">
        
        <div class="h2">История платежей слушателей</div>
        
        <table class="table table-responsive table-condensed table-cleared">
            <tr>
                <th>ФИО</th>
                <th>Дата платежа</th>
                <th>Сумма</th>
                <th>Курс</th>
                <th>Главы</th>
            </tr>
            <tr>
                <td>Ким А.В.</td>
                <td>23.05.15</td>
                <td>1000</td>
                <td>Revit</td>
                <td>10, 12, 24</td>
            </tr>
            <tr>
                <td>Селеванова А.Ю.</td>
                <td>22.04.15</td>
                <td>3000</td>
                <td>BIM</td>
                <td>5, 6</td>
            </tr>
            <tr>
                <td>Бачарова А.А.</td>
                <td>20.04.15</td>
                <td>5000</td>
                <td>BIM Structure</td>
                <td>все</td>
            </tr>
            <tr class="total-row">
                <td><b>Итого:</b></td>
                <td></td>
                <td><b>9000</b></td>
                <td></td>
                <td></td>
            </tr>
        </table>

    </div>
</div>