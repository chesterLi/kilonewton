<div class="about-top">
    <img src="static/i/about-img.jpg" class="img-responsive center-block">
</div>

<div class="bg-gray about-main">
    <div class="container">
        
        <h1 class="block-header text-center">О компании</h1>
        <p class="text-gray">Проект KiloNewton.ru — это центр онлайн-образования для архитекторов, проектировщиков и инженеров-строителей. Нашей командой, совместно с привлеченными профессионалами-практиками, разрабатываются и совершенствуются дистанционные курсы по AutoCAD, Revit, Robot, Лира и другим популярным CAD- и CAE-комплексам.</p>
        <p>&nbsp;</p>
        <p><b>Миссия проекта:</b></p>
        <p>Повышение профессиональных навыков и квалификации русскоязычных специалистов в строительной отрасли.</p>
        <p>&nbsp;</p>
        <p><b>Цель проекта:</b></p>
        <p>Разработать эффективные дистанционные курсы по всем ключевым навыкам, необходимым современному инженеру-строителю, архитектору и проектировщику.</p>
        
        <h2 class="block-header text-center">Почему учатся у нас?</h2>
        <div class="about-benefit-list">
            <div class="about-benefit">
                <img src="static/i/about-benefit-1.png">
                Доступность и планомерность изложения материала
            </div>
            <div class="about-benefit">
                <img src="static/i/about-benefit-2.png">
                Глубокое погружение в тему, подробная информация о каждом инструменте
            </div>
            <div class="about-benefit">
                <img src="static/i/about-benefit-3.png">
                Занятия ведут сертифицированные преподаватели
            </div>
            <div class="about-benefit">
                <img src="static/i/about-benefit-4.png">
                Отработанная модель преподавания
            </div>
            <div class="about-benefit">
                <img src="static/i/about-benefit-5.png">
                Индивидуальный подход к обучению и индивидуальные консультации
            </div>
            <div class="about-benefit">
                <img src="static/i/about-benefit-6.png">
                Интерактивные задания
            </div>
            <div class="about-benefit">
                <img src="static/i/about-benefit-7.png">
                Удобное для занятий время и место
            </div>
            <div class="about-benefit">
                <img src="static/i/about-benefit-8.png">
                Возможность выбрать только нужные уроки, а не слушать весь курс, отыскивая необходимую информацию
            </div>
            <div class="about-benefit">
                <img src="static/i/about-benefit-9.png">
                Курс «заточен» на практическое применение знаний
            </div>
            <div class="about-benefit">
                <img src="static/i/about-benefit-10.png">
                Получение сертификата Autodesk после прохождения курса
            </div>
            <div class="about-benefit">
                <img src="static/i/about-benefit-11.png">
                Карьерный и профессиональный рост
            </div>
        </div>
        
    </div>
</div>

<div class="container team">
    
    <h2 class="block-header text-center">Команда</h2>
    <div class="row text-center">
        <div class="col-xs-10 col-xs-offset-1 col-sm-4 col-sm-offset-1">
            <div class="team-member">
                <img src="static/i/_temp/instructor.png" class="img-responsive img-circle center-block" />
                <h4>Александр Шахнович</h4>
                <h5>Генеральный директор</h5>
                <p>Отвечает за работоспособность и развитие проекта. Если Вы знаете как сделать проект лучше, или у вас есть просто идея — пишите, он с радостью Вам ответит.<br><b>a@kilonewton.ru</b></p>
            </div>
        </div>
        <div class="col-xs-10 col-xs-offset-1 col-sm-4 col-sm-offset-2">
            <div class="team-member">
                <img src="static/i/_temp/instructor.png" class="img-responsive img-circle center-block" />
                <h4>Анна Шаповалова</h4>
                <h5>Менеджер по маркетингу</h5>
                <p>Совершенствует процессы взаимодействия компании с клиентами и партнерами. Занимается подбором материалов для сайта компании.<br><b>redactor@kilonewton.ru</b></p>
            </div>
        </div>
    </div>
    <div class="row text-center">
        <div class="col-xs-10 col-xs-offset-1 col-sm-4 col-sm-offset-1">
            <div class="team-member">
                <img src="static/i/_temp/instructor.png" class="img-responsive img-circle center-block" />
                <h4>Галина Лихтарова</h4>
                <h5>Менеджер по работе с клиентами</h5>
                <p>Галина решает все вопросы, связанные с приобретением курсов и их успешным прохождением. Она постоянно общается с пользователями KiloNewton.ru и заботится об удобном и эффективном взаимодействии команды с обучающимися.<br><b>info@kilonewton.ru</b></p>
            </div>
        </div>
        <div class="col-xs-10 col-xs-offset-1 col-sm-4 col-sm-offset-2">
            <div class="team-member">
                <img src="static/i/_temp/instructor.png" class="img-responsive img-circle center-block" />
                <h4>Александр Васильев</h4>
                <h5>МРазработчик электронных курсов</h5>
                <p>Все курсы на kiloNewton, Александр, скрупулезно изучает, структурирует, выстраивает и снабжает интерактивными заданиями. Кроме того, он создает обучающие видеоролики по продуктами компании.<br><b>alex@kilonewton.ru</b></p>
            </div>
        </div>
    </div>
    <div class="row text-center">
        <div class="col-xs-10 col-xs-offset-1 col-sm-4 col-sm-offset-4">
            <div class="team-member">
                <img src="static/i/_temp/instructor.png" class="img-responsive img-circle center-block" />
                <h4>Мария Кулькина</h4>
                <h5>Программист</h5>
                <p>С любовью пишет код, который служит на благо повышения квалификации специалистов по строительному проектированию.<br><b>mk.1001001@gmail.com</b></p>
            </div>
        </div>
    </div>
    
</div>

<div class="bg-ocean about-ocean-block">
    <div class="container">
        На курсах kiloNewton преподают <a href="?part=experts">эксперты</a> отрасли
    </div>
</div>

<div class="container">
    <h2 class="block-header text-center">С нами сотрудничают:</h2>
    <div class="partners-slider-wrap">
        <div class="partners-slider">
            <div class="slide-box">
                <div class="slide"><img src="static/i/_temp/partner-1.png" /></div>
            </div>
            <div class="slide-box">
                <div class="slide"><img src="static/i/_temp/partner-2.png" /></div>
            </div>
            <div class="slide-box">
                <div class="slide"><img src="static/i/_temp/partner-3.png" /></div>
            </div>
            <div class="slide-box">
                <div class="slide"><img src="static/i/_temp/partner-4.png" /></div>
            </div>
            <div class="slide-box">
                <div class="slide"><img src="static/i/_temp/partner-5.png" class="img-responsive" /></div>
            </div>
        </div>
    </div><!-- .partners-slider-wrap -->
</div>