<div class="page-404 bg-ocean">
    <div class="container text-center">
        <h1 class="block-header">404. Что-то пошло не так.<br>Мы постараемся исправить ситуацию.</h1>
        <p><a href="#">Сообщите</a>, пожалуйста, нашему менеджеру о проблеме.</p>
        <p>Или перейдите на <a href="#">главную страницу</a>.</p>
        <img src="static/i/404-img.png" class="img-responsive center-block">
    </div>
</div>
