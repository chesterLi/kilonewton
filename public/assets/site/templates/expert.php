<div class="container p-tb-35">
    <div class="row">
        <div class="col-xs-3 hidden-xs">
            <img src="static/i/_temp/hr-photo.png" class="img-circle img-responsive">
        </div>
        <div class="col-sm-9 col-xs-12">
            <h1 class="block-header" style="padding: 0 0 0.4em 0;">Константин Биктимиров</h1>
            <div class="user-info">
                <div>Экспертные области: BIM, Revit</div>
                <div>Авторизованный инструктор Autodesk</div>
                <div>Регалии:</div>
                <div>Контакты:</div>
            </div>
        </div>
    </div>
</div>
        
<div class="bg-gray p-tb-35">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-6">
                <div class="h2">Курсы с участием эксперта</div>
                <p><a href="?part=courses">Курс Revit. Глава 1</a></p>
                <p><a href="?part=courses">Курс Revit. Глава 2</a></p>
                <p><a href="?part=courses">Курс Revit. Глава 4</a></p>
                <p><a href="?part=courses">Курс AutoCAD. Глава 1</a></p>
                <p><a href="?part=courses">Курс AutoCAD. Глава 7</a></p>
                <p><a href="?part=courses">Курс AutoCAD. Глава 5</a></p>
                <p><a href="?part=courses">Курс BIM. Глава 6</a></p>
                <p><a href="?part=courses">Курс BIM. Глава 19</a></p>
            </div>
            <div class="col-xs-12 col-sm-6">
                <div class="h2">Статьи</div>
                <p><a href="?part=blog_inner">Инженерные мысли или AutoCAD в деталях.</a></p>
                <p><a href="?part=blog_inner">Первые лифты в истории.</a></p>
                <p><a href="?part=blog_inner">Чертежи Да Винчи.</a></p>
            </div>
        </div>
    </div>
</div>