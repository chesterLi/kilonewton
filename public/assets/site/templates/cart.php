<div class="cart-header text-center">
    <div class="container">Оформление заказа</div>
</div>

<div class="cart-inner">
    <div class="container">
        <div class="row">
            <div class="col-sm-6 col-xs-12">
                <p class="course-name">Курс Revit MEP. Состав набора:</p>
                <ul class="course-consist">
                    <li class="clearfix">
                        <a href="#" class="pull-right"><img src="static/i/close-inverse.png" /></a>
                        <div class="price pull-right">1000 <span>s</span></div>
                        <p>1. Начало работы</p>
                    </li>
                    <li class="clearfix">
                        <a href="#" class="pull-right"><img src="static/i/close-inverse.png" /></a>
                        <div class="price pull-right">2000 <span>s</span></div>
                        <p>2. Создание электрической системы</p>
                    </li>
                </ul>
                
                <p class="course-name">Курс AutoCAD Civil 3D: Сети</p>
                <ul class="course-consist">
                    <li class="clearfix">
                        <a href="#" class="pull-right"><img src="static/i/close-inverse.png" /></a>
                        <div class="price pull-right">12 500 <span>s</span></div>
                        <p>Полный</p>
                    </li>
                </ul>
                <div class="cart-total clearfix">
                    <p class="pull-left">Итого:</p>
                    <div class="price pull-right">15 5000 <span>s</span></div>
                </div>
            </div>
            <div class="col-lg-3 col-lg-offset-3 col-md-4 col-md-offset-2 col-sm-5 col-sm-offset-1 col-xs-7 col-xs-offset-5 promo-code">
                <p class="text-right">Если у вас есть промо-код, введите:</p>
                <form action="">
                    <div class="form-group has-success has-feedback">
                        <div class="input-group">
                            <input type="text" class="form-control" name="promo-code">
                            <span class="input-group-btn">
                                <button class="btn btn-default" type="button"><img src="static/i/icon-success.png"></button>
                            </span>                            
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div><!-- .cart-inner -->

<div class="cart-payment bg-gray">
    <div class="container">
        <div class="h2">Выбор способа оплаты</div>
        <div class="row">
            <div class="col-xs-12 col-sm-6">
                <label class="radio-inline">
                    <input type="radio" name="payment-type" id="payment-type-1" value="1"> Физическое лицо
                </label>
                <div class="payment-type-logos">
                    <img src="static/i/_temp/payment-logos.jpg" class="img-responsive">
                </div>
                <a href="#" class="btn btn-block btn-success">перейти к оплате</a>
                <p class="payment-help-text text-center">через систему Робокасса</p>
            </div>
            <div class="col-xs-12 col-sm-6">
                <label class="radio-inline">
                    <input type="radio" name="payment-type" id="payment-type-2" value="2"> Юридическое лицо
                </label>
                <div class="form-group">
                    <textarea class="form-control" name="order-message"></textarea>
                </div>
                <a href="#" class="btn btn-block btn-primary">отправить заявку</a>
            </div>
        </div>
    </div>
</div><!-- .cart-payment -->